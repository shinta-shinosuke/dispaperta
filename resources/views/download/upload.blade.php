@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data Download
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('download.store') }}" id="myForm" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Tentang</label>                    
                    <input type="tentang" name="tentang" class="form-control" id="tentang" aria-describedby="tentang" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Pilih File</label> 
                    <input type="file" class="form-control" name="file">
                </div>
            <button type="submit" class="btn btn-primary" value="Upload">Kirim</button>
            <a href="{{ route('download')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
@endsection