@extends('layouts.user_layout')
  
@section('content')

<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Jenis Usaha
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form method="post" action="{{ route('usaha.update_jenis', $usaha->id) }}" id="myForm">
                @csrf
                @method('PUT')
                    <div class="form-group">
                        <label for="name">Nama Jenis Alsintan</label>                    
                        <input type="text" name="name" class="form-control" id="name" value="{{ $usaha->nama }}" aria-describedby="name" required="required">                
                    </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="{{ route('usaha.jenis')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection