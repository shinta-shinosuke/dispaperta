@extends('layouts.user_layout')
  
@section('content')   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data Desa
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('desa.store') }}" id="myForm">
            @csrf
                <div class="form-group">
                    <label for="name">Nama Desa</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" required="required">                
                    
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                
                <div class="form-group">
                    <label for="kecamatan">{{ __('Kecamatan') }}</label>
                        <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                            <option value="">-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $kecamatan)
                            <option value="{{ $kecamatan['kode'] }}">{{ $kecamatan['nama'] }}</option>
                            @endforeach
                        </select>    
                                
                        @error('kecamatan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="{{ route('desa')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
@endsection