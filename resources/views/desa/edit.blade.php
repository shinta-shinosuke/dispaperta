@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Desa
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @foreach ($desa as $desa)
                <form method="post" action="{{ route('desa.update', $desa['id']) }}" id="myForm">
                @csrf
                @method('PUT')
                    <div class="form-group">         
                        <input type="hidden" name="kode" class="form-control" id="kode" value="{{ $desa['id_desa'] }}" aria-describedby="kode" required="required">                
                        <label for="name">Nama Desa</label>                    
                        <input type="text" name="name" class="form-control" id="name" value="{{ $desa['nama_desa'] }}" aria-describedby="name" required="required">                
                    </div>
                    <div class="form-group kecamatan">
                        <label for="name">Kecamatan</label>                   
                        <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                            <option value="">-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $kecamatan)
                                <option {{ $desa['id_kecamatan'] == $kecamatan['kode'] ? 'selected' : ''}} value={{ $kecamatan['kode'] }}>{{ $kecamatan['nama'] }}</option>
                            @endforeach
                        </select>    
                    </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="{{ route('desa')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection