@extends('layouts.user_layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Manajemen Data Sembako</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-success" href="{{ route('sembako.create') }}"> Tambah Data Sembako</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <div class="container">
    <table class="table table-bordered data-table">
      <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th width="280px">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
   </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function () {   
    $('.data-table').DataTable({
        "autoWidth": false,
        processing: true,
        serverSide: true,
        ajax: "{{ route('sembako') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'sembako', name: 'sembako'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script>