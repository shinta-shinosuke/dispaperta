@extends('sikep.index')

@section('content')
<section id="download" style="margin-top:100px;">
      <div class="container" data-aos="fade-up" >
    	<div class="row">
          <div class="col-lg-12">
	        <div class="section-title">
	          <h2>Form</h2>
          	    <p>{{ __('Login SIKEP') }}</p>
	        </div>
      	  </div>
      	</div>
      </div>
<div class="container" data-aos="fade-up">
    @if (session('error'))
    <div class="alert alert-danger">
      {{ session('error') }}
    </div>
    @endif
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
	      <div class="row">
	          <div class="col-lg-12">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <input type="hidden" name="level" id="level" value="2">
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('register'))
                                    <a class="btn btn-link" href="{{ url('/sikep/register/') }}">
                                        {{ __('Registrasi Admin SIKEP') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
