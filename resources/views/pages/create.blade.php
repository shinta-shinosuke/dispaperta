@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 50rem;">
            <div class="card-header">
            Tambah Data Halaman
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('pages.store') }}" enctype="multipart/form-data" id="myForm">
            @csrf
                <div class="form-group">
                    <label for="name">Judul</label>                    
                    <input type="text" name="judul" class="form-control" id="judul" aria-describedby="judul" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Url</label>                    
                    <input type="text" name="slug" class="form-control" id="slug" aria-describedby="slug" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Content</label>
                    <textarea id="summernote" name="content"></textarea>
                </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="{{ route('pages')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
    <script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap5.min.js') }}"></script>
<script src="{{ asset('assets/js/summernote.min.js') }}"></script>

    <script type="text/javascript">
            $(document).ready(function () {
                $('#summernote').summernote({
                    height: 450,
                });
            });

        </script>
@endsection
