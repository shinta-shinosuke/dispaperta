@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 50rem;">
            <div class="card-header">
            Edit Data Halaman
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('pages.update', $pages['id']) }}" id="myForm">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Judul</label>                    
                    <input type="text" name="judul" class="form-control" id="judul" value="{{ $pages->judul }}" aria-describedby="judul" required="required">                
                </div>
                 <div class="form-group">
                    <label for="name">Url</label>                    
                    <input type="text" name="slug" class="form-control" id="slug"value="{{ $pages->slug }}" aria-describedby="slug" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Content</label>                   
                    <textarea id="edit-page-summernote" name="content">{{ $pages->content }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="{{ route('pages')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#edit-page-summernote').summernote({
                    height: 450,
                });
            });

        </script>
@endsection