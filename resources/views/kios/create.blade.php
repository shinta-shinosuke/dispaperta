@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data Kios
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('kios.store') }}" id="myForm">
            @csrf
                <div class="form-group">
                    <label for="name">Nama Kios</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" required="required">                
                </div>

                <div class="form-group">
                    <label for="name">Kode Kios</label>                    
                    <input type="text" name="kode" class="form-control" id="kode" aria-describedby="kode" required="required">                
                </div>

                <div class="form-group">
                    <label for="kecamatan">{{ __('Kecamatan') }}</label>
                        <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                            <option value="">-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $kecamatan)
                            <option value="{{ $kecamatan['kode_cepat'] }}">{{ $kecamatan['nama'] }}</option>
                            @endforeach
                        </select>    
                </div>
                
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="{{ route('kios')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
@endsection