@extends('layouts.user_layout')
  
@section('content')

<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Jenis Penyuluh
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form method="post" action="{{ route('penyuluh.update', $penyuluh['id']) }}" id="myForm">
                @csrf
                @method('PUT')
                    <div class="form-group">
                        <label for="name">NIP</label>                    
                        <input type="text" name="nip" class="form-control" id="nip" aria-describedby="nip" value="{{ $penyuluh['nip'] }}" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Nama Penyuluh</label>                    
                        <input type="text" name="name" class="form-control" id="name" value="{{ $penyuluh['nama'] }}" aria-describedby="name" required="required">                
                    </div>

                     <div class="form-group">
                        <label for="kecamatan">{{ __('Kecamatan') }}</label>
                            <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                                <option value="">-- Pilih Kecamatan --</option>
                                @foreach ($kecamatan as $kecamatan)
                                <option {{ $penyuluh['kode_kecamatan'] == $kecamatan['kode_cepat'] ? 'selected' : ''}} value={{ $kecamatan['kode_cepat'] }}>{{ $kecamatan['nama'] }}</option>
                                @endforeach
                            </select>    
                    </div>

                    <div class="form-group">
                        <label for="name">Nama Unor</label>                    
                        <input type="text" name="nama_unor" class="form-control" id="nama_unor" aria-describedby="nama_unor" value="{{ $penyuluh['nama_unor'] }}" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Jabatan</label>                    
                        <input type="text" name="jabatan" class="form-control" id="jabatan" aria-describedby="jabatan" value="{{ $penyuluh['jabatan'] }}" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Kelas Jabatan</label>                    
                        <input type="text" name="kelas_jabatan" maxxlength="2" class="form-control" id="kelas_jabatan" aria-describedby="kelas_jabatan" value="{{ $penyuluh['kelas_jabatan'] }}" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Unor Induk ID</label>                    
                        <input type="text" name="unor_induk_id" class="form-control" id="unor_induk_id" aria-describedby="unor_induk_id" value="{{ $penyuluh['unor_induk_id'] }}" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Jabatan Fungsional ID</label>                    
                        <input type="text" name="jabatan_fungsional_id" class="form-control" id="jabatan_fungsional_id" aria-describedby="nip" value="{{ $penyuluh['jabatan_fungsional_id'] }}" required="required">                
                    </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="{{ route('penyuluh')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection