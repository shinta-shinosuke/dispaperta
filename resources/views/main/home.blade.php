@extends('main.index')

@section('content')
<main id="main">
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>{{ __('Dashboard') }}</h2>
          <ol>
            <li><a href="{{ url('/main/home) }}">Home</a></li>
            <li class="nav-item">
            <a class="nav-link" href="{{ url('/download') }}">{{ __('Download') }}</a>
            </li>
          </ol>
        </div>

      </div>
    </section>

    <section class="inner-page">
      <div class="container">
        <p>Hi, {{ Auth::user()->name }}</p>
        {{ __('You are logged in!') }}
      </div>
    </section>
</main><!-- End #main -->
@endsection
