@extends('main.index')

@section('content')
<!-- ======= Specials Section ======= -->
    <section id="bidang" class="specials" style="margin-top:100px;">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Bidang</h2>
              <p>Tanaman Pangan</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-3">
            <ul class="nav nav-tabs flex-column">
              <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#tab-1">Struktur</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-2">Tugas Pokok</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-3">Fungsi</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-9 mt-4 mt-lg-0">
            <div class="tab-content">
              <div class="tab-pane active" id="tab-1">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                    <div id="hdbidang">
                            <div class="row">
                                <center> <span class="col-md-4 btn-info bdt" style="color: #FFF;">Kepala Bidang Tanaman Pangan</span></center>
                            </div>
                            <div class="row">
                              <div class="col-md-12" style="margin-top: 35px">
                              </div>
                            </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-2">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                     <p class="fst-italic">Bidang Tanaman Pangan mempunyai tugas melaksanan sebagian tugas Kepala Dinas dalam koordinasi, bimbingan, supervisi dan konsultasi, perencanaan, penelitian, pemantauan, pengembangan dan pengawasan serta evaluasi di bidang tanaman pangan.</p>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-3">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                    <div align="justify">
                    <ol>
                          <li>&nbsp;Pelaksanaan penyusunan dan pengembangan kebijakan teknis perencanaan dan program kerja pada Bidang Tanaman Pangan;</li>
                          <li>&nbsp;Penyelenggaraan upaya peningkatan pelayanan publik di bidang Tanaman Pangan;</li>
                          <li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan sarana prasarana tanaman pangan;</li>
                          <li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan produksi&nbsp; dan perlindungan tanaman pangan;</li>
                          <li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegitan pasca panen dan agribisnis tanaman pangan;</li>
                          <li>&nbsp;Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas pada Bidang Tanaman Pangan; dan</li>
                          <li>&nbsp;Pelaksanaan tugas kedinasan lain yang diberikan oleh atsan.<br>
                          </li>
                        </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Specials Section -->
@endsection
