@extends('main.index')

@section('content')

 <main id="main">
    <section class="breadcrumbs">
      <div class="container">

        <div class="justify-content-between align-items-center">
          <h2>{{ $articles['judul'] }}</h2>
          <ol>
            <li><i class="fa fa-clock-o"></i>&nbsp;&nbsp;{{ $articles['day_created'] }}, {{ $articles['created_at'] }}</li>
            <li><i class="fa heart-o"></i>&nbsp;&nbsp;{{ $articles['viewer'] }}</li>
            <li><i class="fa fa-user"></i>&nbsp;&nbsp;{{ $articles['posting_by'] }}</li>
          </ol>
        </div>

      </div>
    </section>

    <section class="inner-page">
      <div class="container">
        <p style="color:#FFF">
          {!! $articles['content'] !!}
        </p>
      </div>
    </section>

  </main><!-- End #main -->
  <script src="{{ asset( 'assets/js/jquery-1.9.1.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {   
      var base_url = {!! json_encode(url('/')) !!};
      var src = $('img').attr('src');
      $('img').removeAttr('src');
      $('img').attr('src',base_url+src);
      });
  </script>
  @endsection