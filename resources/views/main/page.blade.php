@extends('layouts.user_layout')

@section('content')
<!-- ======= Specials Section ======= -->
    <section id="bidang" class="specials">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Bidang</h2>
          @if (isset($_GET['p']))
            @php
              $uri = explode('-', $_GET['p']);

              if(count($uri)>1){
                echo '<p>'.ucfirst($uri[0]).' '.ucfirst($uri[1]).'</p>';
              }else{
                echo '<p>'.ucfirst($_GET['p']).'</p>';
              }
            @endphp
          @else
              <p>Sekretariat</p>
          @endif
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-3">
            <ul class="nav nav-tabs flex-column">
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-1">Struktur</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-2">Tugas Pokok</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active show" data-bs-toggle="tab" href="#tab-3">Fungsi</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-9 mt-4 mt-lg-0">
            <div class="tab-content">
              <div class="tab-pane" id="tab-1">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                    <div id="hdbidang">
                      @if (isset($_GET['p']))
                        @php
                          $uri = explode('-', $_GET['p']);
                            if(count($uri)>1){
                            echo' 
                            <div class="row">
                                <center> <span class="col-md-4 btn-info bdt" style="color: #FFF;">Kepala Bidang '.ucfirst($uri[0]).' '.ucfirst($uri[1]).'</span></center>
                            </div>
                            <div class="row">
                              <div class="col-md-12" style="margin-top: 35px">
                              </div>
                            </div>';
                            }else{
                              if($uri[0] !== 'sekretariat' && $uri[0] !== 'pskp'){
                              echo' 
                                <div class="row">
                                    <center> <span class="col-md-4 btn-info bdt" style="color: #FFF;">Kepala Bidang '.ucfirst($uri[0]).'</span></center>
                                </div>
                                <div class="row">
                                  <div class="col-md-12" style="margin-top: 35px">
                                  </div>
                                </div>';
                              }else if($uri[0] == 'pskp'){
                              echo'
                                  <div class="row">
                                        <center> <span class="col-md-4 btn-info bdt" style="color: #FFF;">Kepala Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian</span></center>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12" style="margin-top: 35px">
                                  </div>
                                </div>';
                              }else{
                                echo '
                                <div class="row">
                                 <center> <span class="col-md-4 btn-info bdt" style="color: #FFF;">Sekretaris</span></center>
                                </div>
                                <div class="row">
                                  <div class="col-md-12" style="margin-top: 35px">
                                      <span class="col-md-3 col-sm-4 btn-info spbidang1" style="color: #FFF;">Kepala Bidang Program</span>
                                      <span class="col-md-3 col-sm-4 btn-info spbidang2" style="color: #FFF;">Kepala Bidang Umpeg</span>
                                      <span class="col-md-3 col-sm-4 btn-info spbidang3">Kepala Bidang Keuangan</span>
                                  </div>
                                </div>
                                ';
                              }
                            }
                        @endphp
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-2">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                  @if (isset($_GET['p']) && $_GET['p']== 'sekretariat')
                    <p class="fst-italic">Tugas Pokok Sekretariat memiliki tugas menyelenggarakan administrasi umum, perlengkapan, kerumahtanggaan, kelembagaan, kehumasan,        kepegawaian, keuangan, dan program di lingkungan Dispaperta.</p>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'ketahanan-pangan')
                    <p class="fst-italic">Bidang Ketahanan Pangan memiliki tugas melaksanakan sebagian tugas Kepala Dinas dalam koordinasi, bimbingan, suvervisi dan konsultasi, perencanaan, penelitian, pemantauan, pengembangan dan pengawasan dalam urusan ketahanan pangan.</p>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'tanaman-pangan')
                    <p class="fst-italic">Bidang Tanaman Pangan mempunyai tugas melaksanan sebagian tugas Kepala Dinas dalam koordinasi, bimbingan, supervisi dan konsultasi, perencanaan, penelitian, pemantauan, pengembangan dan pengawasan serta evaluasi di bidang tanaman pangan.</p>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'hortikultura')
                  <p class="fst-italic">Bidang Hortikultura mempunyai tugas melaksanakan sebagian Kepala Dinas dalam koordinasi, bimbingan, supervisi dan konsultasi, perencanaan , penelitian , pemantauan, pengembangan dan pengawasan di bidang hortikultura.</p>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'perkebunan')
                  <p class="fst-italic">Bidang Perkebunan mempunyai tugas melaksanakan sebagian tugas Kepala Dinas dalam menyusun kebijakan di bidang perkebunan, melaksanakan rehabilitasi, diversifikasi lahan dan infrastruktur perkebunan serta menyelenggarakan perlindungan tanaman perkebunan dan penatausahaan perkebunan.</p>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'pskp')
                  <p class="fst-italic">Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian mempunyai tugas melaksanakan sebagian tugas Kepala Dinas dalam menyusun, menyiapkan, melaksanakan, mengkoordinasikan, memfasilitasi, mengatur, memantau dan mengevaluasi serta melaporkan kegiatan dibidang pengembangan sumberdaya manusia dan kelembagaan pertanian.</p>
                  @endif
                  </div>
                </div>
              </div>
              <div class="tab-pane active show" id="tab-3">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                    <div align="justify">
                  @if (isset($_GET['p']) && $_GET['p']== 'sekretariat')
                    <ol>
                    	<li>Pelaksanaan Penyusunan dan pengembangan kebijakan teknis dan program kerja pada Sekretariat;</li>
                    	<li>Pengkoordinasian dan penyiapan bahan penyusunan perencanaan dan program kerja bidang secara terpadu;</li>
                    	<li>Pelaksanaan upaya peningkatan pelayanan publik di bidang kesekretariatan;</li>
                    	<li>Pengelolaan dan pengendalian administrasi umum, administrasi kepegawaian dan administrasi keuangan;</li>
                    	<li>Pelaksanaan urusan kerumahtanggaan dan perlengkapan;</li>
                    	<li>Pelaksanaan urusan organisasi, tatalaksana dan kehumasan;</li>
                    	<li>Pelayanan teknis administratif kepada Kepala Dinas dan semua satuan unit kerja di lingkungan Dispaperta;
                    	</li>
                    	<li>Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas kesekretariatan dan dinas; dan <br></li>
                    	<li>Pelaksanaan tugs kedinasan lain yang diberikan oleh atasan </li>
                    </ol>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'ketahanan-pangan')
                    <ol>
                    	<li>Pelaksanaan Penyusunan dan pengembangan kebijakan teknis perencanaan dan program kerja pada bidang Ketahanan Pangan ;</li>
                    	<li>Penyelenggaraan upaya peningkatan pelayanan publik di bidang Ketahanan Pangan;</li>
                    	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan, dan pengendalian pelaksanaan kegiatan ketersediaan, distribusi, dan kawanan pangan;</li>
                    	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan keanekaragaman konsumsi pangan;</li>
                    	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan keamanan pangan;</li>
                    	<li>Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas pada bidang Ketahanan Pangan;</li>
                    	<li>Pelaksanaan tugas kedinasan lain yang diberikan oleh atasan.</li>
                    </ol>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'tanaman-pangan')
                        <ol>
                        	<li>&nbsp;Pelaksanaan penyusunan dan pengembangan kebijakan teknis perencanaan dan program kerja pada Bidang Tanaman Pangan;</li>
                        	<li>&nbsp;Penyelenggaraan upaya peningkatan pelayanan publik di bidang Tanaman Pangan;</li>
                        	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan sarana prasarana tanaman pangan;</li>
                        	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan produksi&nbsp; dan perlindungan tanaman pangan;</li>
                        	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegitan pasca panen dan agribisnis tanaman pangan;</li>
                        	<li>&nbsp;Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas pada Bidang Tanaman Pangan; dan</li>
                        	<li>&nbsp;Pelaksanaan tugas kedinasan lain yang diberikan oleh atsan.<br>
                        	</li>
                        </ol>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'hortikultura')
                    <ol>
                    	<li>Pelaksanaan penyusunan dan pengembangan kebijakan teknis perencanaan dan program kerja pada Bidang Hortikultura;</li>
                    	<li>&nbsp;Penyelenggaraan upaya peningkatan pelayanan publik di bidan;</li>
                    	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan sarana prasarana hortikultura;</li>
                    	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan, dan pengendalian pelaksanaan kegiatan produksi dan perlindungan hortikultura;</li>
                    	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan,, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan pasca panen dan agribisnis hortikultura;</li>
                    	<li>&nbsp;Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas pada Bidang Hortikultura;</li>
                    	<li>&nbsp;Pelaksanaan tugas kedinasan lain yang di berikan oleh atsan.<br>
                    	</li>
                    </ol>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'perkebunan')
                    <ol>
                    	<li>Penyusunan kebijakan teknis, perencanaan dan program kerja pada Bidang Perkebunan;</li>
                    	<li>&nbsp;Penyelenggaraan upaya peningkatan pelayanan publik di Bidang Perkebunan;</li>
                    	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan Produksi Perkebunan;</li>
                    	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan perlindungan tanaman dan sarana prasarana perkebunan ;</li>
                    	<li>Pelaksanaan perencanaan, pembinnaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan pasca panen dan agribisnis perkebunan;</li>
                    	<li>&nbsp;Pelaksanaan monitoring, evaluasi dan pelaporan pelaksanaan tugas pada bidang Perkebunan; dan</li>
                    	<li>&nbsp;Pelaksanaan tugas kedinasan lain yang diberikan oleh atasan.<br>
                    	</li>
                    </ol>
                  @elseif (isset($_GET['p']) && $_GET['p'] == 'pskp')
                  <ol>
                	<li>Penyusunan Kebijakan teknis, perencanaan dan program kerja pada Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian ;</li>
                	<li>Penyelenggaraan upaya peningkatan pelayanan publik di bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian;</li>
                	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan kelembagaan pertanian;</li>
                	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan sumber daya manusia penyuluhan;</li>
                	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksannaan kegiatan penyelenggaraan penyuluhan;</li>
                	<li>Pelaksanaan&nbsp; monitoring, evaluasi dan laporan pelaksanaan tugas pada Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian; dan</li>
                	<li>Pelaksanaan tugas kedinasan lain yang diberikan oleh atasan.<br>
                	</li>
                </ol>
                  @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Specials Section -->
@endsection
