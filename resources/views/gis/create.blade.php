@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 50rem;">
            <div class="card-header">
            Tambah Data GIS
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('gis.store') }}" enctype="multipart/form-data" id="myForm">
            @csrf
                <div class="form-group">
                    <label for="name">Nama</label>                    
                    <input type="text" name="nama" class="form-control" id="nama" aria-describedby="nama" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Alamat</label>                    
                    <input type="text" name="alamat" class="form-control" id="alamat" aria-describedby="alamat" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Telp.</label>                    
                    <input type="text" name="telp" class="form-control" id="telp" aria-describedby="telp" value="0" required="required">                
                </div>
                <div class="form-group">
                    <label for="lat">lat</label>
                    <input type="text" name="lat" placeholder="lat" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label for="lng">lng</label>
                    <input type="text" name="lng" placeholder="lng" class="form-control" required="required">
                </div>
                
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="{{ route('pages')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
@endsection