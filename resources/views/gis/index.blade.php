@extends('layouts.user_layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Manajemen GIS</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-primary" href="{{ route('gis.show') }}"> Lihat Peta GIS</a>
                <a class="btn btn-success" href="{{ route('gis.create') }}"> Tambah Data GIS</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <div class="container">
    <table class="table table-bordered data-table">
      <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Telp</th>
            <th>Lat</th>
            <th>Lang</th>
            <th width="280px">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
   </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function () {   
    $('.data-table').DataTable({
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('gis') }}",
        "deferRender": true,
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'nama', name: 'nama'},
            {data: 'alamat', name: 'alamat'},
            {data: 'telp', name: 'telp'},
            {data: 'lat', name: 'lat'},
            {data: 'lng', name: 'lng'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script>