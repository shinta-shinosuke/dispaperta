@extends('layouts.user_layout')
  
@section('content')

<div class="container mt-5">
    <div class="row justify-content-center align-items-center">
            <div class="col-md-12">
                <h2>{{ $pages->judul }}</h2>
                <p>{!! $pages->content !!}</p>
            </div>
            <a class="btn btn-success mt-3" href="{{ url('articles/edit/'.$pages->id) }}">Edit</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-warning mt-3" href="{{ route('articles') }}">Kembali</a>

        </div>
    </div>
</div>
<script src="{{ asset( 'assets/js/jquery-1.9.1.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function () {   
    var base_url = {!! json_encode(url('/')) !!};
    var src = $('img').attr('src');
    $('img').removeAttr('src');
    $('img').attr('src',base_url+src);
    });
</script>

@endsection 