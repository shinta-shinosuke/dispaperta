@extends('layouts.user_layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Manajemen Artikel</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-success" href="{{ route('articles.create') }}"> Tambah Data Artikel</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <div class="container">
    <table class="table table-bordered data-table">
      <thead>
        <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Url</th>
            <th>Content</th>
            <th>Image Feature</th>
            <th width="280px">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
   </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function () {   
    $('.data-table').DataTable({
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('articles') }}",
        "deferRender": true,
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'judul', name: 'judul'},
            {data: 'slug', name: 'slug'},
            {data: 'content', "render": function ( data, type, row, meta ) {
                return type === 'display' && data.length > 40 ?
        '<span title="'+data+'">'+data.substr( 0, 100 )+'...</span>' :
        data;
            }},
            {data: 'img', name: 'img'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script>