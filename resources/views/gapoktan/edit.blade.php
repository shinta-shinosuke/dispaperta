@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 1024rem;">
            <div class="card-header">
            Edit Data Gapoktan / Poktan
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @foreach ($gapoktan as $g)
                <form method="post" action="{{ route('gapoktan.update', $g['id']) }}" id="myForm">
                @csrf
                @method('PUT')
                    <div class="form-group">
                    <label for="name">Tanggal</label>                    
                    <label for="name">{{ date('d-m-Y H:i:s') }}</label>         
                </div>
                <input type="hidden" name="id" class="form-control" id="id" value="{{ $g['id'] }}" required="required">   
                <input type="hidden" name="filename" class="form-control" id="filename" value="{{ $g['nama_file'] }}" required="required">   
                <input type="hidden" name="extensi" class="form-control" id="extensi" value="{{ $g['extensi'] }}" required="required">   
                <div class="form-group">
                    <label for="kecamatan">{{ __('Kecamatan') }}</label>
                        <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                            <option value="">-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $kecamatan)
                            <option {{ $g['id_kecamatan'] == $kecamatan['id_kecamatan'] ? 'selected' : ''}} value={{ $kecamatan['id_kecamatan'] }}>{{ $kecamatan['kecamatan'] }}</option>
                            @endforeach
                        </select>    
                                
                        @error('kecamatan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="kecamatan">{{ __('Desa') }}</label>
                        <select name="desa" class="form-control" id="desa" aria-describedby="desa" required="required">
                            <option value="">-- Pilih Desa --</option>
                            @foreach ($desa as $desa)
                            <option {{ $g['id_desa'] == $desa['id_desa'] ? 'selected' : ''}} value={{ $desa['id_desa'] }}>{{ $desa['desa'] }}</option>
                            @endforeach
                        </select>    
                                
                        @error('desa')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="name">No Induk</label>                    
                      <input type="text" name="no_induk" class="form-control" id="no_induk" aria-describedby="no_induk" value="{{ $g['no_induk'] }}" required="required">   
                    </div>
                    
                    <div class="col-md-6">
                      <label for="name">Nama</label>                       
                      <input type="text" name="nama" class="form-control" id="nama" aria-describedby="nama" value="{{ $g['nama'] }}" required="required">  
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                                 
                </div>
                
                <div class="form-group">
                    <label for="name">Jenis Kelembagaan</label>  
                    <div class="row">
                        <div class="input-group col-md-6">
                            <span class="input-group-addon">
                              <input type="radio" name="pelaku" value="Pelaku Utama" {{ $g['jenis_kelembagaan'] == 'Pelaku Utama' ? 'checked' : '' }} >  
                            </span>
                            <label class="form-control">Pelaku Utama</label>
                        </div>
                        <div class="input-group col-md-6">
                            <span class="input-group-addon">
                              <input type="radio" name="pelaku" value="Pelaku Usaha" {{ $g['jenis_kelembagaan'] == 'Pelaku Usaha' ? 'checked' : '' }} >  
                            </span>
                            <label class="form-control">Pelaku Usaha</label>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="col-md-6">
                            <label for="utama">{{ __('Utama') }}</label>
                            <select name="utama" class="form-control" id="utama" aria-describedby="utama">
                                <option value="" {{ $g['utama'] == '' ? 'selected' : '' }} >-----</option>
                                <option value="GAPOKTAN" {{ $g['utama'] == 'GAPOKTAN' ? 'selected' : '' }} >GAPOKTAN</option>
                                <option value="POKTAN" {{ $g['utama'] == 'POKTAN' ? 'selected' : '' }} >POKTAN</option>
                                <option value="POKDAKAN" {{ $g['utama'] == 'POKDAKAN' ? 'selected' : '' }} >POKDAKAN</option>
                                <option value="KEL. TANI IKAN" {{ $g['utama'] == 'KEL. TANI IKAN' ? 'selected' : '' }} >KEL. TANI IKAN</option>
                                <option value="PEMUDA TANI" {{ $g['utama'] == 'PEMUDA TANI' ? 'selected' : '' }} >PEMUDA TANI</option>
                                <option value="KWT" {{ $g['utama'] == 'KWT' ? 'selected' : '' }} >KWT</option>
                                <option value="KTHR" {{ $g['utama'] == 'KTHR' ? 'selected' : '' }} >KTHR</option>
                                <option value="LMDH" {{ $g['utama'] == 'LMDH' ? 'selected' : '' }} >LMDH</option>
                                <option value="POKNAK" {{ $g['utama'] == 'POKNAK' ? 'selected' : '' }} >POKNAK</option>
                                <option value="FMA" {{ $g['utama'] == 'FMA' ? 'selected' : '' }} >FMA</option>
                                <option value="POKHUT" {{ $g['utama'] == 'POKHUT' ? 'selected' : '' }} >POKHUT</option>
                                <option value="UPG" {{ $g['utama'] == 'UPG' ? 'selected' : '' }} >UPG</option>
                                <option value="KUB" {{ $g['utama'] == 'KUB' ? 'selected' : '' }} >KUB</option>
                                <option value="KTP" {{ $g['utama'] == 'KTP' ? 'selected' : '' }} >KTP</option>
                                <option value="P3A" {{ $g['utama'] == 'P3A' ? 'selected' : '' }} >P3A</option>
                                <option value="POKBUN" {{ $g['utama'] == 'POKBUN' ? 'selected' : '' }} >POKBUN</option>
                                <option value="AFINITAS / MAPAN" {{ $g['utama'] == 'AFINITAS / MAPAN' ? 'selected' : '' }} >AFINITAS / MAPAN</option>
                                <option value="LMPA" {{ $g['utama'] == 'LMPA' ? 'selected' : '' }} >LMPA</option>
                                <option value="LKMA" {{ $g['utama'] == 'LKMA' ? 'selected' : '' }} >LKMA</option>
                                <option value="PAGUYUBAN" {{ $g['utama'] == 'PAGUYUBAN' ? 'selected' : '' }} >PAGUYUBAN</option>
                            </select>    
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="col-md-6">
                            <label for="usaha">{{ __('Usaha') }}</label>
                            <select name="usaha" class="form-control" id="usaha" aria-describedby="usaha">
                                <option value="" {{ $g['usaha'] == '' ? 'selected' : '' }} >-----</option>
                                <option value="KWT" {{ $g['usaha'] == 'KWT' ? 'selected' : '' }} >KWT</option>
                                <option value="KUB" {{ $g['usaha'] == 'KUB' ? 'selected' : '' }} >KUB</option>
                                <option value="ASOSIASI" {{ $g['usaha'] == 'ASOSIASI' ? 'selected' : '' }} >ASOSIASI</option>
                                <option value="P4K" {{ $g['usaha'] == 'P4K' ? 'selected' : '' }} >P4K</option>
                                <option value="P4S" {{ $g['usaha'] == 'P4S' ? 'selected' : '' }} >P4S</option>
                                <option value="UPG" {{ $g['usaha'] == 'UPG' ? 'selected' : '' }} >UPG</option>
                                <option value="P3A" {{ $g['usaha'] == 'P3A' ? 'selected' : '' }} >P3A</option>
                                <option value="KTP" {{ $g['usaha'] == 'KTP' ? 'selected' : '' }} >KTP</option>
                                <option value="LMDH" {{ $g['usaha'] == 'LMDH' ? 'selected' : '' }} >LMDH</option>
                                <option value="AFINITAS / MAPAN" {{ $g['usaha'] == 'AFINITAS / MAPAN' ? 'selected' : '' }} >AFINITAS / MAPAN</option>
                                <option value="LKMA" {{ $g['usaha'] == 'LKMA' ? 'selected' : '' }} >LKMA</option>
                                <option value="KEL. LEBAH" {{ $g['usaha'] == 'KEL. LEBAH' ? 'selected' : '' }} >KEL. LEBAH</option>
                                <option value="PUAP" {{ $g['usaha'] == 'PUAP' ? 'selected' : '' }} >PUAP</option>
                                <option value="KPK" {{ $g['usaha'] == 'KPK' ? 'selected' : '' }} >KPK</option>
                            </select>    
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="kelas kelompok">{{ __('Kelas Kelompok') }}</label>
                        <select name="kelas_kelompok" class="form-control" id="kelas_kelompok" aria-describedby="kelas_kelompok" required="required">
                            <option value="" {{ $g['kelas_kelompok'] == '' ? 'selected' : '' }} >-- Pilih Kelompok --</option>
                            <option value="Pemula" {{ $g['kelas_kelompok'] == 'Pemula' ? 'selected' : '' }} >Pemula</option>
                            <option value="Lanjut" {{ $g['kelas_kelompok'] == 'Lanjut' ? 'selected' : '' }} >Lanjut</option>
                            <option value="Madya" {{ $g['kelas_kelompok'] == 'Madya' ? 'selected' : '' }} >Madya</option>
                            <option value="Utama" {{ $g['kelas_kelompok'] == 'Utama' ? 'selected' : '' }} >Utama</option>
                        </select> 
                </div>
                
                <div class="form-group">
                    <label for="name">Jumlah Anggota</label>                    
                    <div class="row">
                      <div class="input-group col-md-3">
                        <input type="number" name="jumlah_anggota" class="form-control" id="jumlah_anggota" aria-describedby="jumlah_anggota" value="{{ $g['jumlah_anggota'] }}" required="required">
                      </div>
                        
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="anggota" value="Kelompok" {{ $g['jenis_jumlah'] == 'Kelompok' ? 'checked' : '' }} >  
                            </span>
                            <label class="form-control">Kelompok</label>
                        </div>
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="anggota" value="Orang" {{ $g['jenis_jumlah'] == 'Orang' ? 'checked' : '' }} >  
                            </span>
                            <label class="form-control">Orang</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Luas Areal</label>                    
                    <div class="row">
                      <div class="input-group col-md-3">
                        <input type="number" name="luas_areal" class="form-control" id="luas_areal" aria-describedby="luas_areal" value="{{ $g['luas_areal'] }}" required="required">
                      </div>
                        
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="luas" value="Ha"  {{ $g['jenis_areal'] == 'Ha' ? 'checked' : '' }} >  
                            </span>
                            <label class="form-control">Ha</label>
                        </div>
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="luas" value="Unit"  {{ $g['jenis_areal'] == 'Unit' ? 'checked' : '' }} >  
                            </span>
                            <label class="form-control">Unit</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Ketua</label>                    
                    <input type="text" name="ketua" class="form-control" id="ketua" aria-describedby="ketua" value="{{ $g['ketua'] }}" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="name">Sekretaris</label>                    
                    <input type="text" name="sekretaris" class="form-control" id="sekretaris" aria-describedby="sekretaris" value="{{ $g['sekretaris'] }}" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="name">Bendahara</label>                    
                    <input type="text" name="bendahara" class="form-control" id="bendahara" aria-describedby="bendahara" value="{{ $g['bendahara'] }}" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="name">Tahun Pembentukan</label>             
                      <div class="input-group col-md-2" style="margin-left:-15px">
                        <input type="number" name="tahun_pembentukan" class="form-control" id="tahun_pembentukan" aria-describedby="tahun_pembentukan" value="{{ $g['tahun_pembentukan'] }}" required="required">
                      </div>
                </div>
                      
                <div class="form-group"> 
                    <div class="row">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Tanaman Pangan" {{ $g['jenis_pembentukan'] == 'Tanaman Pangan' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Tanaman Pangan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Perkebunan" {{ $g['jenis_pembentukan'] == 'Perkebunan' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Perkebunan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Prikanan Tangkap" {{ $g['jenis_pembentukan'] == 'Prikanan Tangkap' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Prikanan Tangkap</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Peternakan" {{ $g['jenis_pembentukan'] == 'Peternakan' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Peternakan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Kehutanan" {{ $g['jenis_pembentukan'] == 'Kehutanan' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Kehutanan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Pengolahan Hasil" {{ $g['jenis_pembentukan'] == 'Pengolahan Hasil' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Pengolahan Hasil</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Hortikultura" {{ $g['jenis_pembentukan'] == 'Hortikultura' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Hortikultura</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Budidaya Ikan" {{ $g['jenis_pembentukan'] == 'Budidaya Ikan' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Budidaya Ikan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Lainnya" {{ $g['jenis_pembentukan'] == 'Lainnya' ? 'checked' : '' }}>  
                            </span>
                            <label class="form-control">Lainnya</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="name">HP</label>             
                      <div class="input-group">
                        <input type="text" name="hp" class="form-control" id="hp" aria-describedby="hp" value="{{ $g['hp'] }}" required="required">
                      </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Email</label>             
                      <div class="input-group">
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="email" value="{{ $g['email'] }}" required="required">
                      </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Input Dokumen</label> 
                    <input type="file" class="form-control" name="file">
                </div>
                
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="{{ route('kecamatan')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function () { 
    var kecID = '';
    $('#kecamatan').change(function(){
    var kecID = $(this).val();    
    //alert(kecID);
    if(kecID){
        $.ajax({
           type:"GET",
           //url:"getdesa/"+kecID,
           url:"{{ url('/gapoktan/getdesa/') }}/"+kecID,
           dataType: 'JSON',
           success:function(res){               
            if(res){
                $("#desa").empty();
                $("#desa").append('<option>-- Pilih Desa --</option>');
                $.each(res,function(desa,id_desa){
                    $("#desa").append('<option value="'+id_desa+'">'+desa+'</option>');
                });
            }else{
               $("#desa").empty();
            }
           }
        });
    }else{
        $("#desa").empty();
    }      
   });
});
</script>