@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data User
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('users.store') }}" id="myForm">
            @csrf
                 <div class="form-group">
                    <label for="name">Nama Lengkap</label>                    
                    <input type="text" name="fullname" class="form-control" id="fullname" aria-describedby="fullname" >                
                </div>
                <div class="form-group">
                    <label for="name">Username</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" >                
                </div>
                <div class="form-group">
                    <label for="password">Password</label>                    
                    <input type="password" name="password" class="form-control" id="password" aria-describedby="password" >                
                </div>
               
                <div class="form-group">
                    <label for="email">Email</label>                    
                    <input type="email" name="email" class="form-control" id="email" aria-describedby="email" >                
                </div>
                <div class="form-group">
                    <label for="name">No. Telephone</label>                    
                    <input type="text" name="phone" class="form-control" id="phone" aria-describedby="phone" >                
                </div>
                <div class="form-group">
                    <label for="name">Level</label>                   
                    <select name="level" class="form-control" id="level" aria-describedby="level">
                        <option value="Admin">Admin</option>
                        <option value="Admin Bidang">Admin Bidang</option>
                    </select>    
                </div>
                <div class="form-group kecamatan">
                    <label for="name">Kecamatan</label>                   
                    <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan">
                        <option value="">-- Pilih Kecamatan --</option>
                        @foreach ($kecamatan as $kecamatan)
                            <option value="{{ $kecamatan['id_kecamatan'] }}">{{ $kecamatan['kecamatan'] }}</option>
                        @endforeach
                    </select>    
                </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="{{ route('users')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
@endsection
<script type="text/javascript">
    $(document).on('change', '#level', function() {
        var level = $(this).val();

        if(level != 'Admin'){
            $('.kecamatan').hide();
        }else{
            $('.kecamatan').somehow();
        }
    });
</script>