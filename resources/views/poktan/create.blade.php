@extends('layouts.user_layout')
  
@section('content')   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 34rem;">
            <div class="card-header">
            Tambah Data POKTAN
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('poktan.store') }}" id="myForm">
            @csrf
                <div class="form-group">
                    <label for="name">Nama POKTAN</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="kecamatan">{{ __('GAPOKTAN') }}</label>
                        <input type="hidden" name="gapoktan_nama" class="form-control" id="gapoktan_nama">
                        <select name="gapoktan" class="form-control" id="gapoktan" aria-describedby="gapoktan" required="required">
                            <option value="">-- Pilih GAPOKTAN --</option>
                            @foreach ($gapoktan as $gapoktan)
                            <option value="{{ $gapoktan->id }}">{{ $gapoktan->nama_gapoktan }}</option>
                            @endforeach
                        </select>    
                </div>

                <div class="form-group">
                    <label for="name">Kecamatan</label>                    
                    <input type="text" name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" disabled>                
                </div>

                <div class="form-group">
                    <label for="name">Desa</label>                    
                    <input type="hidden" name="kode_desa" class="form-control" id="kode_desa" aria-describedby="kode_desa">                
                    <input type="text" name="desa" class="form-control" id="desa" aria-describedby="desa" disabled>                
                </div>

                <div class="form-group">
                    <label for="name">Penyuluh</label>                    
                    <input type="text" name="penyuluh" class="form-control" id="penyuluh" aria-describedby="penyuluh" disabled>                
                </div>

            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="{{ route('desa')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
    <script src="{{ asset( 'assets/js/jquery-1.9.1.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
            $.ajaxSetup({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
             });
     $('#gapoktan').change(function(){
        var id = $(this).val();

          $.ajax({
              url: "{{ route('poktan.check') }}",
              type: 'post',
              data: { id : id},
              success: function(response)
              {
                console.log(response);
                $('input[name="gapoktan_nama"]').val(response['gapoktan_nama']);
                $('input[name="kecamatan"]').val(response['kecamatan_nama']);
                $('input[name="desa"]').val(response['desa_nama']);
                $('input[name="kode_desa"]').val(response['desa_kode']);
                $('input[name="penyuluh"]').val(response['penyuluh_nama']);
              },
              error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error");
                }
            });
     });
});
</script>
@endsection