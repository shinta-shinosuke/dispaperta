@extends('layouts.user_layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Manajemen POKTAN</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-success" href="{{ route('poktan.create') }}"> Tambah Data POKTAN</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <div class="container">
    <!--<div class="row">
    <div class="col-md-12">
        <div class="search_filter">
            Search:&nbsp;&nbsp;<input type="text" id="datatable-search">
        </div>
    </div>
    </div>-->
    <table class="table table-bordered data-table">
      <thead>
        <tr>
            <th>Nama</th>
            <th>Gapoktan</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Penyuluh</th>
            <th width="280px">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
   </div>
@endsection
<script src="{{ asset( 'assets/js/jquery-1.9.1.js') }}"></script>
<script type="text/javascript">
var tabel = null;
    $(document).ready(function() {
            $.ajaxSetup({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
             });
        $('.data-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":
            {
                "url": "{{ route('poktan') }}",
                //data: function (d) {
                //    d.search = $('#datatable-search').val();
                //}
            },
            "deferRender": true,
            "columns": [
                { "data": "poktan_nama" }, 
                { "data": "gapoktan_nama" },
                { "data": "kecamatan_nama" },
                { "data": "desa_nama" },
                { "data": "penyuluh_nama" },
                { "data": "action", orderable: false, searchable: false},
            ],
        });

    });
</script>