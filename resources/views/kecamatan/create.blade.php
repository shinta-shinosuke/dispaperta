@extends('layouts.user_layout')
  
@section('content')

<?php
foreach($kecamatan as $kecamatan){


}
$no = $kecamatan['kode']+1;
?>   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data Kecamatan
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('kecamatan.store') }}" id="myForm">
            @csrf
                <div class="form-group">
                    <input type="hidden" name="kode" class="form-control" id="kode" aria-describedby="kode" value="{{ $no }}">
                    <label for="name">Nama Kecamatan</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" required="required">                
                </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="{{ route('kecamatan')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
@endsection