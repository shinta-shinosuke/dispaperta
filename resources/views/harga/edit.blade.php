@extends('layouts.user_layout')
  
@section('content')
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Harga Pasar
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form method="post" action="{{ route('harga.update', $harga->id) }}" id="myForm">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">BLTH</label>                    
                    <input type="text" name="blth" class="form-control" id="blth" aria-describedby="blth" value="{{ $harga->blth }}" laceholder="mmyyyy" maxlength="6" readonly="readonly">                
                </div>
                <div class="form-group">
                    <label for="name">Nama Sembako</label>
                    <select name="sembako" class="form-control" id="sembako" aria-describedby="sembako" required="required">
                        <option value="">-- Pilih Sembako --</option>
                        @foreach ($sembako as $sembako)
                           <option {{ $harga->id_sembako == $sembako['id'] ? 'selected' : ''}} value={{ $sembako['id'] }}>{{ $sembako['sembako'] }}</option>
                        @endforeach
                    </select>  
                </div>
                <div class="form-group">
                    <label for="name">Harga Sebelum</label>                    
                    <input type="number" name="harga_sebelum" class="form-control" id="harga_sebelum" value="{{ $harga->harga_sebelum }}" aria-describedby="harga_sebelum" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Harga Sesudah</label>                    
                    <input type="number" name="harga_sesudah" class="form-control" id="harga_sesudah" value="{{ $harga->harga_sesudah }}" aria-describedby="harga_sesudah" required="required">                
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="{{ route('harga')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection