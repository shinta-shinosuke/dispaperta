@extends('layouts.user_layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Manajemen Data Jenis Alsintan</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-success" href="{{ route('alsintan.create_jenis') }}"> Tambah Data Jenis Alsintan</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   <div class="container">
    <table class="table table-bordered data-table">
      <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th width="280px">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
   </div>
@endsection
<script src="{{ asset( 'assets/js/jquery-1.9.1.js') }}"></script>
<script type="text/javascript">
var tabel = null;
    $(document).ready(function() {
            $.ajaxSetup({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
             });
        $('.data-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":
            {
                "url": "{{ route('alsintan.jenis') }}",
            },
            "deferRender": true,
            "columns": [
                { "data": "id"},
                { "data": "jenis_alsintan"},
                { "data": "action", orderable: false, searchable: false},
            ],
        });

    });
</script>