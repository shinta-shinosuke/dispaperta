@extends('layouts.user_layout')
  
@section('content')

<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Alsintan
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <form method="post" action="{{ route('alsintan.update', $alsintan['id_alsintan']) }}" id="myForm">
                @csrf
                @method('PUT')
                    <div class="form-group">
                        <label for="name">Nama Alsintan</label>                    
                        <input type="text" name="name" class="form-control" id="name" value="{{ $alsintan['nama_alsintan'] }}" aria-describedby="name" required="required">                
                    </div>
                    <div class="form-group kecamatan">
                        <label for="name">Jenis Alsintan</label>                   
                        <select name="jenis_alsintan" class="form-control" id="jenis_alsintan" aria-describedby="jenis_alsintan" required="required">
                            <option value="">-- Pilih Jenis Alsintan --</option>
                             @foreach($jenis_alsintan as $jenis)

                             <option {{ $alsintan['jenis_alsintan_id'] == $jenis->id ? 'selected' : ''}} value="{{ $jenis->id }}">{{ $jenis->jenis_alsintan }}</option>

                             @endforeach
                        </select>    
                    </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="{{ route('alsintan')}}"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection