<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ 'DISPAPERTA | Kabupaten Batang' }}</title>
        
        <!-- Favicons -->
      <link href="{{ url('/images/icon.png') }}" rel="icon">
      
       <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-4.1.3.min.css') }}" />
        <link href="{{ asset('assets/css/jquery-1.10.16.dataTables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/dataTables-1.10.19.bootstrap4.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/mapbox-gl-v.1.4.0.css') }}" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

        <script src="{{ asset('assets/js/jquery-1.9.1.js') }}"></script>  
        <script src="{{ asset('assets/js/jquery-1.19.0.validate.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-1.10.16.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap-4.1.3.min.js') }}"></script>
        <script src="{{ asset('assets/js/dataTables.bootstrap4-1.10.19.min.js') }}"></script>
        <script src="{{ asset('assets/js/mapbox-gl-v1.4.0.js') }}"></script>
        
        <style type="text/css">
            .mapboxgl-popup {
                max-width: 400px;
                font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
            }
            #map {
                width: 100%;
                height: 500px;
            }
            .marker {
                background-image: url('/dispaperta/public/images/point.png');
                background-repeat:no-repeat;
                background-size:100%;
                width: 50px;
                height: 100px;
                cursor: pointer; 
            }
            .dataTables_wrapper .dataTables_paginate .paginate_button { 
                box-sizing: border-box;
                display: inline-block;
                min-width: 1.5em;
                padding: 0px 0px;
                margin-left: 2px;
                text-align: center;
                text-decoration: none !important;
                cursor: pointer;
                *cursor: hand;
                color: #333 !important;
                border: 1px solid transparent;
                border-radius: 2px;
            }
            .bg-black{
                background-color: #000;
            }
            .navbar-light .navbar-brand {
                color: rgba(255,255,255);
            }
            .navbar-light .navbar-toggler {
                color: rgba(255,255,255);
                border-color: rgba(255,255,255);
            }
            .navbar-light .navbar-nav .nav-link {
                color: rgba(255,255,255);
            }
            .input-group-addon {
                padding: 6px 12px;
                font-size: 14px;
                font-weight: 400;
                line-height: 1;
                color: #555;
                text-align: center;
                background-color: #eee;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            
            .input-group-addon, .input-group-btn {
                width: 1%;
                white-space: nowrap;
                vertical-align: middle;
            }
            
            .input-group .form-control, .input-group-addon, .input-group-btn {
                display: table-cell;
            }
            .dataTables_wrapper .dataTables_filter {
                float: right;
                text-align: right;
                /*visibility: hidden;*/
            }

            .search_filter {
                float: right;
                color: #333;
            }
            /*
            .navbar-expand-md .navbar-nav .dropdown-sub-menu {
                position: absolute;
            }

            .navbar-nav .dropdown-sub-menu {
                position: static;
                float: none;
            }
            .dropdown-sub-menu.show {
                display: block;
            }
            .dropdown-sub-menu-right {
                right: 0;
                left: auto;
            }*/
        </style>
    </head>
    <body>
         <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-black shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    @if (Auth::user()->level == 2)
                        {{ 'SIKEP' }}
                    @elseif (Auth::user()->level == 3)
                        {{ 'BLOG' }}
                    @else
                        {{ 'DISPAPERTA' }}
                    @endif
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                       
                        @guest
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('/sikep/register/') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @if (Auth::user()->level == 3)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('pages') }}">{{ __('Halaman') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('articles') }}">{{ __('Artikel') }}</a>
                            </li>
                            @endif
                            @if (Auth::user()->level == 2)
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Pertanian
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="{{ route('desa') }}">{{ __('Desa') }}</a>
                                    <a class="dropdown-item"  href="{{ route('kecamatan') }}">{{ __('Kecamatan') }}</a>    
                                    <a class="dropdown-item"  href="{{ route('alsintan.jenis') }}">{{ __('Jenis Alsintan') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('alsintan') }}">{{ __('Alsintan') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('usaha.jenis') }}">{{ __('Jenis Usaha') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('komoditas') }}">{{ __('Komoditas') }}</a> 
                                    <a class="dropdown-item"  href="{{ route('refkomoditas') }}">{{ __('Ref Komoditas') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('kios') }}">{{ __('Kios') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('poktan') }}">{{ __('POKTAN') }}</a>  
                                    <!--<a class="dropdown-item" href="{{ route('gapoktan') }}">{{ __('Gapoktan') }}</a>    -->
                                    <a class="dropdown-item"  href="{{ route('penyuluh') }}">{{ __('Peyuluh') }}</a>  
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Official
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="{{ route('articles') }}">{{ __('Artikel') }}</a>
                                    <a class="dropdown-item"  href="{{ route('pages') }}">{{ __('Halaman') }}</a>
                                </div>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('sembako') }}">{{ __('Sembako') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('harga') }}">{{ __('Harga Pasar') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('download') }}">{{ __('Download') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('gis') }}">{{ __('GIS') }}</a>
                            </li>

                            @endif
                            @if (Auth::user()->level == 1)
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Pertanian
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="{{ route('desa') }}">{{ __('Desa') }}</a>
                                    <a class="dropdown-item"  href="{{ route('kecamatan') }}">{{ __('Kecamatan') }}</a>    
                                    <a class="dropdown-item"  href="{{ route('alsintan.jenis') }}">{{ __('Jenis Alsintan') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('alsintan') }}">{{ __('Alsintan') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('usaha.jenis') }}">{{ __('Jenis Usaha') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('komoditas') }}">{{ __('Komoditas') }}</a> 
                                    <a class="dropdown-item"  href="{{ route('refkomoditas') }}">{{ __('Ref Komoditas') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('kios') }}">{{ __('Kios') }}</a>  
                                    <a class="dropdown-item"  href="{{ route('poktan') }}">{{ __('POKTAN') }}</a>  
                                    <!--<a class="dropdown-item" href="{{ route('gapoktan') }}">{{ __('Gapoktan') }}</a>    -->
                                    <a class="dropdown-item"  href="{{ route('penyuluh') }}">{{ __('Peyuluh') }}</a>  
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Official
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="{{ route('articles') }}">{{ __('Artikel') }}</a>
                                    <a class="dropdown-item"  href="{{ route('pages') }}">{{ __('Halaman') }}</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('users') }}">{{ __('Users') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('sembako') }}">{{ __('Sembako') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('harga') }}">{{ __('Harga Pasar') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('download') }}">{{ __('Download') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('gis') }}">{{ __('GIS') }}</a>
                            </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            @yield('content')
        </div>
      </div>
    </body>
</html>