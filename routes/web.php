<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\SembakoController;
use App\Http\Controllers\HargaController;
use App\Http\Controllers\GISController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\MainController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::post('file-upload', [CKEditorController::class, 'fileUpload']);

Route::post('ckeditor/upload', [CKEditorController::class, 'upload'])->name('ckeditor.image-upload');

Route::get('upload',  [UploadController::class, 'upload'])->name('upload');
Route::post('/upload/proses',  [UploadController::class, 'proses_upload'])->name('proses_upload');

//Route::get('/', function () {
//    return view('main.index');
//});

Route::get('/', [App\Http\Controllers\MainController::class, 'index'])->name('main.index');

// ############################ SUPER USER ################################# //

Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('level');

// ############################ END SUPERUSER ############################### //

// ############################ BLOG ################################# //
// ####################### MAIN ################################ //

Route::get('/main', [App\Http\Controllers\MainController::class, 'index'])->name('main.index');
Route::get('/main/sekretariat/', [App\Http\Controllers\MainController::class, 'sekretariat'])->name('main.sekretariat');
Route::get('/main/ketahanan-pangan/', [App\Http\Controllers\MainController::class, 'ketahanan_pangan'])->name('main.ketahanan-pangan');
Route::get('/main/tanaman-pangan/', [App\Http\Controllers\MainController::class, 'tanaman_pangan'])->name('main.tanaman-pangan');
Route::get('/main/hortikultura/', [App\Http\Controllers\MainController::class, 'hortikultura'])->name('main.hortikultura');
Route::get('/main/perkebunan/', [App\Http\Controllers\MainController::class, 'perkebunan'])->name('main.perkebunan');
Route::get('/main/pskp/', [App\Http\Controllers\MainController::class, 'pskp'])->name('main.pskp');
Route::get('/main/download/', [App\Http\Controllers\MainController::class, 'download'])->name('main.download');
Route::get('/main/harga/', [App\Http\Controllers\MainController::class, 'harga'])->name('main.harga');
Route::get('/main/gis/', [App\Http\Controllers\MainController::class, 'gis'])->name('main.gis');
Route::get('/main/bidang/', [App\Http\Controllers\MainController::class, 'index'])->name('main.bidang');
Route::get('/main/artikel/{id}', [App\Http\Controllers\MainController::class, 'baca'])->name('baca');

// ################### ADMIN MANAGE BLOG PAGE ############################# //
Route::get('pages', [App\Http\Controllers\PageController::class, 'index'])->name('pages');
Route::get('/pages/create/', [App\Http\Controllers\PageController::class, 'create'])->name('pages.create');
Route::post('/pages/store/', [App\Http\Controllers\PageController::class, 'store'])->name('pages.store');
Route::get('/pages/show/{id}', [App\Http\Controllers\PageController::class, 'show'])->name('pages.show');
Route::get('/pages/edit/{id}', [App\Http\Controllers\PageController::class, 'edit'])->name('pages.edit');
Route::put('/pages/update/{id}', [App\Http\Controllers\PageController::class, 'update'])->name('pages.update');
Route::get('/pages/destroy/{id}', [App\Http\Controllers\PageController::class, 'destroy'])->name('pages.destroy');

// ################### ADMIN MANAGE ARTICLE PAGE ########################### //
Route::get('articles', [App\Http\Controllers\ArticleController::class, 'index'])->name('articles');
Route::get('/articles/create/', [App\Http\Controllers\ArticleController::class, 'create'])->name('articles.create');
Route::post('/articles/store/', [App\Http\Controllers\ArticleController::class, 'store'])->name('articles.store');
Route::get('/articles/show/{id}', [App\Http\Controllers\ArticleController::class, 'show'])->name('articles.show');
Route::get('/articles/edit/{id}', [App\Http\Controllers\ArticleController::class, 'edit'])->name('articles.edit');
Route::put('/articles/update/{id}', [App\Http\Controllers\ArticleController::class, 'update'])->name('articles.update');
Route::get('/articles/destroy/{id}', [App\Http\Controllers\ArticleController::class, 'destroy'])->name('articles.destroy');

// ################ ADMIN MANAGE DATA BLOG DOWNLOAD ######################### //

Route::get('download', [App\Http\Controllers\DownloadController::class, 'index'])->name('download');
Route::get('/download/create/', [App\Http\Controllers\DownloadController::class, 'create'])->name('download.create');
Route::post('/download/store/', [App\Http\Controllers\DownloadController::class, 'store'])->name('download.store');
Route::get('/download/edit/{id}', [App\Http\Controllers\DownloadController::class, 'edit'])->name('download.edit');
Route::post('/download/update/{id}', [App\Http\Controllers\DownloadController::class, 'update'])->name('download.update');
Route::get('/download/destroy/{id}', [App\Http\Controllers\DownloadController::class, 'destroy'])->name('download.destroy');

// ################### CREDENTIAL BLOG ################################# //
Route::get('/main/register/', [App\Http\Controllers\MainController::class, 'blog_register'])->name('main.blog_register');
Route::get('/main/login/', [App\Http\Controllers\MainController::class, 'blog_login'])->name('main.blog_login');
// ############################ END BLOG ################################# //


// ############################ SIKEP ################################# //
// ################# ADMIN MANAGE DATA GAPOKTAN ########################### //

Route::get('gapoktan', [App\Http\Controllers\GapoktanController::class, 'index'])->name('gapoktan');
Route::get('/gapoktan/create/', [App\Http\Controllers\GapoktanController::class, 'create'])->name('gapoktan.create');
Route::post('/gapoktan/store/', [App\Http\Controllers\GapoktanController::class, 'store'])->name('gapoktan.store');
Route::get('/gapoktan/edit/{id}', [App\Http\Controllers\GapoktanController::class, 'edit'])->name('gapoktan.edit');
Route::put('/gapoktan/update/{id}', [App\Http\Controllers\GapoktanController::class, 'update'])->name('gapoktan.update');
Route::get('/gapoktan/destroy/{id}', [App\Http\Controllers\GapoktanController::class, 'destroy'])->name('gapoktan.destroy');
Route::get('/gapoktan/getdesa/{id}', [App\Http\Controllers\GapoktanController::class, 'getDesa'])->name('gapoktan.getDesa');
Route::get('/gapoktan/showdata/{id}', [App\Http\Controllers\GapoktanController::class, 'showData'])->name('gapoktan.showData');
// ################# ADMIN MANAGE DATA KECAMATAN ########################### //

Route::get('kecamatan', [App\Http\Controllers\KecamatanController::class, 'index'])->name('kecamatan');
Route::get('/kecamatan/create/', [App\Http\Controllers\KecamatanController::class, 'create'])->name('kecamatan.create');
Route::post('/kecamatan/store/', [App\Http\Controllers\KecamatanController::class, 'store'])->name('kecamatan.store');
Route::get('/kecamatan/edit/{id}', [App\Http\Controllers\KecamatanController::class, 'edit'])->name('kecamatan.edit');
Route::put('/kecamatan/update/{id}', [App\Http\Controllers\KecamatanController::class, 'update'])->name('kecamatan.update');
Route::get('/kecamatan/destroy/{id}', [App\Http\Controllers\KecamatanController::class, 'destroy'])->name('kecamatan.destroy');


// ################# ADMIN MANAGE DATA DESA ########################### //

Route::get('desa', [App\Http\Controllers\DesaController::class, 'index'])->name('desa');
Route::get('/desa/create/', [App\Http\Controllers\DesaController::class, 'create'])->name('desa.create');
Route::post('/desa/store/', [App\Http\Controllers\DesaController::class, 'store'])->name('desa.store');
Route::get('/desa/edit/{id}', [App\Http\Controllers\DesaController::class, 'edit'])->name('desa.edit');
Route::put('/desa/update/{id}', [App\Http\Controllers\DesaController::class, 'update'])->name('desa.update');
Route::get('/desa/destroy/{id}', [App\Http\Controllers\DesaController::class, 'destroy'])->name('desa.destroy');
Route::get('/desa/fetch/', [App\Http\Controllers\DesaController::class, 'fetch'])->name('desa.fetch');

// ################# ADMIN MANAGE DATA poktan ########################### //

Route::get('poktan', [App\Http\Controllers\PoktanController::class, 'index'])->name('poktan');
Route::get('/poktan/create/', [App\Http\Controllers\PoktanController::class, 'create'])->name('poktan.create');
Route::post('/poktan/store/', [App\Http\Controllers\PoktanController::class, 'store'])->name('poktan.store');
Route::get('/poktan/edit/{id}', [App\Http\Controllers\PoktanController::class, 'edit'])->name('poktan.edit');
Route::put('/poktan/update/{id}', [App\Http\Controllers\PoktanController::class, 'update'])->name('poktan.update');
Route::get('/poktan/destroy/{id}', [App\Http\Controllers\PoktanController::class, 'destroy'])->name('poktan.destroy');
Route::post('/poktan/check/', [App\Http\Controllers\PoktanController::class, 'check'])->name('poktan.check');
//Route::get('ajaxRequest', [AjaxController::class, 'ajaxRequest']);
//Route::post('ajaxRequest', [AjaxController::class, 'ajaxRequestPost'])->name('ajaxRequest.post');

// ################# ADMIN MANAGE DATA kios ########################### //

Route::get('kios', [App\Http\Controllers\KiosController::class, 'index'])->name('kios');
Route::get('/kios/create/', [App\Http\Controllers\KiosController::class, 'create'])->name('kios.create');
Route::post('/kios/store/', [App\Http\Controllers\KiosController::class, 'store'])->name('kios.store');
Route::get('/kios/edit/{id}', [App\Http\Controllers\KiosController::class, 'edit'])->name('kios.edit');
Route::put('/kios/update/{id}', [App\Http\Controllers\KiosController::class, 'update'])->name('kios.update');
Route::get('/kios/destroy/{id}', [App\Http\Controllers\KiosController::class, 'destroy'])->name('kios.destroy');

// ################# ADMIN MANAGE DATA refkomoditas ########################### //

Route::get('refkomoditas', [App\Http\Controllers\RefkomoditasController::class, 'index'])->name('refkomoditas');
Route::get('/refkomoditas/create/', [App\Http\Controllers\RefkomoditasController::class, 'create'])->name('refkomoditas.create');
Route::post('/refkomoditas/store/', [App\Http\Controllers\RefkomoditasController::class, 'store'])->name('refkomoditas.store');
Route::get('/refkomoditas/edit/{id}', [App\Http\Controllers\RefkomoditasController::class, 'edit'])->name('refkomoditas.edit');
Route::put('/refkomoditas/update/{id}', [App\Http\Controllers\RefkomoditasController::class, 'update'])->name('refkomoditas.update');
Route::get('/refkomoditas/destroy/{id}', [App\Http\Controllers\RefkomoditasController::class, 'destroy'])->name('refkomoditas.destroy');


// ################# ADMIN MANAGE DATA KOMODITAS ########################### //

Route::get('komoditas', [App\Http\Controllers\KomoditasController::class, 'index'])->name('komoditas');
Route::get('/komoditas/create/', [App\Http\Controllers\KomoditasController::class, 'create'])->name('komoditas.create');
Route::post('/komoditas/store/', [App\Http\Controllers\KomoditasController::class, 'store'])->name('komoditas.store');
Route::get('/komoditas/edit/{id}', [App\Http\Controllers\KomoditasController::class, 'edit'])->name('komoditas.edit');
Route::put('/komoditas/update/{id}', [App\Http\Controllers\KomoditasController::class, 'update'])->name('komoditas.update');
Route::get('/komoditas/destroy/{id}', [App\Http\Controllers\KomoditasController::class, 'destroy'])->name('komoditas.destroy');

// ################# ADMIN MANAGE DATA USAHA ########################### //

Route::get('/usaha/jenis/', [App\Http\Controllers\UsahaController::class, 'jenis'])->name('usaha.jenis');
Route::get('/usaha/jenis/create/', [App\Http\Controllers\UsahaController::class, 'create_jenis'])->name('usaha.create_jenis');
Route::post('/usaha/jenis/store/', [App\Http\Controllers\UsahaController::class, 'store_jenis'])->name('usaha.store_jenis');
Route::get('/usaha/jenis/edit/{id}', [App\Http\Controllers\UsahaController::class, 'edit_jenis'])->name('usaha.edit_jenis');
Route::put('/usaha/jenis/update/{id}', [App\Http\Controllers\UsahaController::class, 'update_jenis'])->name('usaha.update_jenis');
Route::get('/usaha/jenis/destroy/{id}', [App\Http\Controllers\UsahaController::class, 'destroy_jenis'])->name('usaha.destroy_jenis');

// ################# ADMIN MANAGE DATA PENYULUH ########################### //

Route::get('/penyuluh/', [App\Http\Controllers\PenyuluhController::class, 'index'])->name('penyuluh');
Route::get('/penyuluh/create/', [App\Http\Controllers\PenyuluhController::class, 'create'])->name('penyuluh.create');
Route::post('/penyuluh/store/', [App\Http\Controllers\PenyuluhController::class, 'store'])->name('penyuluh.store');
Route::get('/penyuluh/edit/{id}', [App\Http\Controllers\PenyuluhController::class, 'edit'])->name('penyuluh.edit');
Route::put('/penyuluh/update/{id}', [App\Http\Controllers\PenyuluhController::class, 'update'])->name('penyuluh.update');
Route::get('/penyuluh/destroy/{id}', [App\Http\Controllers\PenyuluhController::class, 'destroy'])->name('penyuluh.destroy');

// ################# ADMIN MANAGE DATA ALSINTAN ########################### //

Route::get('alsintan', [App\Http\Controllers\AlsintanController::class, 'index'])->name('alsintan');
Route::get('/alsintan/create/', [App\Http\Controllers\AlsintanController::class, 'create'])->name('alsintan.create');
Route::post('/alsintan/store/', [App\Http\Controllers\AlsintanController::class, 'store'])->name('alsintan.store');
Route::get('/alsintan/edit/{id}', [App\Http\Controllers\AlsintanController::class, 'edit'])->name('alsintan.edit');
Route::put('/alsintan/update/{id}', [App\Http\Controllers\AlsintanController::class, 'update'])->name('alsintan.update');
Route::get('/alsintan/destroy/{id}', [App\Http\Controllers\AlsintanController::class, 'destroy'])->name('alsintan.destroy');

Route::get('/alsintan/jenis/', [App\Http\Controllers\AlsintanController::class, 'jenis'])->name('alsintan.jenis');
Route::get('/alsintan/jenis/create/', [App\Http\Controllers\AlsintanController::class, 'create_jenis'])->name('alsintan.create_jenis');
Route::post('/alsintan/jenis/store/', [App\Http\Controllers\AlsintanController::class, 'store_jenis'])->name('alsintan.store_jenis');
Route::get('/alsintan/jenis/edit/{id}', [App\Http\Controllers\AlsintanController::class, 'edit_jenis'])->name('alsintan.edit_jenis');
Route::put('/alsintan/jenis/update/{id}', [App\Http\Controllers\AlsintanController::class, 'update_jenis'])->name('alsintan.update_jenis');
Route::get('/alsintan/jenis/destroy/{id}', [App\Http\Controllers\AlsintanController::class, 'destroy_jenis'])->name('alsintan.destroy_jenis');

// ################# ADMIN MANAGE DATA SEMBAKO ########################### //

Route::get('sembako', [App\Http\Controllers\SembakoController::class, 'index'])->name('sembako');
Route::get('/sembako/create/', [App\Http\Controllers\SembakoController::class, 'create'])->name('sembako.create');
Route::post('/sembako/store/', [App\Http\Controllers\SembakoController::class, 'store'])->name('sembako.store');
Route::get('/sembako/edit/{id}', [App\Http\Controllers\SembakoController::class, 'edit'])->name('sembako.edit');
Route::put('/sembako/update/{id}', [App\Http\Controllers\SembakoController::class, 'update'])->name('sembako.update');
Route::get('/sembako/destroy/{id}', [App\Http\Controllers\SembakoController::class, 'destroy'])->name('sembako.destroy');

// ################# ADMIN MANAGE DATA HARGA ########################### //

Route::get('harga', [App\Http\Controllers\HargaController::class, 'index'])->name('harga');
Route::get('/harga/create/', [App\Http\Controllers\HargaController::class, 'create'])->name('harga.create');
Route::post('/harga/store/', [App\Http\Controllers\HargaController::class, 'store'])->name('harga.store');
Route::get('/harga/edit/{id}', [App\Http\Controllers\HargaController::class, 'edit'])->name('harga.edit');
Route::put('/harga/update/{id}', [App\Http\Controllers\HargaController::class, 'update'])->name('harga.update');
Route::get('/harga/destroy/{id}', [App\Http\Controllers\HargaController::class, 'destroy'])->name('harga.destroy');

// ################# ADMIN MANAGE DATA GIS ########################### //

Route::get('gis', [App\Http\Controllers\GISController::class, 'index'])->name('gis');
Route::get('/gis/create/', [App\Http\Controllers\GISController::class, 'create'])->name('gis.create');
Route::post('/gis/store/', [App\Http\Controllers\GISController::class, 'store'])->name('gis.store');
Route::get('/gis/show/', [App\Http\Controllers\GISController::class, 'show'])->name('gis.show');
Route::get('/gis/detail/{id}', [App\Http\Controllers\GISController::class, 'detail'])->name('gis.detail');
Route::get('/gis/edit/{id}', [App\Http\Controllers\GISController::class, 'edit'])->name('gis.edit');
Route::put('/gis/update/{id}', [App\Http\Controllers\GISController::class, 'update'])->name('gis.update');
Route::get('/gis/destroy/{id}', [App\Http\Controllers\GISController::class, 'destroy'])->name('gis.destroy');

// ################# ADMIN MANAGE DATA USER ########################### //

Route::get('users',  [App\Http\Controllers\UserController::class, 'index'])->name('users');
Route::get('/users/create/', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
Route::post('/users/store/', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
Route::get('/users/show/{id}', [App\Http\Controllers\UserController::class, 'show'])->name('users.show');
Route::get('/users/edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
Route::put('/users/update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');
Route::get('/users/destroy/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');

// ################### SIKEP ################################# //

Route::get('sikep', [App\Http\Controllers\SikepController::class, 'index'])->name('sikep');

Route::get('/main/gapoktan/', [App\Http\Controllers\MainController::class, 'gapoktan'])->name('main.gapoktan');

// ################### CREDENTIAL SIKEP ################################# //

Route::get('/sikep/register/', [App\Http\Controllers\SikepController::class, 'sikep_register'])->name('sikep.register');
Route::get('/sikep/getdesa/{id}', [App\Http\Controllers\SikepController::class, 'getDesa'])->name('sikep.getDesa');
// ################################# END SIKEP  ########################### //

// ################### CREDENTIAL  ################################# //

Route::post('/blog_login', [App\Http\Controllers\Auth\LoginController::class,'blog_login'])->name('blog_login');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::get('login', [App\Http\Controllers\Auth\LoginController::class,'showLoginForm'])->name('login');

// Registration Routes...
Route::get('register', [App\Http\Controllers\Auth\RegisterController::class,'showRegistrationForm'])->name('register');
Route::post('register', [App\Http\Controllers\Auth\RegisterController::class,'create'])->name('signup');
Route::get('/migrations', [App\Http\Controllers\Auth\ArtisanController::class,'migrations'])->name('migrations');
// ############################## END CREDENTIAL  ######################### //