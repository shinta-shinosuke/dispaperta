<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGapoktanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gapoktans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_kecamatan');
            $table->string('id_desa');
            $table->string('no_induk');
            $table->string('nama');
            $table->string('jenis_kelembagaan');
            $table->string('utama');
            $table->string('usaha');
            $table->string('kelas_kelompok');
            $table->string('jumlah_anggota');
            $table->string('jenis_jumlah');
            $table->string('luas_areal');
            $table->string('jenis_areal');
            $table->string('ketua');
            $table->string('sekretaris');
            $table->string('bendahara');
            $table->string('tahun_pembentukan');
            $table->string('jenis_pembentukan');
            $table->string('hp');
            $table->string('email');
            $table->string('extensi');
            $table->string('nama_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gapoktans');
    }
}
