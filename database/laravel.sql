-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2021 at 03:41 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tentang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extensi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `tentang`, `extensi`, `nama_file`, `created_at`, `updated_at`) VALUES
(1, 'ABK Pelaksanan Pengawas', 'rar', 'ABK-Pelaksana-Pengawas.rar', '2021-05-02 09:35:55', '2021-05-02 09:58:55'),
(2, 'LAPORAN KINERJA INSTANSI PEMERINTAH (LKjIP) DISPAPERTA TAHUN 2018', 'pdf', 'LKjIP-2018-fix-dikonversi.pdf', '2021-05-02 09:59:32', '2021-05-02 09:59:32'),
(3, 'cara input artikel web', 'docx', 'tutor-isi-artikel-web-dispaperta.docx', '2021-05-02 10:00:11', '2021-05-02 10:00:11'),
(4, 'LKJIP 2017', 'rar', 'lkjip2017.rar', '2021-05-02 10:00:28', '2021-05-02 10:00:28'),
(5, 'UU No. 16 Tahun 2006 Tentang Sistem Penyuluhan Pertanian, Perikanan Dan Kehutanan', 'pdf', 'UU-2006-16.pdf', '2021-05-02 10:00:45', '2021-05-02 10:00:45');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gis`
--

CREATE TABLE `gis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gis`
--

INSERT INTO `gis` (`id`, `nama`, `alamat`, `telp`, `lng`, `lat`, `created_at`, `updated_at`) VALUES
(1, 'Dinas Pangan dan Pertanian Kab. Batang', 'JL. Ahmad Yani No.943 Kec. Batang, Kab. Batang, Jawa tengah', '(0285)391902', '109.728295', '-6.915591', '2021-05-02 10:21:22', '2021-05-02 11:59:05'),
(2, 'Kebun Pisang', 'Perkebunan Pisang Tragung', '-', '109.75535325154897', '-6.943541082737787', '2021-05-02 10:21:57', '2021-05-02 11:59:40'),
(3, 'Kantor Cabang Clapar Subah', 'Kantor Cabang Clapar di Subah', '-', '109.85277', '-6.9672', '2021-05-02 10:22:40', '2021-05-02 12:00:03'),
(4, 'Kom. Bawang', 'Kom. Bawang', '-', '109.8542', '-7.08556', '2021-05-02 10:23:00', '2021-05-02 12:00:29'),
(6, 'tes', 'tes', 'tes', 'es', 'tes', '2021-05-02 11:58:10', '2021-05-02 11:58:10');

-- --------------------------------------------------------

--
-- Table structure for table `hargas`
--

CREATE TABLE `hargas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_sembako` int(11) NOT NULL,
  `harga_sebelum` int(11) NOT NULL,
  `harga_sesudah` int(11) NOT NULL,
  `blth` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hargas`
--

INSERT INTO `hargas` (`id`, `id_sembako`, `harga_sebelum`, `harga_sesudah`, `blth`, `created_at`, `updated_at`) VALUES
(1, 1, 10700, 10500, '082020', '2021-05-02 06:50:26', '2021-05-02 06:50:26'),
(3, 2, 9500, 9400, '082020', '2021-05-02 07:27:01', '2021-05-02 07:27:01'),
(4, 3, 3200, 3200, '082020', '2021-05-02 07:48:19', '2021-05-02 07:48:19'),
(5, 4, 2000, 2000, '082020', '2021-05-02 07:48:29', '2021-05-02 07:48:29'),
(6, 5, 6000, 5400, '082020', '2021-05-02 07:48:44', '2021-05-02 07:48:44'),
(7, 6, 32000, 35000, '082020', '2021-05-02 07:49:05', '2021-05-02 07:49:05'),
(8, 7, 68000, 68000, '082020', '2021-05-02 07:50:40', '2021-05-02 07:50:40'),
(9, 8, 55000, 55000, '082020', '2021-05-02 07:50:55', '2021-05-02 07:50:55'),
(10, 9, 80000, 160000, '082020', '2021-05-02 07:51:38', '2021-05-02 07:51:38'),
(11, 10, 90000, 90000, '082020', '2021-05-02 07:51:54', '2021-05-02 07:51:54'),
(12, 11, 27000, 30000, '082020', '2021-05-02 07:52:14', '2021-05-02 07:52:14'),
(13, 12, 7400, 7400, '082020', '2021-05-02 07:52:30', '2021-05-02 07:52:30'),
(14, 13, 25000, 25000, '082020', '2021-05-02 07:52:43', '2021-05-02 07:52:43'),
(15, 14, 22000, 22000, '082020', '2021-05-02 07:52:56', '2021-05-02 07:52:56'),
(16, 15, 40000, 40000, '082020', '2021-05-02 07:53:09', '2021-05-02 07:53:09'),
(17, 16, 2300, 2300, '082020', '2021-05-02 07:53:19', '2021-05-02 07:53:19'),
(18, 17, 8000, 8000, '082020', '2021-05-02 07:53:32', '2021-05-02 07:53:32'),
(19, 18, 13000, 13000, '082020', '2021-05-02 07:53:47', '2021-05-02 07:53:47'),
(20, 19, 5100, 4500, '082020', '2021-05-02 07:54:06', '2021-05-02 07:54:06');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatans`
--

CREATE TABLE `kecamatans` (
  `id_kecamatan` int(10) UNSIGNED NOT NULL,
  `kecamatan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kecamatans`
--

INSERT INTO `kecamatans` (`id_kecamatan`, `kecamatan`, `created_at`, `updated_at`) VALUES
(0, '', NULL, NULL),
(1, 'Warungasem', '2021-04-30 06:56:02', '2021-04-30 06:56:02'),
(2, 'Batang', '2021-04-30 06:56:08', '2021-04-30 06:56:08'),
(3, 'Kandeman', '2021-04-30 06:56:13', '2021-04-30 06:56:13'),
(4, 'Tulis', '2021-04-30 06:56:26', '2021-04-30 06:56:26'),
(5, 'Subah', '2021-04-30 06:56:31', '2021-04-30 06:56:31'),
(6, 'Banyuputih', '2021-04-30 06:56:42', '2021-04-30 06:56:42'),
(7, 'Gringsing', '2021-04-30 06:56:49', '2021-04-30 06:56:49'),
(8, 'Wonotunggal', '2021-04-30 06:57:01', '2021-04-30 06:57:01'),
(9, 'Bandar', '2021-04-30 06:57:06', '2021-04-30 06:57:06'),
(10, 'Pecalungan', '2021-04-30 06:57:16', '2021-04-30 06:57:16'),
(11, 'Limpung', '2021-04-30 06:57:22', '2021-04-30 06:57:22'),
(12, 'Tersono', '2021-04-30 06:57:31', '2021-04-30 06:57:31'),
(13, 'Bawang', '2021-04-30 06:57:35', '2021-04-30 06:57:35'),
(14, 'Blado', '2021-04-30 06:57:40', '2021-04-30 06:57:40'),
(15, 'Reban', '2021-04-30 06:57:49', '2021-04-30 06:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2019_08_19_000000_create_failed_jobs_table', 1),
(12, '2021_04_30_133042_create_users_table', 1),
(13, '2021_04_30_134048_create_kecamatans_table', 2),
(14, '2021_04_30_134334_create_pages_table', 3),
(15, '2021_05_02_080224_create_sembakos_table', 4),
(16, '2021_05_02_082724_create_hargas_table', 5),
(17, '2021_05_02_160538_create_downloads_table', 6),
(18, '2021_05_02_171212_create_gis_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `judul`, `content`, `img`, `created_at`, `updated_at`) VALUES
(1, 'Dinas Pangan dan Pertanian Kabupaten Batang', '<p><a href=\"https://dispaperta.batangkab.go.id/assets/foto_banner/dispaperta2018.jpg\" target=\"_blank\"><img alt=\"\" src=\"https://dispaperta.batangkab.go.id/assets/foto_banner/dispaperta2018.jpg\" style=\"height:281px; width:500px\" /></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Peraturan daerah no. 8 tahun 2016 tentang pembentukan dan susunan perangkat daerah.<br />\r\nPeraturan Bupati no. 53 tahun 2016 tentang kedudukan suatu organisasi, tugas dan fungsi serta tatakerja Dinas Pangan dan Pertanian Kabupaten Batang Dispaperta adalah suatu bentuk perangkat daerah di bidang Pangan dan Pertanian.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>Dinas Pangan dan Pertanian (DISPAPERTA) Kab. Batang</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Alamat &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Jl. Ahmad Yani No. 943 Batang</p>\r\n\r\n<p>Telepon&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (0285)391902</p>\r\n\r\n<p>Email &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;dispaperta@batangkab.go.id</p>\r\n\r\n<p>WebSite &nbsp; &nbsp; &nbsp; &nbsp; dispaperta.batangkab.go.id</p>\r\n\r\n<p>Kode Pos &nbsp; &nbsp; &nbsp;&nbsp; 51216</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3><strong>Pimpinan</strong></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nama &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ir. SUSILO HERU YUWONO,MM</p>\r\n\r\n<p>NIP &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; 1964050719900031007</p>\r\n\r\n<p>Jabatan&nbsp; &nbsp; &nbsp; &nbsp; Kepala Dinas Pangan dan Pertanian</p>', '', '2021-04-30 12:04:32', '2021-05-01 08:14:09'),
(2, 'Visi Misi', '<p><img alt=\"\" src=\"https://dispaperta.batangkab.go.id/assets/foto_banner/gedung.jpg\" style=\"height:375px; width:500px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Visi :</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Menjadi Lembaga Pengelola Sektor Ketahanan Pangan dan Pertanian yang Profesional dalam melayani masyarakat, Amanah, Aspiratif dan Inovatif demi Terwujudnya Peningkatan Kesejahteraan Petani</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Misi :&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Meningkatkan kualitas dan kuantitas hasil pangan dan pertanian menuju industrialisasi hasil pertanian berbasis sumberdaya lokal dan ramah lingkungan</li>\r\n	<li>Menciptakan sistem agribisnis yang berorientasi pasar dengan menumbuhkan usaha ekonomi produktif dan penciptaan lapangan kerja di pedesaan serta mengurangi kemiskinan</li>\r\n	<li>Mewujudkan kondisi pangan yang cukup, aman, beragam, bergizi, merata dan terjangkau bagi masyarakat</li>\r\n	<li>Meningkatkan pengetahuan, ketrampilan dan sikap petani melalui pendekatan kelompok</li>\r\n	<li>Meningkatkan kualitas kinerja dan pelayanan aparatur pemerintah bidang pertanian yang amanah, aspiratif dan inovatif</li>\r\n</ul>', '', '2021-05-02 00:28:58', '2021-05-02 00:29:41'),
(3, 'Sekretariat', '<p>A.TUGAS POKOK</p>\r\n\r\n<p>Tugas Pokok Sekretariat memiliki tugas menyelenggarakan administrasi umum, perlengkapan, kerumahtanggaan, kelembagaan, kehumasan,&nbsp;kepegawaian, keuangan, dan program di lingkungan Dispaperta.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>B. FUNGSI</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Pelaksanaan Penyusunan dan pengembangan kebijakan teknis dan program kerja pada Sekretariat;</p>\r\n	</li>\r\n	<li>\r\n	<p>Pengkoordinasian dan penyiapan bahan penyusunan perencanaan dan program kerja bidang secara terpadu;</p>\r\n	</li>\r\n	<li>\r\n	<p>Pelaksanaan upaya peningkatan pelayanan publik di bidang kesekretariatan;</p>\r\n	</li>\r\n	<li>\r\n	<p>Pengelolaan dan pengendalian administrasi umum, administrasi kepegawaian dan administrasi keuangan;</p>\r\n	</li>\r\n	<li>\r\n	<p>Pelaksanaan urusan kerumahtanggaan dan perlengkapan;</p>\r\n	</li>\r\n	<li>\r\n	<p>Pelaksanaan urusan organisasi, tatalaksana dan kehumasan;</p>\r\n	</li>\r\n	<li>\r\n	<p>Pelayanan teknis administratif kepada Kepala Dinas dan semua satuan unit kerja di lingkungan Dispaperta;</p>\r\n	</li>\r\n	<li>\r\n	<p>Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas kesekretariatan dan dinas; dan</p>\r\n	</li>\r\n	<li>\r\n	<p>Pelaksanaan tugs kedinasan lain yang diberikan oleh atasan</p>\r\n	</li>\r\n</ol>', '', '2021-05-02 00:52:33', '2021-05-02 00:52:33'),
(4, 'Ketahanan Pangan', '<p>A.TUGAS POKOK</p>\r\n\r\n<p>Bidang Ketahanan Pangan memiliki tugas melaksanakan sebagian tugas Kepala Dinas dalam koordinasi, bimbingan, suvervisi dan konsultasi, perencanaan, penelitian, pemantauan, pengembangan dan pengawasan dalam urusan ketahanan pangan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>B. FUNGSI</p>\r\n\r\n<ol>\r\n	<li>Pelaksanaan Penyusunan dan pengembangan kebijakan teknis perencanaan dan program kerja pada bidang Ketahanan Pangan ;</li>\r\n	<li>Penyelenggaraan upaya peningkatan pelayanan publik di bidang Ketahanan Pangan;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan, dan pengendalian pelaksanaan kegiatan ketersediaan, distribusi, dan kawanan pangan;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan keanekaragaman konsumsi pangan;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan keamanan pangan;</li>\r\n	<li>Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas pada bidang Ketahanan Pangan;</li>\r\n	<li>Pelaksanaan tugas kedinasan lain yang diberikan oleh atasan.</li>\r\n</ol>', '', '2021-05-02 00:53:43', '2021-05-02 00:53:43'),
(5, 'Hortikultura', '<p>A.&nbsp;&nbsp; &nbsp; TUGAS POKOK</p>\r\n\r\n<p>Bidang Hortikultura mempunyai tugas melaksanakan sebagian Kepala Dinas dalam koordinasi, bimbingan, supervisi dan konsultasi, perencanaan , penelitian , pemantauan, pengembangan dan pengawasan di bidang hortikultura.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>B.&nbsp;&nbsp; &nbsp;BIDANG HORTIKULTURA MEMPUNYAI FUNGSI;</p>\r\n\r\n<ol>\r\n	<li>Pelaksanaan penyusunan dan pengembangan kebijakan teknis perencanaan dan program kerja pada Bidang Hortikultura;</li>\r\n	<li>&nbsp;Penyelenggaraan upaya peningkatan pelayanan publik di bidan;</li>\r\n	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan sarana prasarana hortikultura;</li>\r\n	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan, dan pengendalian pelaksanaan kegiatan produksi dan perlindungan hortikultura;</li>\r\n	<li>&nbsp;Pelaksanaan perencanaan, pembinaan, pengembangan,, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan pasca panen dan agribisnis hortikultura;</li>\r\n	<li>&nbsp;Pelaksanaan monitoring, evaluasi dan laporan pelaksanaan tugas pada Bidang Hortikultura;</li>\r\n	<li>&nbsp;Pelaksanaan tugas kedinasan lain yang di berikan oleh atsan.</li>\r\n</ol>', '', '2021-05-02 00:55:23', '2021-05-02 00:55:23'),
(6, 'Perkebunan', '<p>A.&nbsp;&nbsp;&nbsp; TUGAS POKOK</p>\r\n\r\n<p>Bidang Perkebunan mempunyai tugas melaksanakan sebagian tugas Kepala Dinas dalam menyusun kebijakan di bidang perkebunan, melaksanakan rehabilitasi, diversifikasi lahan dan infrastruktur perkebunan serta menyelenggarakan perlindungan tanaman perkebunan dan penatausahaan perkebunan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>B.&nbsp;&nbsp;&nbsp; FUNGSI</p>\r\n\r\n<ol>\r\n	<li>Penyusunan kebijakan teknis, perencanaan dan program kerja pada Bidang Perkebunan;</li>\r\n	<li>&nbsp;Penyelenggaraan upaya peningkatan pelayanan publik di Bidang Perkebunan;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan Produksi Perkebunan;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan perlindungan tanaman dan sarana prasarana perkebunan ;</li>\r\n	<li>Pelaksanaan perencanaan, pembinnaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan pasca panen dan agribisnis perkebunan;</li>\r\n	<li>&nbsp;Pelaksanaan monitoring, evaluasi dan pelaporan pelaksanaan tugas pada bidang Perkebunan; dan</li>\r\n	<li>&nbsp;Pelaksanaan tugas kedinasan lain yang diberikan oleh atasan.</li>\r\n</ol>', '', '2021-05-02 00:56:08', '2021-05-02 00:56:08'),
(7, 'PSKP', '<p>A. &nbsp;&nbsp; &nbsp;TUGAS POKOK</p>\r\n\r\n<p>Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian mempunyai tugas melaksanakan sebagian tugas Kepala Dinas dalam menyusun, menyiapkan, melaksanakan, mengkoordinasikan, memfasilitasi, mengatur, memantau dan mengevaluasi serta melaporkan kegiatan dibidang pengembangan sumberdaya manusia dan kelembagaan pertanian.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>B. &nbsp;&nbsp; &nbsp;FUNGSI</p>\r\n\r\n<ol>\r\n	<li>Penyusunan Kebijakan teknis, perencanaan dan program kerja pada Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian ;</li>\r\n	<li>Penyelenggaraan upaya peningkatan pelayanan publik di bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan kelembagaan pertanian;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan sumber daya manusia penyuluhan;</li>\r\n	<li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksannaan kegiatan penyelenggaraan penyuluhan;</li>\r\n	<li>Pelaksanaan&nbsp; monitoring, evaluasi dan laporan pelaksanaan tugas pada Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian; dan</li>\r\n	<li>Pelaksanaan tugas kedinasan lain yang diberikan oleh atasan.</li>\r\n</ol>', '', '2021-05-02 00:57:02', '2021-05-02 00:57:02'),
(8, 'Tugas Pokok dan Fungsi', '<p>A.TUGAS POKOK DISPAPERTA</p>\r\n\r\n<p>Dispaperta mempunyai tugas membantu Bupati melaksanakan urusan pemerintahan bidang pangan dan pertanian dan tugas pembantuan yang diberikan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>B. FUNGSI</p>\r\n\r\n<ul>\r\n	<li>Merumuskan kebijakan teknis bid pangan dan pertanian</li>\r\n	<li>Melaksanakan kebijakan teknis bid pangan dan pertanian</li>\r\n	<li>Menyelenggarakan urusan pemerintahan dan pelayanan umum di bid pangan dan pertanian</li>\r\n	<li>Pembinaan dan pelaksanaan tugas bid pangan dan pertanian</li>\r\n	<li>Pengoordinasian dan pemetaan kawasan rawan pangan</li>\r\n	<li>Pengelolaan rekomendasi teknis perizinan di bid Pangan dan pertanian</li>\r\n	<li>Penyebaran informasi di bidang Pangan dan Pertanian</li>\r\n	<li>Pembinaan terhadap UPTD dalam lingkup Dispaperta</li>\r\n	<li>Penyelenggaraan kesekretariatan Dispaperta.</li>\r\n</ul>', '', '2021-05-02 00:58:29', '2021-05-02 00:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sembakos`
--

CREATE TABLE `sembakos` (
  `id` int(10) UNSIGNED NOT NULL,
  `sembako` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sembakos`
--

INSERT INTO `sembakos` (`id`, `sembako`, `created_at`, `updated_at`) VALUES
(1, 'Beras Premium', '2021-05-02 01:13:31', '2021-05-02 01:13:31'),
(2, 'Beras Medium', '2021-05-02 01:13:39', '2021-05-02 01:13:39'),
(3, 'Ubi Jalar', '2021-05-02 01:13:48', '2021-05-02 01:13:48'),
(4, 'Ubi Kayu', '2021-05-02 01:13:56', '2021-05-02 01:13:56'),
(5, 'Gabah Kering Giling', '2021-05-02 01:14:07', '2021-05-02 01:14:07'),
(6, 'Bawang Putih', '2021-05-02 01:14:15', '2021-05-02 01:14:15'),
(7, 'Bunga Cengkeh Kering', '2021-05-02 01:14:24', '2021-05-02 01:14:24'),
(8, 'Cabe Merah Keriting', '2021-05-02 01:14:35', '2021-05-02 01:14:35'),
(9, 'Cabe Merah Besar', '2021-05-02 01:14:53', '2021-05-02 01:14:53'),
(10, 'Cabe Rawit Merah', '2021-05-02 01:15:02', '2021-05-02 01:15:02'),
(11, 'Bawang Merah', '2021-05-02 01:15:12', '2021-05-02 01:15:12'),
(12, 'Karet (Lump)', '2021-05-02 01:15:30', '2021-05-02 01:15:30'),
(13, 'Kakao (Non Fermentasi)', '2021-05-02 01:15:44', '2021-05-02 01:15:44'),
(14, 'Kopi Robusta', '2021-05-02 01:15:58', '2021-05-02 01:15:58'),
(15, 'Kopi Arabika', '2021-05-02 01:16:19', '2021-05-02 01:16:19'),
(16, 'Teh Pucuk Basah', '2021-05-02 01:16:33', '2021-05-02 01:16:33'),
(17, 'Kelapa', '2021-05-02 01:16:49', '2021-05-02 01:16:49'),
(18, 'Gula Kristal Putih', '2021-05-02 01:17:02', '2021-05-02 01:17:02'),
(19, 'Gabah Kering Panen', '2021-05-02 01:17:09', '2021-05-02 01:25:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_kecamatan` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama_lengkap`, `name`, `email`, `email_verified_at`, `password`, `phone`, `level`, `id_kecamatan`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', 'admin', 'admin@test.com', NULL, 'admin', '', NULL, NULL, NULL, NULL, NULL),
(2, 'shinta', 'shino', 'shino@test.com', NULL, '$2y$10$QLEQQxx5kH0qddpwaUSgue63zHX0TZ4tIBPXRAlHaKGZFpri1PtXq', '0', 'Admin Bidang', 0, NULL, '2021-05-01 02:20:04', '2021-05-01 02:20:04'),
(3, 'Teguh Satrio', 'goeh', 'goeh@test.com', NULL, '$2y$10$mbIMOQDSrqYuD8qeiPP/XOY/kyHJSUJW9Yd9aQqpE3Iz3mw4m/pk.', '0', 'Admin Bidang', 0, NULL, '2021-05-01 02:23:45', '2021-05-02 10:03:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gis`
--
ALTER TABLE `gis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hargas`
--
ALTER TABLE `hargas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kecamatans`
--
ALTER TABLE `kecamatans`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sembakos`
--
ALTER TABLE `sembakos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gis`
--
ALTER TABLE `gis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hargas`
--
ALTER TABLE `hargas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `kecamatans`
--
ALTER TABLE `kecamatans`
  MODIFY `id_kecamatan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sembakos`
--
ALTER TABLE `sembakos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
