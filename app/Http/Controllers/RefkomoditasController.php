<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Refkomoditas;
use Illuminate\Http\Request;
use DataTables;

class RefkomoditasController extends Controller
{
    protected $tb_komoditas;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_komoditas = new Refkomoditas();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
                $data = $this->tb_komoditas->select_all_ajax();
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="refkomoditas/edit/'.$row->id_komoditas.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="refkomoditas/destroy/'.$row->id_komoditas.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('refkomoditas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('refkomoditas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
            'varietas'      => 'required',
            'jenis'      => 'required',
        ]);
        
        $this->tb_komoditas->store($request);

        return redirect()->route('refkomoditas')->withStatus(__('Data komoditas berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $komoditas = $this->tb_komoditas->select_one($id);
        return view('refkomoditas.edit',compact('komoditas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
            'varietas'      => 'required',
            'jenis'      => 'required',
        ]);

        $update = $this->tb_komoditas->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('refkomoditas')
            ->with('success', 'Data komoditas Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_komoditas->remove($id);
        if($delete){
            return redirect()->route('refkomoditas')
            ->with('success', 'Data komoditas Berhasil Dihapus');
        }
        
    }

}
