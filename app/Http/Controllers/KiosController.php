<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Kios;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use DataTables;

class KiosController extends Controller
{
    protected $tb_kios;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_kios = new Kios();
        $this->tb_kecamatan = new Kecamatan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_kios->select_all_join_kecamatan_null();
            }else{
                $data = $this->tb_kios->select_all_join_kecamatan($search);
            }

                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="kios/edit/'.$row->id_kios.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="kios/destroy/'.$row->id_kios.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('kios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('kios.create', compact('kecamatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
            'kode'      => 'required',
            'kecamatan'      => 'required',
        ]);
        
        $this->tb_kios->store($request);

        return redirect()->route('kios')->withStatus(__('Data kios berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $kecamatan = $this->tb_kecamatan->select_all();
        $kios = $this->tb_kios->select_one($id);
        return view('kios.edit',compact('kios','kecamatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name'      => 'required',
            'kode'      => 'required',
            'kecamatan'      => 'required',
        ]);

        $update = $this->tb_kios->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('kios')
            ->with('success', 'Data kios Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_kios->remove($id);
        if($delete){
            return redirect()->route('kios')
            ->with('success', 'Data kios Berhasil Dihapus');
        }
        
    }

}
