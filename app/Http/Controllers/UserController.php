<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use DataTables;

class UserController extends Controller
{
    protected $tb_user;
    protected $tb_kecamatan;
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_user = new User();
        $this->tb_kecamatan = new Kecamatan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_user->select_all_join_kecamatan_null();
            }else{
                $data = $this->tb_user->select_all_join_kecamatan($search);
            }
            return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="users/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="users/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('users.create')->with('kecamatan', $kecamatan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'fullname'  => 'required',
            'name'      => 'required',
            'email'     => 'required',
            'password'  => 'required',
            'phone'     => 'required',
            'level'     => 'required',
        ]);
        
        $newUser = $this->tb_user->store($request);

        if($newUser)
        {
            //jika data berhasil ditambahkan, akan kembali ke halaman utama
            return redirect()->route('users')->withStatus(__('User berhasil ditambahkan.'));
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user
        $user = $this->tb_user->select_one($id);
        return view('users.detail', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $user = $this->tb_user->select_one($id);
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('users.edit', compact('user','kecamatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'fullname'  => 'required',
            'name'      => 'required',
            'email'     => 'required',
            'phone'     => 'required',
            'level'     => 'required',
        ]);

        $update = $this->tb_user->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('users')
            ->with('success', 'User Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_user->remove($id);
        if($delete){
            return redirect()->route('users')
            ->with('success', 'User Berhasil Dihapus');
        }
        
    }
}
