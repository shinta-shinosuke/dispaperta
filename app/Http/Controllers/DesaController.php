<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Desa;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Collection;
use DataTables;

class DesaController extends Controller
{
    protected $tb_desa;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_desa = new Desa();
        $this->tb_kecamatan = new Kecamatan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            //$request = $request->toArray();
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_desa->select_all_join_kecamatan_null();
            }else{
                $data = $this->tb_desa->select_all_join_kecamatan($search);
            }
                return Datatables::of($data)
                    
                        ->addIndexColumn()

                        ->addColumn('action', function($row){
                               $btn = '<a href="desa/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="desa/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            
        }

        return view('desa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('desa.create', compact('kecamatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
            'kecamatan'      => 'required',
        ]);
        
        /*echo"<pre>";
        print_r($request);
        echo"</pre>";*/
        $this->tb_desa->store($request);

        return redirect()->route('desa')->withStatus(__('Data Desa berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $kecamatan = $this->tb_kecamatan->select_all();
        $desa = $this->tb_desa->select_one($id);
        return view('desa.edit', compact('desa','kecamatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
            'kecamatan' => 'required'
        ]);

        $update = $this->tb_desa->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('desa')
            ->with('success', 'Data Desa Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_desa->remove($id);
        if($delete){
            return redirect()->route('desa')
            ->with('success', 'Data Desa Berhasil Dihapus');
        }
        
    }
}
