<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }
        
    public function login(Request $request){
        $uname = $request->username;
        $password = $request->password;

        // Retrive Input
        $data = $request->only('name', 'password');

        if (Auth::attempt($data)) {
    		
    		if (auth()->user()->level == 1) {
    		    $request->session()->put('level',1);
                return redirect()->route('admin.home');
            }else if (auth()->user()->level == 2) {
                $request->session()->put('level',2);
                return redirect()->route('home');
    		}else if (auth()->user()->level == 3) {
                $request->session()->put('level',3);
                return redirect()->route('home');
    		}else{
    		    $this->guard()->logout();
 
                $request->session()->flush();
         
                $request->session()->regenerate();
                
    		    return redirect('/main/login')->withError(__('Maaf, Anda tidak dikenal'));    
    		}
        }else{
            return redirect('/main/login')->withError(__('Data tidak sesuai'));
        }
    }
    
    /**
     * Log the user out of the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
 
        $request->session()->flush();
 
        $request->session()->regenerate();
 
        return redirect('/main/login')
            ->withSuccess('Terimakasih, selamat datang kembali!');
    }
}
