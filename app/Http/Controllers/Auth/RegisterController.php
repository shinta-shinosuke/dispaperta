<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Kecamatan;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    protected $tb_kecamatan;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->tb_kecamatan = new Kecamatan();
    }

    public function showRegistrationForm()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('auth.register', compact('kecamatan'));
    }
    
    public function showRegisterForm()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('main.register', compact('kecamatan'));
    }
    
    public function showRegBlogForm()
    {
        //$kecamatan = $this->tb_kecamatan->select_all();
        return view('main.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_lengkap' => ['required', 'string'],
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
            'phone' => ['required', 'integer', 'max:13'],
            'level' => ['required'],
            'alamat' => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(Request $data)
    {
        if(empty($data['kecamatan'])){
            $kecamatan = '0';
        }else{
            $kecamatan = $data['kecamatan'];
        }

        $newuser = User::create([
                    'remember_token' => $data['_token'],
                    'nama_lengkap'   => $data['fullname'],
                    'name'           => $data['name'],
                    'email'          => $data['email'],
                    'password'       => Hash::make($data['password']),
                    'phone'          => $data['phone'],
                    'level'          => $data['level'],
                    'kode_kecamatan'   => $kecamatan,
                    'kode_desa'   => $data['desa'],
                    'alamat' => $data['alamat']
        ]);

        if($newuser){
                return redirect('/main/login')->withStatus(__('Admin berhasil ditambahkan.'));
        }
    }
}
