<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\poktan;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Collection;
use DataTables;

class PoktanController extends Controller
{
    protected $tb_poktan;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_poktan = new poktan();
        $this->tb_kecamatan = new Kecamatan();
        $this->tb_poktan = new Poktan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            //$request = $request->toArray();
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_poktan->select_all_null();
            }else{
                $data = $this->tb_poktan->select_all_join($search);
            }
                return Datatables::of($data)
                    
                        ->addIndexColumn()

                        ->addColumn('action', function($row){
                               $btn = '<a href="poktan/edit/'.$row->poktan_id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="poktan/destroy/'.$row->poktan_id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            
        }

        return view('poktan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gapoktan = $this->tb_poktan->select_all_gapoktan();
        return view('poktan.create', compact('gapoktan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
        ]);
        
        /*echo"<pre>";
        print_r($request);
        echo"</pre>";*/
        $this->tb_poktan->store($request);

        return redirect()->route('poktan')->withStatus(__('Data poktan berhasil disimpan.'));
    }

    public function check(Request $request)
    {
        $request = $request->all();
        $poktan = $this->tb_poktan->check($request);

        //return $poktan;
        return response()->json($poktan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $kecamatan = $this->tb_kecamatan->select_all();
        $poktan = $this->tb_poktan->select_one($id);
        $gapoktan = $this->tb_poktan->select_all_gapoktan();
        return view('poktan.edit', compact('poktan','kecamatan','gapoktan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
        ]);

        $update = $this->tb_poktan->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('poktan')
            ->with('success', 'Data poktan Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_poktan->remove($id);
        if($delete){
            return redirect()->route('poktan')
            ->with('success', 'Data poktan Berhasil Dihapus');
        }
        
    }
}
