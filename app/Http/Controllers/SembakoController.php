<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Sembako;
use Illuminate\Http\Request;
use DataTables;

class SembakoController extends Controller
{
    protected $tb_sembako;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_sembako = new Sembako();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = $this->tb_sembako->select_all_ajax();
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="sembako/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="sembako/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('sembako.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sembako.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
        ]);
        
        $this->tb_sembako->store($request);

        return redirect()->route('sembako')->withStatus(__('Data Sembako berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $sembako = $this->tb_sembako->select_one($id);
        return view('sembako.edit',compact('sembako'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
        ]);

        $update = $this->tb_sembako->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('sembako')
            ->with('success', 'Data Sembako Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_sembako->remove($id);
        if($delete){
            return redirect()->route('sembako')
            ->with('success', 'Data Sembako Berhasil Dihapus');
        }
        
    }
}
