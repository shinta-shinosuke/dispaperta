<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Harga;
use App\Models\Sembako;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon as CarbonDate;

class HargaController extends Controller
{
    protected $tb_harga;
    protected $tb_sembako;
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_harga = new Harga();
        $this->tb_sembako = new Sembako();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            //$request = $request->toArray();
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_harga->select_all_join_sembako_null();
            }else{
                $data = $this->tb_harga->select_all_join_sembako($search);
            }
            
            
            return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="harga/edit/'.$row->id_harga.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="harga/destroy/'.$row->id_harga.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->addColumn('updated_at', function($row){

                               return CarbonDate::parse($row->updated_at)->format('d-m-Y');
                        })
                        ->addColumn('harga_sebelum', function($row){

                               return number_format($row->harga_sebelum, 2);
                        })
                        ->addColumn('harga_sesudah', function($row){

                            /*if($row->harga_sesudah > $row->harga_sebelum){
                                return number_format($row->harga_sesudah, 2).'&nbsp;<span class="fa fa-arrow-circle-o-up"></span>';
                            }else if($row->harga_sesudah < $row->harga_sebelum){

                               return number_format($row->harga_sesudah, 2).'&nbsp;<span class="fa fa-arrow-circle-o-down"></span>';
                            }else{
                                return number_format($row->harga_sesudah, 2);
                            }*/
                            return number_format($row->harga_sesudah, 2);
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('harga.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sembako = $this->tb_sembako->select_all();
        $blth = date('m').date('Y');
        //$blth='082020';
        return view('harga.create',compact('sembako', 'blth'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'blth'              => 'required|digits:6',
            'sembako'           => 'required',
            'harga_sebelum'     => 'required',
            'harga_sesudah'     => 'required',
        ]);
        
        $newHarga = $this->tb_harga->store($request);

        if($newHarga)
        {
            //jika data berhasil ditambahkan, akan kembali ke halaman utama
            return redirect()->route('harga')->withStatus(__('Harga berhasil ditambahkan.'));
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id Harga
        $harga = $this->tb_harga->select_one($id);
        return view('harga.detail', compact('harga'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //menampilkan detail data dengan menemukan/berdasarkan id Harga untuk diedit
        $harga = $this->tb_harga->select_one($id);
        $sembako = $this->tb_sembako->select_all();
        return view('harga.edit', compact('harga','sembako'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //validation parameters
        $request->validate([
            'blth'              => 'required|digits:6',
            'sembako'           => 'required',
            'harga_sebelum'     => 'required',
            'harga_sesudah'     => 'required',
        ]);

        $update = $this->tb_harga->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('harga')
            ->with('success', 'Harga Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_harga->remove($id);
        if($delete){
            return redirect()->route('harga')
            ->with('success', 'Harga Berhasil Dihapus');
        }
        
    }
}
