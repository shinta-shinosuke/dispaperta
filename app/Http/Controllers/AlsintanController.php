<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Alsintan;
use Illuminate\Http\Request;
use DataTables;

class AlsintanController extends Controller
{
    protected $tb_alsintan;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_alsintan = new Alsintan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_alsintan->select_all_join_jenis_null();
            }else{
                $data = $this->tb_alsintan->select_all_join_jenis($search);
            }
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="alsintan/edit/'.$row->id_alsintan.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="alsintan/destroy/'.$row->id_alsintan.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('alsintan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_alsintan = $this->tb_alsintan->select_all_jenis();
        return view('alsintan.create',compact('jenis_alsintan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
            'jenis_alsintan' => 'required'
        ]);
        
        $this->tb_alsintan->store($request);

        return redirect()->route('alsintan')->withStatus(__('Data Alsintan berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $jenis_alsintan = $this->tb_alsintan->select_all_jenis();
        $alsintan = $this->tb_alsintan->select_one($id);
        return view('alsintan.edit',compact('alsintan','jenis_alsintan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
            'jenis_alsintan' => 'required'
        ]);

        $update = $this->tb_alsintan->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('alsintan')
            ->with('success', 'Data Alsintan Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_alsintan->remove($id);
        if($delete){
            return redirect()->route('alsintan')
            ->with('success', 'Data Alsintan Berhasil Dihapus');
        }
        
    }

    /* ########################## JENIS ALSINTAN ####################################### */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jenis(Request $request)
    {

        if ($request->ajax()) {
                $data = $this->tb_alsintan->select_all_jenis();
            
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="jenis/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="jenis/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('alsintan.jenis');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_jenis()
    {
        return view('alsintan.create_jenis');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_jenis(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
        ]);
        
        $this->tb_alsintan->store_jenis($request);

        return redirect()->route('alsintan.jenis')->withStatus(__('Data Jenis Alsintan berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_jenis($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $alsintan = $this->tb_alsintan->select_one_jenis($id);
        return view('alsintan.edit_jenis',compact('alsintan'));
        //echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_jenis(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
        ]);

        $update = $this->tb_alsintan->modify_jenis($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('alsintan.jenis')
            ->with('success', 'Data Jenis Alsintan Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_jenis($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_alsintan->remove_jenis($id);
        if($delete){
            return redirect()->route('alsintan.jenis')
            ->with('success', 'Data Jenis Alsintan Berhasil Dihapus');
        }
        
    }

}
