<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Gis;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Requests\FormMapRequest;

class GISController extends Controller
{
    protected $tb_gis;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_gis = new Gis();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            
            $data = $this->tb_gis->select_all_ajax();
            return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="gis/detail/'.$row->id.'" class="edit btn btn-primary btn-sm">Detail</a>&nbsp;&nbsp;&nbsp;<a href="gis/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="gis/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('gis.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'nama'     => 'required',
            'alamat'   => 'required',
            'telp'     => 'required',
            'lng'      => 'required',
            'lat'      => 'required',
        ]);
        
        $data = $this->tb_gis->store($request);

        if($data)
        {
            //jika data berhasil ditambahkan, akan kembali ke halaman utama
            return redirect()->route('gis')->withStatus(__('Data berhasil ditambahkan.'));
        }

    }

    public function detail($id)
    {
        $boxmap = $this->tb_gis->select_map($id);
        /*foreach ($boxmap as $key => $value) {
            print_r($value['nama']);
        }*/

        $dataMap  = Array();
        $dataMap['type']='FeatureCollection';
        $dataMap['features']=array();
        foreach($boxmap as $value){
                $feaures = array();
                $feaures['type']='Feature';
                $geometry = array("type"=>"Point","coordinates"=>[$value->lng, $value->lat]);
                $feaures['geometry']=$geometry;
                $properties=array('title'=>$value->nama,"description"=>$value->alamat."<br>Telp.".$value->telp);
                $feaures['properties']= $properties;
                array_push($dataMap['features'],$feaures);

       }

        //menampilkan detail data dengan menemukan/berdasarkan id user
        //$gis = $this->tb_gis->select_one($id);
        //return view('gis.detail', compact('gis'));
       $data = array(
                    'dataArray' => json_encode($dataMap),
                    'gis'       => $boxmap
                );
       return View('gis.detail')->with($data);
    }

    public function show()
    {
        $boxmap = $this->tb_gis->select_all_map();
        /*foreach ($boxmap as $key => $value) {
            print_r($value['nama']);
        }*/

        $dataMap  = Array();
        $dataMap['type']='FeatureCollection';
        $dataMap['features']=array();
        foreach($boxmap as $value){
                $feaures = array();
                $feaures['type']='Feature';
                $geometry = array("type"=>"Point","coordinates"=>[$value->lng, $value->lat]);
                $feaures['geometry']=$geometry;
                $properties=array('title'=>$value->nama,"description"=>$value->alamat."<br>Telp.".$value->telp);
                $feaures['properties']= $properties;
                array_push($dataMap['features'],$feaures);

       }

        //menampilkan detail data dengan menemukan/berdasarkan id user
        //$gis = $this->tb_gis->select_one($id);
        //return view('gis.detail', compact('gis'));
       $data = array(
                    'dataArray' => json_encode($dataMap),
                    'gis'       => $boxmap
                );
       return View('gis.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $gis = $this->tb_gis->select_one($id);
        return view('gis.edit', compact('gis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'nama'     => 'required',
            'alamat'   => 'required',
            'telp'     => 'required',
            'lng'      => 'required',
            'lat'      => 'required',
        ]);

        $update = $this->tb_gis->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('gis')
            ->with('success', 'Data Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_gis->remove($id);
        if($delete){
            return redirect()->route('gis')
            ->with('success', 'Data Berhasil Dihapus');
        }
        
    }
}
