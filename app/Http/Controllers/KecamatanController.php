<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use DataTables;

class KecamatanController extends Controller
{
    protected $tb_kecamatan;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_kecamatan = new Kecamatan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = $this->tb_kecamatan->select_all_ajax();
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="kecamatan/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="kecamatan/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('kecamatan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('kecamatan.create')->with('kecamatan', $kecamatan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
        ]);
        
        $this->tb_kecamatan->store($request);

        return redirect()->route('kecamatan')->withStatus(__('Data kecamatan berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $kecamatan = $this->tb_kecamatan->select_one($id);
        return view('kecamatan.edit')->with('kecamatan', $kecamatan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
        ]);

        $update = $this->tb_kecamatan->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('kecamatan')
            ->with('success', 'Data Kecamatan Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_kecamatan->remove($id);
        if($delete){
            return redirect()->route('kecamatan')
            ->with('success', 'Data Kecamatan Berhasil Dihapus');
        }
        
    }
}
