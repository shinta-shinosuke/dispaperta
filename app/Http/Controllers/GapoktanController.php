<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Gis;
use App\Models\Download;
use App\Models\Harga;
use App\Models\Kecamatan;
use App\Models\Gapoktan;
use App\Models\Desa;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon as CarbonDate;

class GapoktanController extends Controller
{
    protected $tb_gapoktan;

    public function __construct()
    {
        $this->tb_page = new Page();
        $this->tb_gis = new Gis();
        $this->tb_download = new Download();
        $this->tb_harga = new Harga();
        $this->tb_kecamatan = new Kecamatan();
        $this->tb_gapoktan = new Gapoktan();
        $this->tb_desa = new Desa();
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = $this->tb_gapoktan->select_all_ajax();
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="gapoktan/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="gapoktan/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('gapoktan.index');
    }
    
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        $gapoktan = array();
        return view('gapoktan.create', compact('kecamatan', 'gapoktan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $validated = $request->validate([
                    'kecamatan'      => 'required',
                    'desa'           => 'required',
                    'no_induk'          => 'required',
                    'nama'              => 'required',
                    'pelaku'            => 'required',
                    'kelas_kelompok'    => 'required',
                    'jumlah_anggota'    => 'required',
                    'anggota'           => 'required',
                    'luas_areal'        => 'required',
                    'luas'              => 'required',
                    'ketua'             => 'required',
                    'sekretaris'        => 'required',
                    'bendahara'         => 'required',
                    'tahun_pembentukan' => 'required',
                    'jenis_pembentukan' => 'required',
                    'hp'                => 'required',
                    'email'             => 'required',
                    'file'              => 'required'
        ]);
        
        if ($validated) {
            
            // menyimpan data file yang diupload ke variabel $file
            $file  = $request->file('file');
    
            $upload = $this->upload($file);
    
            if($upload){
                $data = $this->tb_gapoktan->store($upload['name'], $upload['ext'], $request);
    
                if($data)
                {
                    //jika data berhasil ditambahkan, akan kembali ke halaman utama
                    return redirect()->route('gapoktan')->withStatus(__('Data berhasil ditambahkan.'));
                }
            }
        }else{
            $kecamatan = $this->tb_kecamatan->select_all();
            $gapoktan = $request->all();
            return view('gapoktan.create', compact('kecamatan', 'gapoktan'));
        }

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $kecamatan = $this->tb_kecamatan->select_all();
            $desa = $this->tb_desa->select_all();
            $gapoktan = $this->tb_gapoktan->select_one($id);
            return view('gapoktan.edit', compact('desa', 'kecamatan', 'gapoktan'));
    }
    
    public function update(Request $request)
    {
         //validation parameters
        $validated = $request->validate([
                    'kecamatan'      => 'required',
                    'desa'           => 'required',
                    'no_induk'          => 'required',
                    'nama'              => 'required',
                    'pelaku'            => 'required',
                    'kelas_kelompok'    => 'required',
                    'jumlah_anggota'    => 'required',
                    'anggota'           => 'required',
                    'luas_areal'        => 'required',
                    'luas'              => 'required',
                    'ketua'             => 'required',
                    'sekretaris'        => 'required',
                    'bendahara'         => 'required',
                    'tahun_pembentukan' => 'required',
                    'jenis_pembentukan' => 'required',
                    'hp'                => 'required',
                    'email'             => 'required'
        ]);
        
        if ($validated) {
            if(!empty($request->file('file'))){
                // menyimpan data file yang diupload ke variabel $file
                $file  = $request->file('file');
        
                $upload = $this->upload($file);
                if($upload){
                    $data = $this->tb_gapoktan->modify($upload['name'], $upload['ext'], $request, $request->id);
                }
            }else{
                $data = $this->tb_gapoktan->modify($request->filename, $request->extensi, $request, $request->id);
            }
            
            if($data)
            {
                //jika data berhasil ditambahkan, akan kembali ke halaman utama
                return redirect()->route('gapoktan')->withStatus(__('Data berhasil ditambahkan.'));
            }
        }else{
            $kecamatan = $this->tb_kecamatan->select_all();
            $gapoktan = $request->all();
            return view('gapoktan.edit', compact('kecamatan', 'gapoktan'));
        }

    }
    
    public function getKecamatan(Request $request){
        $kecamatan = $this->tb_kecamatan::where('id_kecamatan', '!=', 0)->pluck('id_kecamatan','kecamatan');
        return response()->json($kecamatan);
    }
    
    public function getDesa($id){
        $desa = $this->tb_desa::where("id_kecamatan",$id)->pluck('id_desa','desa');
        return response()->json($desa);
    }

    public function upload($file){
        $name  = $file->getClientOriginalName();
        $ext   = $file->getClientOriginalExtension();

        // isi dengan nama folder tempat kemana file diupload
        $path = 'data_file/gapoktan/';

        // upload file
        $file->move($path,$file->getClientOriginalName());

        $data = array(
                'name' => $name,
                'ext'  => $ext
                );

        return $data;
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_gapoktan->remove($id);
        if($delete){
            return redirect()->route('gapoktan')
            ->with('success', 'Data Berhasil Dihapus');
        }
        
    }
    
    public function showData($id)
    {
        return view('gapoktan.showdata');
    }

}
