<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Gis;
use App\Models\Download;
use App\Models\Harga;
use App\Models\Kecamatan;
use App\Models\Desa;
use App\Models\Article;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon as CarbonDate;

class MainController extends Controller
{
    protected $tb_user;

    public function __construct()
    {
        $this->tb_page = new Page();
        $this->tb_article = new Article();    
        $this->tb_gis = new Gis();
        $this->tb_download = new Download();
        $this->tb_harga = new Harga();
        $this->tb_kecamatan = new Kecamatan();
        $this->tb_desa = new Desa();
    }

    public function index(){
        $articles = $this->tb_article->select_all_slide();
        //echo"<pre>";
        //print_r($articles);
        //echo"</pre>";
        return view('main.index', compact('articles'));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gis(Request $request)
    {
        /* GIS */
        $boxmap = $this->tb_gis->select_all_map();

        $dataMap  = Array();
        $dataMap['type']='FeatureCollection';
        $dataMap['features']=array();
        foreach($boxmap as $value){
                $feaures = array();
                $feaures['type']='Feature';
                $geometry = array("type"=>"Point","coordinates"=>[$value->lng, $value->lat]);
                $feaures['geometry']=$geometry;
                $properties=array('title'=>$value->nama,"description"=>$value->alamat."<br>Telp.".$value->telp);
                $feaures['properties']= $properties;
                array_push($dataMap['features'],$feaures);

        }

        //menampilkan detail data dengan menemukan/berdasarkan id user
        //$gis = $this->tb_gis->select_one($id);
        //return view('gis.detail', compact('gis'));
       $data = array(
                    'dataArray' => json_encode($dataMap),
                    'gis'       => $boxmap,
                );
       return View('main.gis')->with($data);

    }

    public function download(Request $request)
    {

        if ($request->ajax()) {
            
            $data = $this->tb_download->select_all_ajax();
            return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('download', function($row){

                                return '<a href="data_file/'.$row->nama_file.'" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-cloud-download"></i></a>';
                        })
                        ->addColumn('action', function($row){

                               $btn = '<a href="download/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="download/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action','download'])
                        ->make(true);
        }

        return view('main.download');
    }

    public function harga(Request $request)
    {

        if ($request->ajax()) {
            
            $data = $this->tb_harga->select_all_join_sembako();
            return Datatables::of($data)

                        ->addIndexColumn()
                        ->addColumn('updated_at', function($row){

                               return CarbonDate::parse($row->updated_at)->format('d-m-Y');
                        })
                        ->addColumn('harga_sebelum', function($row){

                            return number_format($row->harga_sebelum, 2);
                        })
                        ->addColumn('harga_sesudah', function($row){

                            return number_format($row->harga_sesudah, 2);
                        })
                        ->make(true);
        }

        return view('main.harga');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function page($slug)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user
        $pages = $this->tb_page->select_page($slug);
        return view('main.'.$slug, compact('pages'));
    }
    
    public function sekretariat()
    {
        return view('main.sekretariat');
    }
    public function ketahanan_pangan()
    {
        return view('main.ketahanan-pangan');
    }
    public function tanaman_pangan()
    {
        return view('main.tanaman-pangan');
    }
    public function hortikultura()
    {
        return view('main.hortikultura');
    }
    public function perkebunan()
    {
        return view('main.perkebunan');
    }
    public function pskp()
    {
        return view('main.pskp');
    }
    public function gapoktan()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        $kec_des = $this->tb_kecamatan->select_all();
        $kec_utama = $this->tb_kecamatan->select_all();
        $kec_usaha = $this->tb_kecamatan->select_all();
        $kec_jenis = $this->tb_kecamatan->select_all();
        $desa = $this->tb_desa->select_all();
        return view('main.gapoktan', compact('kecamatan','desa','kec_des','kec_utama','kec_usaha','kec_jenis'));
    }
    public function blog_register()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('main.register', compact('kecamatan'));
    }
    public function blog_login()
    {
        return view('main.login');
    }
    public function sikep_login()
    {
        return view('main.login');
    }

    public function baca($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user
        $articles = $this->tb_article->select_one_article($id);
        return view('main.baca', compact('articles'));
    }
}
