<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Komoditas;
use Illuminate\Http\Request;
use DataTables;

class KomoditasController extends Controller
{
    protected $tb_komoditas;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_komoditas = new Komoditas();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
                $data = $this->tb_komoditas->select_all_ajax();
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="komoditas/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="komoditas/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('komoditas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('komoditas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
        ]);
        
        $this->tb_komoditas->store($request);

        return redirect()->route('komoditas')->withStatus(__('Data komoditas berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $komoditas = $this->tb_komoditas->select_one($id);
        return view('komoditas.edit',compact('komoditas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
        ]);

        $update = $this->tb_komoditas->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('komoditas')
            ->with('success', 'Data komoditas Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_komoditas->remove($id);
        if($delete){
            return redirect()->route('komoditas')
            ->with('success', 'Data komoditas Berhasil Dihapus');
        }
        
    }

}
