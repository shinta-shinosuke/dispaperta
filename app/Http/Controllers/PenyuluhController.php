<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Penyuluh;
use App\Models\Kecamatan;
use Illuminate\Http\Request;
use DataTables;

class PenyuluhController extends Controller
{
    protected $tb_penyuluh;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_penyuluh = new Penyuluh();
        $this->tb_kecamatan = new Kecamatan();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_penyuluh->select_all_join_null();
            }else{
                $data = $this->tb_penyuluh->select_all_join($search);
            }
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="penyuluh/edit/'.$row->penyuluh_id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="penyuluh/destroy/'.$row->penyuluh_id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('penyuluh.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        return view('penyuluh.create',compact('kecamatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'nip'   => 'required',
            'name'      => 'required',
            'kecamatan' => 'required',
            'nama_unor' => 'required',
            'jabatan' => 'required',
            'kelas_jabatan' => 'required',
            'unor_induk_id' => 'required',
            'jabatan_fungsional_id' => 'required',
        ]);
        
        $this->tb_penyuluh->store($request);

        return redirect()->route('penyuluh')->withStatus(__('Data penyuluh berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $kecamatan = $this->tb_kecamatan->select_all();
        $penyuluh = $this->tb_penyuluh->select_one($id);
        return view('penyuluh.edit',compact('penyuluh','kecamatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'nip'   => 'required',
            'name'      => 'required',
            'kecamatan' => 'required',
            'nama_unor' => 'required',
            'jabatan' => 'required',
            'kelas_jabatan' => 'required',
            'unor_induk_id' => 'required',
            'jabatan_fungsional_id' => 'required',
        ]);

        $update = $this->tb_penyuluh->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('penyuluh')
            ->with('success', 'Data penyuluh Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_penyuluh->remove($id);
        if($delete){
            return redirect()->route('penyuluh')
            ->with('success', 'Data penyuluh Berhasil Dihapus');
        }
        
    }

}
