<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Usaha;
use Illuminate\Http\Request;
use DataTables;

class UsahaController extends Controller
{
    protected $tb_usaha;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_usaha = new Usaha();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
/*
        if ($request->ajax()) {
            $search = (empty($request->input('search'))) ? '' : $request->input('search');
            if($search!= ''){
                $data = $this->tb_usaha->select_all_join_jenis_null();
            }else{
                $data = $this->tb_usaha->select_all_join_jenis($search);
            }
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="usaha/edit/'.$row->id_usaha.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="usaha/destroy/'.$row->id_usaha.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('usaha.index');*/
        return view('usaha.jenis');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        $jenis_usaha = $this->tb_usaha->select_all_jenis();
        return view('usaha.create',compact('jenis_usaha'));
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
            'jenis_usaha' => 'required'
        ]);
        
        $this->tb_usaha->store($request);

        return redirect()->route('usaha')->withStatus(__('Data usaha berhasil disimpan.'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $jenis_usaha = $this->tb_usaha->select_all_jenis();
        $usaha = $this->tb_usaha->select_one($id);
        return view('usaha.edit',compact('usaha','jenis_usaha'));
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
            'jenis_usaha' => 'required'
        ]);

        $update = $this->tb_usaha->modify($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('usaha')
            ->with('success', 'Data usaha Berhasil Diupdate');
        }

        
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_usaha->remove($id);
        if($delete){
            return redirect()->route('usaha')
            ->with('success', 'Data usaha Berhasil Dihapus');
        }
        
    }*/

    /* ########################## JENIS usaha ####################################### */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jenis(Request $request)
    {

        if ($request->ajax()) {
                $data = $this->tb_usaha->select_all_jenis();
            
                return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('action', function($row){

                               $btn = '<a href="jenis/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="jenis/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

        return view('usaha.jenis');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_jenis()
    {
        return view('usaha.create_jenis');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_jenis(Request $request)
    {
         //validation parameters
        $request->validate([
            'name'      => 'required',
        ]);
        
        $this->tb_usaha->store_jenis($request);

        return redirect()->route('usaha.jenis')->withStatus(__('Data Jenis usaha berhasil disimpan.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_jenis($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $usaha = $this->tb_usaha->select_one_jenis($id);
        return view('usaha.edit_jenis',compact('usaha'));
        //echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_jenis(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'name' => 'required',
        ]);

        $update = $this->tb_usaha->modify_jenis($request, $id);

        if($update)
        {
            //jika data berhasil diupdate, akan kembali ke halaman utama
            return redirect()->route('usaha.jenis')
            ->with('success', 'Data Jenis usaha Berhasil Diupdate');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_jenis($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_usaha->remove_jenis($id);
        if($delete){
            return redirect()->route('usaha.jenis')
            ->with('success', 'Data Jenis usaha Berhasil Dihapus');
        }
        
    }

}
