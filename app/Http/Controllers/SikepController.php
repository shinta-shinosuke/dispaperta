<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Gis;
use App\Models\Download;
use App\Models\Harga;
use App\Models\Kecamatan;
use App\Models\Desa;
use App\Models\Sikep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Carbon\Carbon as CarbonDate;

class SikepController extends Controller
{
    protected $tb_user;

    public function __construct()
    {
        $this->tb_page = new Page();
        $this->tb_gis = new Gis();
        $this->tb_download = new Download();
        $this->tb_harga = new Harga();
        $this->tb_kecamatan = new Kecamatan();
        $this->tb_desa = new Desa();
        $this->tb_sikep = new Sikep();
    }

    public function index(){
       return View('sikep.index');
    }

    public function sikep_register()
    {
        $kecamatan = $this->tb_kecamatan->select_all();
        $desa = $this->tb_desa->select_all();
        return view('sikep.register', compact('kecamatan','desa'));
    }
    
    public function sikep_login()
    {
        return view('sikep.login');
    }

    public function getKecamatan(Request $request){
        $kecamatan = DB::connection('mysql2')->table('ref_kecamatan')->where('kode_provinsi', '=', '33')->where('kode_kota_kab'.'='.'25')->pluck('id','kecamatan');
        return response()->json($kecamatan);
    }
    
    public function getDesa($id){
        $desa = DB::connection('mysql2')->table('ref_desa')->where('kode_provinsi', '=', '33')->where('kode_kota_kab','=','25')->where('kode_kecamatan','=',$id)->pluck('kode_cepat','nama');
        return response()->json($desa);
    }
}
