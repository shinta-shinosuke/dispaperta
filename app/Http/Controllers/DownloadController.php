<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Download;
use Illuminate\Http\Request;
use DataTables;

class DownloadController extends Controller
{
    protected $tb_download;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tb_download = new Download();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            
            $data = $this->tb_download->select_all_ajax();
            return Datatables::of($data)

                        ->addIndexColumn()

                        ->addColumn('download', function($row){

                                return '<a href="data_file/'.$row->nama_file.'" target="_blank">'.$row->nama_file.'</a>';
                        })
                        ->addColumn('action', function($row){

                               $btn = '<a href="download/edit/'.$row->id.'" class="edit btn btn-primary btn-sm">Edit</a>&nbsp;&nbsp;&nbsp;<a href="download/destroy/'.$row->id.'" class="edit btn btn-danger btn-sm">Delete</a>';    
                                return $btn;
                        })
                        ->rawColumns(['action','download'])
                        ->make(true);
        }

        return view('download.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('download.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validation parameters
        $request->validate([
            'tentang'     => 'required',
            'file'        => 'required'
        ]);

        // menyimpan data file yang diupload ke variabel $file
        $file  = $request->file('file');

        $upload = $this->upload($file);

        if($upload){
            $data = $this->tb_download->store($upload['name'], $upload['ext'], $request->tentang);

            if($data)
            {
                //jika data berhasil ditambahkan, akan kembali ke halaman utama
                return redirect()->route('download')->withStatus(__('Data berhasil ditambahkan.'));
            }
        }

    }

    public function upload($file){
        $name  = $file->getClientOriginalName();
        $ext   = $file->getClientOriginalExtension();

        // isi dengan nama folder tempat kemana file diupload
        $path = 'data_file';

        // upload file
        $file->move($path,$file->getClientOriginalName());

        $data = array(
                'name' => $name,
                'ext'  => $ext
                );

        return $data;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //menampilkan detail data dengan menemukan/berdasarkan id user
        $download = $this->tb_download->select_one($id);
        return view('download.detail', compact('download'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //menampilkan detail data dengan menemukan/berdasarkan id user untuk diedit
        $download = $this->tb_download->select_one($id);
        return view('download.edit', compact('download'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //melakukan validasi data
        $request->validate([
            'tentang'     => 'required',
        ]);

        if(!empty($request->file('file'))){
            // menyimpan data file yang diupload ke variabel $file
            $file  = $request->file('file');

            $upload = $this->upload($file);

            if($upload){
                $update = $this->tb_download->modify($upload['name'], $upload['ext'], $request->tentang, $id);

                if($update)
                {
                    //jika data berhasil diupdate, akan kembali ke halaman utama
                    return redirect()->route('download')->with('success', 'Data Berhasil Diupdate');
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //fungsi eloquent untuk menghapus data
        $delete = $this->tb_download->remove($id);
        if($delete){
            return redirect()->route('download')
            ->with('success', 'Data Berhasil Dihapus');
        }
        
    }
}
