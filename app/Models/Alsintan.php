<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Alsintan extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_alsintan'
    ];

    protected $table = 'view_alsintan';

    protected  $primaryKey = 'id_alsintan';

    protected $connection = 'mysql2';

    public function select_one($id){
        $data = Alsintan::find($id);
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'nama_alsintan'      => $request['name'],
                    'jenis_alsintan_id'      => $request['jenis_alsintan'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('alsintan')->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'nama_alsintan'      => $request['name'],
                    'jenis_alsintan_id'      => $request['jenis_alsintan'],
                    );

        $data = DB::connection('mysql2')->table('alsintan')->where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove($id){
        $data = DB::connection('mysql2')->table('alsintan')->where('id','=',$id)->delete();
        return $data;
    }

    /* JENIS ALSINTAN */

    public function select_all_jenis(){
        $data = DB::connection('mysql2')->table('jenis_alsintan')->select('*')->get();
        
        return $data->toArray();
    }

    public function select_all_jenis_ajax(){
        $data = Alsintan::select(array('jenis_alsintan.id','jenis_alsintan.jenis_alsintan'));  

        return $data;
    }

    public function select_one_jenis($id){
        $data = DB::connection('mysql2')->table('jenis_alsintan')->where('id','=',$id)->first();
            return $data;
    }

    public function store_jenis($request){

        $request = $request->toArray();

        $udata = array(
                    'jenis_alsintan'      => $request['name'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('jenis_alsintan')->insert($udata);

        return $data;
        
    }

    public function modify_jenis($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'jenis_alsintan'      => $request['name'],
                    );

        $data = DB::connection('mysql2')->table('jenis_alsintan')->where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove_jenis($id){
        $data = DB::connection('mysql2')->table('jenis_alsintan')->where('id','=',$id)->delete();
        return $data;
    }

    /* END JENIS ALSINTAN */

    public function select_all_join_jenis($search){
        $data = Alsintan::select(array('view_alsintan.id_alsintan','view_alsintan.nama_alsintan','view_alsintan.jenis_alsintan'))
                            ->where('view_alsintan.nama_alsintan','like',$search)
                            ->orWhere('view_alsintan.jenis_alsintan','like',$search)
                            ->get();  

        return $data;
    }

    public function select_all_join_jenis_null(){
        $data = Alsintan::select(array('view_alsintan.id_alsintan','view_alsintan.nama_alsintan','view_alsintan.jenis_alsintan'))->get();  

        return $data;
    }
}
