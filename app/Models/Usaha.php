<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Usaha extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama'
    ];

    protected  $primaryKey = 'id';

    protected $connection = 'mysql2';
/*
    public function select_one($id){
        $data = usaha::find($id);
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'nama_usaha'      => $request['name'],
                    'jenis_usaha_id'      => $request['jenis_usaha'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('usaha')->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'nama_usaha'      => $request['name'],
                    'jenis_usaha_id'      => $request['jenis_usaha'],
                    );

        $data = DB::connection('mysql2')->table('usaha')->where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove($id){
        $data = DB::connection('mysql2')->table('usaha')->where('id','=',$id)->delete();
        return $data;
    }*/

    /* JENIS usaha */

    public function select_all_jenis(){
        $data = DB::connection('mysql2')->table('ms_jenis_usaha')->select('*')->get();
        
        return $data->toArray();
    }

    public function select_all_jenis_ajax(){
        $data = usaha::select(array('jenis_usaha.id','jenis_usaha.nama'));  

        return $data;
    }

    public function select_one_jenis($id){
        $data = DB::connection('mysql2')->table('ms_jenis_usaha')->where('id','=',$id)->first();
            return $data;
    }

    public function store_jenis($request){

        $request = $request->toArray();

        $udata = array(
                    'nama'      => $request['name'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('ms_jenis_usaha')->insert($udata);

        return $data;
        
    }

    public function modify_jenis($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'nama'      => $request['name'],
                    );

        $data = DB::connection('mysql2')->table('ms_jenis_usaha')->where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove_jenis($id){
        $data = DB::connection('mysql2')->table('ms_jenis_usaha')->where('id','=',$id)->delete();
        return $data;
    }

    /* END JENIS usaha */

    /*public function select_all_join_jenis($search){
        $data = usaha::select(array('view_usaha.id_usaha','view_usaha.nama_usaha','view_usaha.jenis_usaha'))
                            ->where('view_usaha.nama_usaha','like',$search)
                            ->orWhere('view_usaha.jenis_usaha','like',$search)
                            ->get();  

        return $data;
    }

    public function select_all_join_jenis_null(){
        $data = usaha::select(array('view_usaha.id_usaha','view_usaha.nama_usaha','view_usaha.jenis_usaha'))->get();  

        return $data;
    }*/
}
