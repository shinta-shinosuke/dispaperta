<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Poktan extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'poktan_id', 'poktan_nama'
    ];

    protected $table = 'view_poktan';

    protected $connection = 'mysql2';

    protected  $primaryKey = 'poktan_id';


    public function select_all(){
        $data = Poktan::select('*')->get();
            return $data->toArray();
    }

    public function select_one($id){
        $data = Poktan::select('*')
                           ->where('poktan_id','=',$id)
                           ->get();
            return $data->toArray();
    }

    public function select_all_gapoktan(){
        $data = DB::connection('mysql2')
                                ->table('ms_gapoktan')
                                ->select('*')
                                ->get();
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'gapoktan_id'      => $request['gapoktan'],
                    'gapoktan_nama'    => $request['gapoktan_nama'],
                    'kode_desa'        => $request['kode_desa'],
                    'nama_poktan'      => $request['name'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')
                                ->table('ms_poktan')
                                ->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();

        $udata = array(
                    'gapoktan_id'      => $request['gapoktan'],
                    'gapoktan_nama'    => $request['gapoktan_nama'],
                    'kode_desa'        => $request['kode_desa'],
                    'nama_poktan'      => $request['name'],
                    );

        $data = DB::connection('mysql2')
                                ->table('ms_poktan')
                                ->where('id','=',$id)
                                ->update($udata);

        return $data;
    }

    public function remove($id){
        $data = DB::connection('mysql2')
                                ->table('ms_poktan')
                                ->where('id','=',$id)->delete();
        return $data;
    }

    public function select_all_null(){
        $data = Poktan::select(array('view_poktan.poktan_id','view_poktan.poktan_nama','view_poktan.gapoktan_nama','view_poktan.desa_nama','view_poktan.kecamatan_nama','view_poktan.penyuluh_nama'))
                        ->get();  

        return $data;
    }
    
    public function select_all_join($search){
            $data = Poktan::select(array('view_poktan.poktan_id','view_poktan.poktan_nama','view_poktan.gapoktan_nama','view_poktan.desa_nama','view_poktan.kecamatan_nama','view_poktan.penyuluh_nama'))
                            ->where('poktan_nama','like',$search)
                            ->orWhere('gapoktan_nama','like',$search)
                            ->orWhere('desa_nama','like',$search)
                            ->orWhere('kecamatan_nama','like',$search)
                            ->orWhere('penyuluh_nama','like',$search)
                            ->get(); 

        return $data;
    }

    public function check($search){
        $data = DB::connection('mysql2')
                                ->table('view_gapoktan')
                                ->select(array('gapoktan_id','gapoktan_nama','desa_kode','desa_nama','kecamatan_kode','kecamatan_nama','penyuluh_nama'))
                                ->where('gapoktan_id','=',$search['id'])
                                ->first();     
        
        return $data;
    }
}
