<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Models\Kecamatan;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_lengkap',
        'name',
        'email',
        'password',
        'phone',
        'level',
        'kode_kecamatan',
        'kode_desa',
        'alamat'
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $kecamatan = ('mysql2.ref_kecamatan as kecamatans');
    protected $user = ('mysql.user as users');

    public function select_all(){
        $data = User::select('*');
            return $data;
    }

    public function select_one($id){
        $data = User::find($id);
            return $data;
    }

    public function store($request){

        $request = $request->toArray();

        if(empty($request['kecamatan'])){
            $request['kecamatan'] = '0';
        }
        $udata = array(
                    'remember_token' => $request['_token'],
                    'nama_lengkap'   => $request['fullname'],
                    'name'           => $request['name'],
                    'email'          => $request['email'],
                    'password'       => Hash::make($request['password']),
                    'phone'          => $request['phone'],
                    'level'          => $request['level'],
                    'id_kecamatan'   => $request['kecamatan'],
                    );

        //print_r($data);
        $user = User::create($udata);

        return $user;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();

        if(empty($request['kecamatan'])){
            $request['kecamatan'] = '0';
        }
        $udata = array(
                    'remember_token' => $request['_token'],
                    'nama_lengkap'   => $request['fullname'],
                    'name'           => $request['name'],
                    'email'          => $request['email'],
                    'phone'          => $request['phone'],
                    'level'          => $request['level'],
                    'id_kecamatan'   => $request['kecamatan'],
                    );

        if($request['password'] != ''){
            $udata += array('password' => Hash::make($request['password']));
        }
        
        $user = User::find($id)->update($udata);

        return $user;
    }

    public function remove($id){
        $data = User::find($id)->delete();
        return $data;
    }

    public function select_all_join_kecamatan($search){
        $users = DB::connection('mysql')->table('view_new_user')->select('*')
                        ->where('name','like',$search)
                        ->orWhere('nama_lengkap','like',$search)
                        ->orWhere('phone','like',$search)
                        ->orWhere('level','like',$search)
                        ->orWhere('kecamatan','like',$search)
                        ->orWhere('desa','like',$search)
                        ->get();  
        return $users;

        return $users;
    }

    public function select_all_join_kecamatan_null(){
$users = DB::connection('mysql')->table('view_new_user')->select('*')
                        ->get();  
        return $users;
    }

    public function kecamatan() {
        return $this->belongsTo('App\Models\Kecamatan','id_kecamatan');
    }

    public function users(){
        return $this->hasMany('App\Model\User','id_kecamatan');
    }
}
