<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DataTables;

class Kecamatan extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'kode_provinsi', 'kode_kota_kab', 'kode', 'nama', 'kode_cepat'
    ];

    protected $table = 'ref_kecamatan';

    protected $connection = 'mysql2';

    protected  $primaryKey = 'id';

    public function select_all(){
        $data = Kecamatan::select('*')->where('ref_kecamatan.kode_provinsi','33')->where('ref_kecamatan.kode_kota_kab','25')->get();
            return $data->toArray();
    }

    public function select_one($id){
        $data = Kecamatan::select('*')
                           ->where('id','=',$id)
                           ->get();
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'kode_provinsi' => '33',
                    'kode_kota_kab' => '25',
                    'kode'      => $request['kode'],
                    'nama'      => $request['name'],
                    'kode_cepat' =>'33.25.'.$request['kode'],
                    );

        //print_r($data);
        $data = Kecamatan::create($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();

        $data = Kecamatan::where('id','=',$id);

        $udata = array(
                    'nama'      => $request['name'],
                    );

        $user = Kecamatan::where('id','=',$id)->update($udata);

        return $user;
    }

    public function remove($id){
        $data = Kecamatan::where('id','=',$id)->delete();
        return $data;
    }

    public function select_all_ajax(){
        $data = Kecamatan::select(array('ref_kecamatan.id','ref_kecamatan.kode_provinsi', 'ref_kecamatan.kode_kota_kab', 'ref_kecamatan.kode', 'ref_kecamatan.nama', 'ref_kecamatan.kode_cepat'))
                        ->where('ref_kecamatan.kode_provinsi','33')
                        ->where('ref_kecamatan.kode_kota_kab','25');  

        return $data;
    }
}
