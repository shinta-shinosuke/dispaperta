<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DataTables;

class Download extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tentang',
        'nama_file',
        'extensi',
    ];

    public function select_all(){
        $data = Download::select('*');
            return $data;
    }

    public function select_one($id){
        $data = Download::find($id);
            return $data;
    }

    public function store($name, $ext, $about){

        $udata = array(
                    'nama_file'  => $name,
                    'extensi'    => $ext,
                    'tentang'    => $about,
                    );

        //print_r($data);
        $download = Download::create($udata);

        return $download;
        
    }

    public function modify($name, $ext, $about, $id){
        
        $udata = array(
                    'nama_file'  => $name,
                    'extensi'    => $ext,
                    'tentang'    => $about,
                    );

        $download = Download::find($id)->update($udata);

        return $download;
    }

    public function remove($id){
        $data = Download::find($id)->delete();
        return $data;
    }

    public function select_all_ajax(){
        $data = Download::select(array('downloads.id','downloads.tentang','downloads.extensi','downloads.nama_file'));  

        return $data;
    }
}
