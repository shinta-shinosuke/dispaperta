<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Refkomoditas extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_komoditas'
    ];

    protected  $table = 'ref_komoditas';

    protected  $primaryKey = 'id_komoditas';

    protected $connection = 'mysql2';

    public function select_one($id){
        $data = Refkomoditas::find($id);
            return $data->toArray();
    }

     public function select_all(){
         $data = Refkomoditas::select('*')->get();
            return $data->toArray();
    }

    public function select_all_ajax(){
        $data = Refkomoditas::select(array('ref_komoditas.id_komoditas','ref_komoditas.nama_komoditas','ref_komoditas.varietas','ref_komoditas.jenis'));  

        return $data;
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'nama_komoditas'      => $request['name'],
                    'varietas'      => $request['varietas'],
                    'jenis'      => $request['jenis'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('ref_komoditas')->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'nama_komoditas'      => $request['name'],
                    'varietas'      => $request['varietas'],
                    'jenis'      => $request['jenis'],
                    );

        $data = DB::connection('mysql2')->table('ref_komoditas')->where('id_komoditas','=',$id)->update($udata);

        return $data;
    }

    public function remove($id){
        $data = DB::connection('mysql2')->table('ref_komoditas')->where('id_komoditas','=',$id)->delete();
        return $data;
    }
}
