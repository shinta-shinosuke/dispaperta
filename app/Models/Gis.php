<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DataTables;

class Gis extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'alamat',
        'telp',
        'lng',
        'lat',
    ];

    public function select_all(){
        $data = Gis::select('*');
            return $data;
    }

    public function select_one($id){
        $data = Gis::find($id);
            return $data;
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'nama'      => $request['nama'],
                    'alamat'    => $request['alamat'],
                    'telp'      => $request['telp'],
                    'lat'       => $request['lat'],
                    'lng'       => $request['lng'],
                    );

        //print_r($data);
        $GIS = Gis::create($udata);

        return $GIS;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();

        $udata = array(
                    'nama'      => $request['nama'],
                    'alamat'    => $request['alamat'],
                    'telp'      => $request['telp'],
                    'lat'       => $request['lat'],
                    'lng'       => $request['lng'],
                    );

       $gis = Gis::find($id)->update($udata);

        return $gis;
    }

    public function remove($id){
        $data = Gis::find($id)->delete();
        return $data;
    }

    public function select_all_ajax(){
        $data = Gis::select(array('gis.id','gis.nama','gis.alamat','gis.telp','gis.lng','gis.lat'));  

        return $data;
    }

    public function select_all_map(){
        $data = Gis::select(array('gis.id','gis.nama','gis.alamat','gis.telp','gis.lng','gis.lat'))->get();  

        return $data;
    }

    public function select_map($id){
        $data = Gis::select(array('gis.id','gis.nama','gis.alamat','gis.telp','gis.lng','gis.lat'))->where('gis.id','=',$id)->get();  

        return $data;
    }
}
