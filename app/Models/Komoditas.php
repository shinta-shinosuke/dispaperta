<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Komoditas extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_komoditas'
    ];

    protected  $table = 'ms_komoditas';

    protected  $primaryKey = 'id';

    protected $connection = 'mysql2';

    public function select_one($id){
        $data = Komoditas::find($id);
            return $data->toArray();
    }

     public function select_all(){
         $data = Komoditas::select('*')->get();
            return $data->toArray();
    }

    public function select_all_ajax(){
        $data = Komoditas::select(array('ms_komoditas.id','ms_komoditas.nama_komoditas'));  

        return $data;
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'nama_komoditas'      => $request['name'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('ms_komoditas')->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'nama_komoditas'      => $request['name'],
                    );

        $data = DB::connection('mysql2')->table('ms_komoditas')->where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove($id){
        $data = DB::connection('mysql2')->table('ms_komoditas')->where('id','=',$id)->delete();
        return $data;
    }
}
