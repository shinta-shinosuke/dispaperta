<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Penyuluh extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'nip',
            'nama',
            'kecamatan',
            'nama_unor',
            'jabatan',
            'kelas_jabatan',
            'unor_induk_id',
            'jabatan_fungsional_id',
            'created_at',
            'updated_at'
    ];

    protected  $primaryKey = 'id';

    protected $connection = 'mysql2';

    protected $table = 'penyuluh';

    public function select_one($id){
        $data = Penyuluh::find($id);
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'nip'   => $request['nip'],
                    'nama'      => $request['name'],
                    'kode_kecamatan' => $request['kecamatan'],
                    'nama_unor' => $request['nama_unor'],
                    'jabatan' => $request['jabatan'],
                    'kelas_jabatan' => $request['kelas_jabatan'],
                    'unor_induk_id' => $request['unor_induk_id'],
                    'jabatan_fungsional_id' => $request['jabatan_fungsional_id'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('penyuluh')->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        
        $request = $request->toArray();
        
        $udata = array(
                    'nip'   => $request['nip'],
                    'nama'      => $request['name'],
                    'kode_kecamatan' => $request['kecamatan'],
                    'nama_unor' => $request['nama_unor'],
                    'jabatan' => $request['jabatan'],
                    'kelas_jabatan' => $request['kelas_jabatan'],
                    'unor_induk_id' => $request['unor_induk_id'],
                    'jabatan_fungsional_id' => $request['jabatan_fungsional_id'],
                    );

        $data = DB::connection('mysql2')->table('penyuluh')->where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove($id){
        $data = Penyuluh::find($id)->delete();
        return $data;
    }

    public function select_all_join($search){
        $data = Penyuluh::select(array('penyuluh.id as penyuluh_id', 
                                        'penyuluh.nip as nip',
                                        'penyuluh.nama as penyuluh_nama', 
                                        'penyuluh.kode_kecamatan as kode_kecamatan',
                                        'ref_kecamatan.kode_cepat as kecamatan_kode', 
                                        'ref_kecamatan.nama as kecamatan_nama'))
                            ->leftJoin('ref_kecamatan', 'penyuluh.kode_kecamatan', '=', 'ref_kecamatan.kode_cepat')
                            ->where('ref_kecamatan.kode_provinsi','=','33')
                            ->where('ref_kecamatan.kode_kota_kab','=','25')
                            ->orWhere('penyuluh.nip','like',$search)
                            ->orWhere('penyuluh.nama','like',$search)
                            ->orWhere('ref_kecamatan.nama','like',$search)
                            ->get();  

        return $data;
    }

    public function select_all_join_null(){
        $data = Penyuluh::select(array('penyuluh.id as penyuluh_id', 
                                        'penyuluh.nip as nip',
                                        'penyuluh.nama as penyuluh_nama', 
                                        'penyuluh.kode_kecamatan as kode_kecamatan',
                                        'ref_kecamatan.kode_cepat as kecamatan_kode', 
                                        'ref_kecamatan.nama as kecamatan_nama'))
                            ->leftJoin('ref_kecamatan', 'penyuluh.kode_kecamatan', '=', 'ref_kecamatan.kode_cepat')
                            ->where('ref_kecamatan.kode_provinsi','=','33')
                            ->where('ref_kecamatan.kode_kota_kab','=','25')
                            ->get();  
        return $data;
    }
}
