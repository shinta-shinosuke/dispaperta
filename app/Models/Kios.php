<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Kios extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_kios',
        'nama_kios',
        'kode_kecamatan'
    ];

    protected  $table = 'view_kios';

    protected  $primaryKey = 'id_kios';

    protected $connection = 'mysql2';

    public function select_one($id){
        $data = Kios::find($id);
            return $data->toArray();
    }

     public function select_all(){
         $data = Kios::select('*')->get();
            return $data->toArray();
    }

    public function select_all_join_kecamatan_null(){
        $data = Kios::select(array('view_kios.id_kios','view_kios.kode_kios','view_kios.nama_kios','view_kios.kode_kecamatan','view_kios.nama_kecamatan','view_kios.id_kecamatan'))->get();  

        return $data;
    }
    
    public function select_all_join_kecamatan($search){
            $data = Kios::select(array('view_kios.id_kios','view_kios.kode_kios','view_kios.nama_kios','view_kios.kode_kecamatan','view_kios.nama_kecamatan','view_kios.id_kecamatan'))
                            ->where('nama_kecamatan','like',$search)
                            ->orWhere('nama_kios','like',$search)
                            ->orWhere('kode_kecamatan','like',$search)
                            ->orWhere('kode_kios','like',$search)
                            ->get(); 

        return $data;
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'nama_kios'      => $request['name'],
                    'kode_kios'      => $request['kode'],
                    'kode_kecamatan'      => $request['kecamatan'],
                    );

        //print_r($data);
        $data = DB::connection('mysql2')->table('ms_kios')->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'nama_kios'      => $request['name'],
                    'kode_kios'      => $request['kode'],
                    'kode_kecamatan'      => $request['kecamatan'],
                    );

        $data = DB::connection('mysql2')->table('ms_kios')->where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove($id){
        $data = DB::connection('mysql2')->table('ms_kios')->where('id','=',$id)->delete();
        return $data;
    }
}
