<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use DataTables;

class Desa extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'kode_provinsi', 'kode_kota_kab', 'kode_kecamatan', 'kode', 'nama', 'kode_cepat'
    ];

    protected $table = 'view_desa';

    protected $connection = 'mysql2';

    protected  $primaryKey = 'id';


    public function select_all(){
        $data = DB::connection('mysql2')->table('ref_desa')->select('*')->where('ref_desa.kode_provinsi','33')->where('ref_desa.kode_kota_kab','25')->get();
            return $data->toArray();
    }

    public function select_one($id){
        $data = Desa::select('*')
                           ->where('id','=',$id)
                           ->get();
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

                //$check = DB::select(array('ref_desa.kode','ref_desa.kode_provinsi','ref_desa.kode_kota_kab','ref_desa.kode_kecamatan'))
        $check = DB::connection('mysql2')
                                ->table('ref_desa')
                                ->select('ref_desa.*')
                                ->where('ref_desa.kode_provinsi','33')
                                ->where('ref_desa.kode_kota_kab','25')
                                ->where('ref_desa.kode_kecamatan',$request['kecamatan'])
                                ->orderBy('ref_desa.kode','desc')
                                ->limit('1')
                                ->first();
        $udata = array(
                    'kode_provinsi' => '33',
                    'kode_kota_kab' => '25',
                    'kode_kecamatan'      => $request['kecamatan'],
                    'kode'      => ($check->kode+1),
                    'nama'      => $request['name'],
                    'kode_cepat' =>'33.25.'.$request['kecamatan'].'.'.($check->kode+1),
                    );

        //print_r($data);
        $data = DB::connection('mysql2')
                                ->table('ref_desa')
                                ->insert($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();

        $data = Desa::select('*')->where('id','=',$id)->get();
        $check = $data->toArray();

        if($check[0]['id_kecamatan'] != $request['kecamatan']){
            $check = DB::connection('mysql2')
                                ->table('ref_desa')
                                ->select('ref_desa.*')
                                ->where('ref_desa.kode_provinsi','33')
                                ->where('ref_desa.kode_kota_kab','25')
                                ->where('ref_desa.kode_kecamatan',$request['kecamatan'])
                                ->orderBy('ref_desa.kode','desc')
                                ->limit('1')
                                ->first();


                $kode = $check->kode+1;
        }else{
            $kode = $request['kode'];
        }

        $udata = array(
                    'kode_kecamatan'      => $request['kecamatan'],
                    'kode'      => $kode,
                    'nama'       => $request['name'],
                    'kode_cepat' =>'33.25.'.$request['kecamatan'].'.'.$kode,
                    );

        $data = DB::connection('mysql2')
                                ->table('ref_desa')
                                ->where('id','=',$id)
                                ->update($udata);

        return $data;
    }

    public function remove($id){
        $data = DB::connection('mysql2')
                                ->table('ref_desa')
                                ->where('id','=',$id)->delete();
        return $data;
    }

    public function select_all_ajax(){
        $data = Desa::select(array('ref_desa.id','ref_desa.desa','ref_desa.kode'))
                        ->where('ref_kecamatan.kode_provinsi','33')
                        ->where('ref_kecamatan.kode_kota_kab','25')
                        ->where('ref_desa.kode_provinsi','33')
                        ->where('ref_desa.kode_kota_kab','25');  

        return $data;
    }
    
    public function select_all_join_kecamatan($search){
            $data = Desa::select(array('view_desa.id','view_desa.id_kecamatan','view_desa.id_desa','view_desa.nama_kecamatan','view_desa.nama_desa','view_desa.kode_cepat'))
                            ->where('nama_kecamatan','like',$search)
                            ->orWhere('nama_desa','like',$search)
                            ->orWhere('kode_cepat','like',$search)
                            ->get(); 

        return $data;
    }

    public function select_all_join_kecamatan_null(){
            $data = Desa::select(array('view_desa.id','view_desa.id_kecamatan','view_desa.id_desa','view_desa.nama_kecamatan','view_desa.nama_desa','view_desa.kode_cepat'))
                            ->get(); 

        return $data;
    }

}
