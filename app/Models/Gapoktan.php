<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DataTables;

class Gapoktan extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'gapoktans';
    protected $fillable = [
        'id_kecamatan','id_desa','no_induk','nama','jenis_kelembagaan','utama','usaha','kelas_kelompok','jumlah_anggota','jenis_jumlah','luas_areal','jenis_areal','ketua','sekretaris','bendahara','tahun_pembentukan','jenis_pembentukan','hp','email','nama_file','extensi'
    ];

    protected  $primaryKey = 'id';

    public function select_all(){
        $data = Gapoktan::select('*')->where('id', '!=', 0)->get();
            return $data->toArray();
    }

    public function select_one($id){
        $data = Gapoktan::select('*')
                           ->where('id','=',$id)
                           ->get();
            return $data->toArray();
    }

    public function store($name, $ext, $request){

        $request = $request->toArray();

        $udata = array(
                    'id_kecamatan'      => $request['kecamatan'],
                    'id_desa'           => $request['desa'],
                    'no_induk'          => $request['no_induk'],
                    'nama'              => $request['nama'],
                    'jenis_kelembagaan' => $request['pelaku'],
                    'utama'             => $request['utama'],
                    'usaha'             => $request['usaha'],
                    'kelas_kelompok'    => $request['kelas_kelompok'],
                    'jumlah_anggota'    => $request['jumlah_anggota'],
                    'jenis_jumlah'      => $request['anggota'],
                    'luas_areal'        => $request['luas_areal'],
                    'jenis_areal'       => $request['luas'],
                    'ketua'             => $request['ketua'],
                    'sekretaris'        => $request['sekretaris'],
                    'bendahara'         => $request['bendahara'],
                    'tahun_pembentukan' => $request['tahun_pembentukan'],
                    'jenis_pembentukan' => $request['jenis_pembentukan'],
                    'hp'                => $request['hp'],
                    'email'             => $request['email'],
                    'extensi'           => $ext,
                    'nama_file'         => $name,
                    );

        //print_r($data);
        $data = Gapoktan::create($udata);

        return $data;
        
    }

    public function modify($name, $ext, $request, $id){
        $request = $request->toArray();

        $udata = array(
                    'id_kecamatan'      => $request['kecamatan'],
                    'id_desa'           => $request['desa'],
                    'no_induk'          => $request['no_induk'],
                    'nama'              => $request['nama'],
                    'jenis_kelembagaan' => $request['pelaku'],
                    'utama'             => $request['utama'],
                    'usaha'             => $request['usaha'],
                    'kelas_kelompok'    => $request['kelas_kelompok'],
                    'jumlah_anggota'    => $request['jumlah_anggota'],
                    'jenis_jumlah'      => $request['anggota'],
                    'luas_areal'        => $request['luas_areal'],
                    'jenis_areal'       => $request['luas'],
                    'ketua'             => $request['ketua'],
                    'sekretaris'        => $request['sekretaris'],
                    'bendahara'         => $request['bendahara'],
                    'tahun_pembentukan' => $request['tahun_pembentukan'],
                    'jenis_pembentukan' => $request['jenis_pembentukan'],
                    'hp'                => $request['hp'],
                    'email'             => $request['email'],
                    'extensi'           => $ext,
                    'nama_file'         => $name,
                    );

        $data = Gapoktan::where('id','=',$id)->update($udata);

        return $data;
    }

    public function remove($id){
        $data = Gapoktan::where('id','=',$id)->delete();
        return $data;
    }

    public function select_all_ajax(){
        $data = Gapoktan::select(array('gapoktans.*'));  

        return $data;
    }
    
    public function select_all_join_kecamatan(){
        $data = Gapoktan::Join('kecamatans','Gapoktans.id_kecamatan','=','kecamatans.id_kecamatan')
                        ->select(array('kecamatans.id_kecamatan','kecamatans.kecamatan','Gapoktans.id_Gapoktan','Gapoktans.Gapoktan'));  

        return $data;
    }

}
