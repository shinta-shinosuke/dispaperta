<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DataTables;

class Sikep extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Sikep', 'id_kecamatan'
    ];

    protected  $primaryKey = 'id_Sikep';

    public function select_all(){
        $data = Sikep::select('*')->where('id_Sikep', '!=', 0)->get();
            return $data->toArray();
    }

    public function select_one($id){
        $data = Sikep::select('*')
                           ->where('id_Sikep','=',$id)
                           ->get();
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'Sikep'      => $request['name'],
                    'id_kecamatan'      => $request['kecamatan'],
                    );

        //print_r($data);
        $data = Sikep::create($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();

        $data = Sikep::where('id_Sikep','=',$id);

        $udata = array(
                    'Sikep'      => $request['name'],
                    'id_kecamatan'      => $request['kecamatan'],
                    );

        $user = Sikep::where('id_Sikep','=',$id)->update($udata);

        return $user;
    }

    public function remove($id){
        $data = Sikep::where('id_Sikep','=',$id)->delete();
        return $data;
    }

    public function select_all_ajax(){
        $data = Sikep::select(array('Sikeps.id_Sikep','Sikeps.Sikep'));  

        return $data;
    }
    
    public function select_all_join_kecamatan(){
        $data = Sikep::Join('kecamatans','Sikeps.id_kecamatan','=','kecamatans.id_kecamatan')
                        ->select(array('kecamatans.id_kecamatan','kecamatans.kecamatan','Sikeps.id_Sikep','Sikeps.Sikep'));  

        return $data;
    }

}
