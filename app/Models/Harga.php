<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DataTables;
use App\Models\Sembako;

class Harga extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'blth',
        'id_sembako',
        'harga_sebelum',
        'harga_sesudah',
    ];

    protected $table = 'view_harga';

    public function select_all(){
        $data = Harga::select('*');
            return $data;
    }

    public function select_one($id){
        $data = Harga::find($id);
            return $data;
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'harga_sebelum'          => $request['harga_sebelum'],
                    'harga_sesudah'          => $request['harga_sesudah'],
                    'blth'                   => $request['blth'],
                    'id_sembako'             => $request['sembako'],
                    );

        //print_r($data);
        $Harga = Harga::create($udata);

        return $Harga;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();

        $udata = array(
                    'harga_sebelum'          => $request['harga_sebelum'],
                    'harga_sesudah'          => $request['harga_sesudah'],
                    'blth'                   => $request['blth'],
                    'id_sembako'             => $request['sembako'],
                    );

        $Harga = Harga::find($id)->update($udata);

        return $Harga;
    }

    public function remove($id){
        $data = Harga::find($id)->delete();
        return $data;
    }

    public function select_all_join_Sembako($search){
        $harga = Harga::select(array('view_harga.id_harga','view_harga.blth','view_harga.harga_sebelum','view_harga.harga_sesudah','view_harga.sembako', 'view_harga.created_at','view_harga.updated_at'))
                        ->where('view_harga.blth','like',$search)
                        ->orWhere('view_harga.harga_sebelum','like',$search)
                        ->orWhere('view_harga.harga_sesudah','like',$search)
                        ->orWhere('view_harga.sembako','like',$search)
                        ->orWhere('view_harga.updated_at','like',$search)
                        ->get();   

        return $harga;
    }

    public function select_all_join_Sembako_null(){
        $harga = Harga::select(array('view_harga.id_harga','view_harga.blth','view_harga.harga_sebelum','view_harga.harga_sesudah','view_harga.sembako', 'view_harga.created_at','view_harga.updated_at'))->get();  

        return $harga;
    }

    public function Sembako() {
        return $this->belongsTo('App\Models\Sembako','id');
    }

    public function Harga(){
        return $this->hasMany('App\Model\Harga','id_sembako');
    }
}
