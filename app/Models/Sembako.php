<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DataTables;

class Sembako extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sembako',
    ];

    protected  $primaryKey = 'id';

    public function select_all(){
        $data = Sembako::select('*')->get();
            return $data->toArray();
    }

    public function select_one($id){
        $data = Sembako::find($id);
            return $data->toArray();
    }

    public function store($request){

        $request = $request->toArray();

        $udata = array(
                    'sembako'      => $request['name'],
                    );

        //print_r($data);
        $data = Sembako::create($udata);

        return $data;
        
    }

    public function modify($request, $id){
        $request = $request->toArray();
        $udata = array(
                    'sembako'      => $request['name'],
                    );

        $user = Sembako::find($id)->update($udata);

        return $user;
    }

    public function remove($id){
        $data = Sembako::find($id)->delete();
        return $data;
    }

    public function select_all_ajax(){
        $data = Sembako::select(array('sembakos.id','sembakos.sembako'));  

        return $data;
    }
}
