
  
<?php $__env->startSection('content'); ?>

<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Komoditas
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
                <form method="post" action="<?php echo e(route('refkomoditas.update', $komoditas['id_komoditas'])); ?>" id="myForm">
                <?php echo csrf_field(); ?>
                <?php echo method_field('PUT'); ?>
                    <div class="form-group">
                        <label for="name">Nama Komoditas</label>                    
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo e($komoditas['nama_komoditas']); ?>" aria-describedby="name" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Varietas</label>                    
                        <input type="text" name="varietas" class="form-control" id="varietas" value="<?php echo e($komoditas['varietas']); ?>"" aria-describedby="Varietas" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Jenis</label>                    
                        <input type="text" name="jenis" class="form-control" id="jenis" value="<?php echo e($komoditas['jenis']); ?>"" aria-describedby="jenis" required="required">                
                    </div>
                    
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="<?php echo e(route('refkomoditas')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/refkomoditas/edit.blade.php ENDPATH**/ ?>