
<?php $__env->startSection('content'); ?>

<?php $__currentLoopData = $gis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <div class="container mt-5">
        <div class="row justify-content-center align-items-center">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <h2><?php echo e($gis['nama']); ?></h2>
                        <div id="map" style="width: auto; height: 600px;"></div>  
                        <script>
                            mapboxgl.accessToken = 'pk.eyJ1Ijoic2tpcHBlcmhvYSIsImEiOiJjazE2MjNqMjkxMTljM2luejl0aGRyOTAxIn0.Wyvywisw6bsheh7wJZcq3Q';
                            var map = new mapboxgl.Map({
                              container: 'map',
                              style: 'mapbox://styles/mapbox/streets-v11',
                              center: [109.76810754, -6.92242416], //lng,lat 10.818746, 106.629179
                              zoom: 9
                            });
                            var test ='<?php echo $dataArray;?>';  //ta nhận dữ liệu từ Controller
                            var dataMap = JSON.parse(test); //chuyển đổi nó về dạng mà Mapbox yêu cầu

                            // ta tạo dòng lặp để for ra các đối tượng
                            dataMap.features.forEach(function(marker) {

                                //tạo thẻ div có class là market, để hồi chỉnh css cho market
                                var el = document.createElement('div');
                                el.className = 'marker';

                                //gắn marker đó tại vị trí tọa độ
                                new mapboxgl.Marker(el)
                                    .setLngLat(marker.geometry.coordinates)
                                    .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
                                .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
                                    .addTo(map);
                            });

                        </script>     
                    </div>
                </div>
                <a class="btn btn-success mt-3" href="<?php echo e(url('gis/edit/'.$gis['id'])); ?>">Edit</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-warning mt-3" href="<?php echo e(route('gis')); ?>">Kembali</a>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/gis/detail.blade.php ENDPATH**/ ?>