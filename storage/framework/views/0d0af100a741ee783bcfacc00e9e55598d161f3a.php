
  
<?php $__env->startSection('content'); ?>

<div class="container mt-5">
    <div class="row justify-content-center align-items-center">
            <div class="col-md-12">
                <h2><?php echo e($pages->judul); ?></h2>
                <p><?php echo $pages->content; ?></p>
            </div>
            <a class="btn btn-success mt-3" href="<?php echo e(url('pages/edit/'.$pages->id)); ?>">Edit</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-warning mt-3" href="<?php echo e(route('pages')); ?>">Kembali</a>

        </div>
    </div>
</div>
<script src="<?php echo e(asset( 'assets/js/jquery-1.9.1.js')); ?>"></script>
<script type="text/javascript">
  $(document).ready(function () {   
    var base_url = <?php echo json_encode(url('/')); ?>;
    var src = $('img').attr('src');
    $('img').removeAttr('src');
    $('img').attr('src',base_url+src);
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/pages/detail.blade.php ENDPATH**/ ?>