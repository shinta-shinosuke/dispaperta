

<?php $__env->startSection('content'); ?>

 <main id="main">
    <section class="breadcrumbs">
      <div class="container">

        <div class="justify-content-between align-items-center">
          <h2><?php echo e($articles['judul']); ?></h2>
          <ol>
            <li><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo e($articles['day_created']); ?>, <?php echo e($articles['created_at']); ?></li>
            <li><i class="fa heart-o"></i>&nbsp;&nbsp;<?php echo e($articles['viewer']); ?></li>
            <li><i class="fa fa-user"></i>&nbsp;&nbsp;<?php echo e($articles['posting_by']); ?></li>
          </ol>
        </div>

      </div>
    </section>

    <section class="inner-page">
      <div class="container">
        <p style="color:#FFF">
          <?php echo $articles['content']; ?>

        </p>
      </div>
    </section>

  </main><!-- End #main -->
  <script src="<?php echo e(asset( 'assets/js/jquery-1.9.1.js')); ?>"></script>
  <script type="text/javascript">
    $(document).ready(function () {   
      var base_url = <?php echo json_encode(url('/')); ?>;
      var src = $('img').attr('src');
      $('img').removeAttr('src');
      $('img').attr('src',base_url+src);
      });
  </script>
  <?php $__env->stopSection(); ?>
<?php echo $__env->make('main.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/main/baca.blade.php ENDPATH**/ ?>