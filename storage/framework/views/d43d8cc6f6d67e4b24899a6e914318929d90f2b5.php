
  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data Penyuluh
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('penyuluh.store')); ?>" id="myForm">
            <?php echo csrf_field(); ?>
                <div class="form-group">
                    <label for="name">NIP</label>                    
                    <input type="text" name="nip" class="form-control" id="nip" aria-describedby="nip" required="required">                
                </div>

                <div class="form-group">
                    <label for="name">Nama Penyuluh</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" required="required">                
                </div>

                <div class="form-group">
                    <label for="kecamatan"><?php echo e(__('Kecamatan')); ?></label>
                        <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                            <option value="">-- Pilih Kecamatan --</option>
                            <?php $__currentLoopData = $kecamatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kecamatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($kecamatan['kode_cepat']); ?>"><?php echo e($kecamatan['nama']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>    
                </div>

                <div class="form-group">
                    <label for="name">Nama Unor</label>                    
                    <input type="text" name="nama_unor" class="form-control" id="nama_unor" aria-describedby="nama_unor" required="required">                
                </div>

                <div class="form-group">
                    <label for="name">Jabatan</label>                    
                    <input type="text" name="jabatan" class="form-control" id="jabatan" aria-describedby="jabatan" required="required">                
                </div>

                <div class="form-group">
                    <label for="name">Kelas Jabatan</label>                    
                    <input type="text" name="kelas_jabatan" maxxlength="2" class="form-control" id="kelas_jabatan" aria-describedby="kelas_jabatan" required="required">                
                </div>

                <div class="form-group">
                    <label for="name">Unor Induk ID</label>                    
                    <input type="text" name="unor_induk_id" class="form-control" id="unor_induk_id" aria-describedby="unor_induk_id" required="required">                
                </div>

                <div class="form-group">
                    <label for="name">Jabatan Fungsional ID</label>                    
                    <input type="text" name="jabatan_fungsional_id" class="form-control" id="jabatan_fungsional_id" aria-describedby="nip" required="required">                
                </div>
                
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="<?php echo e(route('penyuluh')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/penyuluh/create.blade.php ENDPATH**/ ?>