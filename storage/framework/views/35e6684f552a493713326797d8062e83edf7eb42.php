
  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data User
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('users.update', $user->id)); ?>" id="myForm">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>                    
                    <input type="text" name="fullname" class="form-control" id="fullname" value="<?php echo e($user->nama_lengkap); ?>"aria-describedby="fullname" >                
                </div>
                <div class="form-group">
                    <label for="name">username</label>                    
                    <input type="text" name="name" class="form-control" id="name" value="<?php echo e($user->name); ?>" aria-describedby="name" >                
                </div>
                <div class="form-group">
                    <label for="writer">Password</label>                    
                    <input type="password" name="password" class="form-control" id="password" value="" placeholder="Kosongkan jika tidak ingin mengubah password" aria-describedby="password" >                
                </div>
                <div class="form-group">
                    <label for="email">Email</label>                    
                    <input type="text" name="email" class="form-control" id="email" value="<?php echo e($user->email); ?>" aria-describedby="email" >                
                </div>
                <div class="form-group">
                    <label for="name">No. Telephone</label>                    
                    <input type="text" name="phone" class="form-control" id="phone" value="<?php echo e($user->phone); ?>" aria-describedby="phone" >                
                </div>
                <div class="form-group">
                    <label for="name">Level</label>                   
                    <select name="level" class="form-control" id="level" aria-describedby="level">
                        <option value="Admin" <?php echo e($user->level == 'Admin' ? 'selected' : ''); ?>>Admin</option>
                        <option value="Admin Bidang" <?php echo e($user->level == 'Admin Bidang' ? 'selected' : ''); ?>>Admin Bidang</option>
                    </select>    
                </div>
                <div class="form-group kecamatan">
                    <label for="name">Kecamatan</label>                   
                    <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan">
                        <option value="">-- Pilih Kecamatan --</option>
                        <?php $__currentLoopData = $kecamatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kecamatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php echo e($user->id_kecamatan == $kecamatan['id_kecamatan'] ? 'selected' : ''); ?> value=<?php echo e($kecamatan['id_kecamatan']); ?>><?php echo e($kecamatan['kecamatan']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>    
                </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="<?php echo e(route('users')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/users/edit.blade.php ENDPATH**/ ?>