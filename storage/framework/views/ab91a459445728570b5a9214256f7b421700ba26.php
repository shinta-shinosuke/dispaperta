
  
<?php $__env->startSection('content'); ?>

<div class="container mt-5">
    <div class="row justify-content-center align-items-center">
            <div class="col-md-12">
                <h2><?php echo e($pages->judul); ?></h2>
                <p><?php echo $pages->content; ?></p>
            </div>
            <a class="btn btn-success mt-3" href="<?php echo e(url('articles/edit/'.$pages->id)); ?>">Edit</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-warning mt-3" href="<?php echo e(route('articles')); ?>">Kembali</a>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/articles/detail.blade.php ENDPATH**/ ?>