

<?php $__env->startSection('content'); ?>
<!-- ======= Specials Section ======= -->
    <section id="bidang" class="specials" style="margin-top:100px;">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Bidang</h2>
              <p>PSKP</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-3">
            <ul class="nav nav-tabs flex-column">
              <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#tab-1">Struktur</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-2">Tugas Pokok</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#tab-3">Fungsi</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-9 mt-4 mt-lg-0">
            <div class="tab-content">
              <div class="tab-pane active" id="tab-1">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                    <div id="hdbidang"> 
                                  <div class="row">
                                        <center> <span class="col-md-4 btn-info bdt" style="color: #FFF;">Kepala Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian</span></center>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12" style="margin-top: 35px">
                                  </div>
                                </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-2">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                    <p class="fst-italic">Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian mempunyai tugas melaksanakan sebagian tugas Kepala Dinas dalam menyusun, menyiapkan, melaksanakan, mengkoordinasikan, memfasilitasi, mengatur, memantau dan mengevaluasi serta melaporkan kegiatan dibidang pengembangan sumberdaya manusia dan kelembagaan pertanian.</p>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab-3">
                <div class="row">
                  <div class="col-lg-12 details order-2 order-lg-1">
                    <div align="justify">
                    <ol>
                  <li>Penyusunan Kebijakan teknis, perencanaan dan program kerja pada Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian ;</li>
                  <li>Penyelenggaraan upaya peningkatan pelayanan publik di bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian;</li>
                  <li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan kelembagaan pertanian;</li>
                  <li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksanaan kegiatan sumber daya manusia penyuluhan;</li>
                  <li>Pelaksanaan perencanaan, pembinaan, pengembangan, pemberdayaan, pemantauan dan pengendalian pelaksannaan kegiatan penyelenggaraan penyuluhan;</li>
                  <li>Pelaksanaan&nbsp; monitoring, evaluasi dan laporan pelaksanaan tugas pada Bidang Pengembangan Sumber Daya Manusia dan Kelembagaan Pertanian; dan</li>
                  <li>Pelaksanaan tugas kedinasan lain yang diberikan oleh atasan.<br>
                  </li>
                </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Specials Section -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/main/pskp.blade.php ENDPATH**/ ?>