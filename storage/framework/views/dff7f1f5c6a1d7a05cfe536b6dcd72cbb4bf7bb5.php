
  
<?php $__env->startSection('content'); ?>

<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Kios
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
                <form method="post" action="<?php echo e(route('kios.update', $kios['id_kios'])); ?>" id="myForm">
                <?php echo csrf_field(); ?>
                <?php echo method_field('PUT'); ?>
                    <div class="form-group">
                        <label for="name">Nama Kios</label>                    
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo e($kios['nama_kios']); ?>" aria-describedby="name" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="name">Kode Kios</label>                    
                        <input type="text" name="kode" class="form-control" id="kode" value="<?php echo e($kios['kode_kios']); ?>" aria-describedby="kode" required="required">                
                    </div>

                    <div class="form-group">
                        <label for="kecamatan"><?php echo e(__('Kecamatan')); ?></label>
                            <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                                <option value="">-- Pilih Kecamatan --</option>
                                <?php $__currentLoopData = $kecamatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kecamatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option <?php echo e($kios['kode_kecamatan'] == $kecamatan['kode_cepat'] ? 'selected' : ''); ?> value=<?php echo e($kecamatan['kode_cepat']); ?>><?php echo e($kecamatan['nama']); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>    
                    </div>
                    
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="<?php echo e(route('kios')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/kios/edit.blade.php ENDPATH**/ ?>