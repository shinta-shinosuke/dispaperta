<html xmlns="http://www.w3.org/1999/xhtml">
  <link type="text/css" rel="stylesheet" id="dark-mode-custom-link">
  <link type="text/css" rel="stylesheet" id="dark-mode-general-link">
  <style lang="en" type="text/css" id="dark-mode-custom-style"></style>
  <style lang="en" type="text/css" id="dark-mode-native-style"></style>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Laporan Penyuluh</title>
    <link href="<?php echo e(url('/images/icon.png')); ?>" rel="icon">
    <link href="config/style2.css" rel="stylesheet" type="text/css">
  </head>
  <body bgcolor="#999999">
    <table width="90%" bgcolor="#FFFFFF" align="center">
      <tbody>
        <tr>
          <td>
            <br><br>

            <div align="center">
              <strong>
                DAFTAR KELEMBAGAAN PELAKU UTAMA DAN PELAKU USAHA<br>
                      BIDANG PERTANIAN, PERIKANAN DAN KEHUTANAN KABUPATEN BATANG
              </strong>
              <?php
                $url= '';
                $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                $uri_segments = explode('/', $uri_path);
                
               $url = $uri_segments[4];
               
               switch ($url) {
                  case 'all':
                    $cat = 'Semua Data';
                    break;
                  case 'pelaku_utama':
                    $cat = 'Pelaku Utama';
                    break;
                  case 'pelaku_usaha':
                    $cat = 'Pelaku Usaha';
                    break;
                  case 'jenis_pelaku':
                    $cat = 'Jenis Pelaku';
                    break;
                  case 'kecamatan_desa':
                    $cat = 'Kec. '.$_POST['kecamatan'].' Desa '.$_POST['desa'];
                    break;
                  default:
                    $cat = ucfirst($url);
                }
              ?>
              <br>Berdasarkan <?php echo e($cat); ?>

            </div>
    
	          <table width="85%" border="1" style="border-collapse:collapse" cellpadding="5" align="center">
              <tbody>
                <tr class="hitam">
                  <td width="3%" rowspan="3">
                    <div align="center"><strong>No</strong></div>
                  </td>
                  <td width="11%" rowspan="3">
                    <div align="center">
                      <strong>Kecamatan<br>Desa</strong>
                    </div>
                  </td>
                  <td colspan="3">
                    <div align="center">
                      <strong>Kelembagaan</strong>
                    </div>
                    <div align="center"></div>
                    <div align="center"></div>
                  </td>
                  <td width="7%" rowspan="3">
                    <div align="center">
                      <strong>Jml Agt<br>(org/ Kel)</strong>
                    </div>      
                    <div align="center"></div>
                  </td>
                  <td width="4%" rowspan="3">
                    <div align="center">
                      <strong>Luas Areal</strong>
                    </div>      
                    <div align="center"></div>
                  </td>
                  <td colspan="3">
                    <div align="center">
                      <strong>Nama Pengurus</strong>
                    </div>      
                    <div align="center"></div>      
                    <div align="center"></div>
                  </td>
                  <td colspan="9">
                    <div align="center">
                      <strong>Jenis Usaha</strong>
                    </div>      
                    <div align="center"></div>      
                    <div align="center"></div>      
                    <div align="center"></div>      
                    <div align="center"></div>      
                    <div align="center"></div>      
                    <div align="center"></div>      
                    <div align="center"></div>      
                    <div align="center"></div>
                  </td>
                  <td width="4%" rowspan="3">
                    <div align="center">Thn</div>      
                    <div align="center"></div>
                  </td>
                </tr>
                <tr class="hitam">
                  <td width="10%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>No. Induk <br>Nama</strong>
                    </div>
                  </td>
                  <td colspan="2" class="hitam">
                    <div align="center">
                      <strong>Pelaku</strong>
                    </div>      
                    <div align="center"></div>
                  </td>
                  <td width="12%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>Ketua</strong>
                    </div>
                  </td>
                  <td width="10%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>Sekretaris</strong>
                    </div>
                  </td>
                  <td width="11%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>Bendahara</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>1</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>2</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>3</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>4</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>5</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>6</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>7</strong>
                    </div>
                  </td>
                  <td width="2%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>8</strong>
                    </div>
                  </td>
                  <td width="3%" rowspan="2" class="hitam">
                    <div align="center">
                      <strong>9</strong>
                    </div>
                  </td>
                </tr>
                <tr class="hitam">
                  <td width="5%">
                    <div align="center">
                      <strong>Utama</strong>
                    </div>
                  </td>
                  <td width="4%">
                    <div align="center">
                      <strong>Usaha</strong>
                    </div>
                  </td>
                </tr>
                <tr bgcolor="#f0f0f0">
                  <td rowspan="2">
                    1
                  </td>
                  <td align="center">
                    Kec. Warungasem<br>BANJIRAN
                  </td>
                  <td align="center">
                    14.11.01<br>SUBUR JAYA
                  </td>
                  <td>
                    POKTAN
                  </td>
                  <td></td>
                  <td>
                    14 Org
                  </td>
                  <td>
                    16 Ha
                  </td>
                  <td>
                    MUSBIKHIN
                  </td>
                  <td>
                    MAHMUDIN
                  </td>
                  <td>
                    SITI DARTINAH
                  </td>
                  <td>
                    V    	
                  </td>
                  <td>
        		        -		
                  </td>
                  <td>
        		        V		
                  </td>
                  <td>
              			-		
                  </td>
                  <td>
              			-		
                  </td>
                  <td>
              			-	
                  </td>
                  <td>
              			-		
                  </td>
                  <td>
              			-		
                  </td>
                  <td>
              			-		
                  </td>
                  <td>
                    1978
                  </td>
                </tr>
                <tr bgcolor="#f0f0f0">
                  <td colspan="19">
                    Kelas Kelompok : Lanjut, HP :   087578754323, Email : 
                  </td>
                </tr>
                <tr bgcolor="#BEBEBE">
                  <td colspan="20">
        		        <strong>1</strong>&nbsp;
                    <a href="https://dispaperta.batangkab.go.id/gapoktan/btampilkandata/10" data-ci-pagination-page="2">2  </a>
                    <a href="https://dispaperta.batangkab.go.id/gapoktan/btampilkandata/20" data-ci-pagination-page="3">3  </a>
                    <a href="https://dispaperta.batangkab.go.id/gapoktan/btampilkandata/30" data-ci-pagination-page="4">4  </a>
                    <a href="https://dispaperta.batangkab.go.id/gapoktan/btampilkandata/40" data-ci-pagination-page="5">5  </a>
                    <a href="https://dispaperta.batangkab.go.id/gapoktan/btampilkandata/10" data-ci-pagination-page="2" rel="next">&gt;  </a>
                    <a href="https://dispaperta.batangkab.go.id/gapoktan/btampilkandata/110" data-ci-pagination-page="12">Last ›  </a>
                  </td>
                </tr>
                <tr>
                  <td colspan="20">
              	   Ket. Jenis Usaha : <br>1=Tanaman Pangan, 	2=Peternakan, 3=Hortikultura, 4=Perkebunan, 5=Kehutanan, 6=Budidaya Ikan, 7=Perikanan Tangkap, 8=Pengolahan, 9=Lainnya
                  </td>
                </tr>
              </tbody>
            </table>

          </td>
        </tr> 
      </tbody>
    </table>
  </body>
</html><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/gapoktan/showdata.blade.php ENDPATH**/ ?>