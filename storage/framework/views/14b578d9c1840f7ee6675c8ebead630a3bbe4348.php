
 
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left mt-2">
                <h2>Manajemen Users</h2>
            </div>
            <div class="float-right my-2">
                <a class="btn btn-success" href="<?php echo e(route('users.create')); ?>"> Tambah Data User</a>
            </div>
        </div>
    </div>
   
    <?php if($message = Session::get('success')): ?>
        <div class="alert alert-success">
            <p><?php echo e($message); ?></p>
        </div>
    <?php endif; ?>
   <div class="container">
    <table class="table table-bordered data-table">
      <thead>
        <tr>
            <th>No</th>
            <th>Nama Lengkap</th>
            <th>username</th>
            <th>Email</th>
            <th>Level</th>
            <th>Kecamatan</th>
            <th width="280px">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
   </div>
<?php $__env->stopSection(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function () {   
    $('.data-table').DataTable({
        "autoWidth": false,
        processing: true,
        serverSide: true,
        ajax: "<?php echo e(route('users')); ?>",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nama_lengkap', name: 'nama_lengkap'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'level', name: 'level'},
            {data: 'kecamatan', name: 'kecamatan', searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/users/index.blade.php ENDPATH**/ ?>