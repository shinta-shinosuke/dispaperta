
  
<?php $__env->startSection('content'); ?>

<?php
foreach($kecamatan as $kecamatan){


}
$no = $kecamatan['kode']+1;
?>   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data Kecamatan
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('kecamatan.store')); ?>" id="myForm">
            <?php echo csrf_field(); ?>
                <div class="form-group">
                    <input type="hidden" name="kode" class="form-control" id="kode" aria-describedby="kode" value="<?php echo e($no); ?>">
                    <label for="name">Nama Kecamatan</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" required="required">                
                </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="<?php echo e(route('kecamatan')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/kecamatan/create.blade.php ENDPATH**/ ?>