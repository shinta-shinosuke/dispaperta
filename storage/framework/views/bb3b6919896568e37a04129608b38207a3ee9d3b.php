  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Download
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('download.update', $download->id)); ?>" id="myForm" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <?php echo method_field('PUT'); ?>
                <div class="form-group">
                    <label for="name">Tentang</label>                    
                    <input type="tentang" name="tentang" class="form-control" id="tentang" value="<?php echo e($download->tentang); ?>" aria-describedby="tentang">                
                </div>
                <div class="form-group">
                    <label for="name">Pilih File</label> 
                    <input type="file" class="form-control" name="file">
                    <span><?php echo e($download->nama_file); ?></span>
                </div>
            <button type="submit" class="btn btn-primary" value="Upload">Kirim</button>
            <a href="<?php echo e(route('download')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/download/edit.blade.php ENDPATH**/ ?>