
  
<?php $__env->startSection('content'); ?>

<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Alsintan
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
                <form method="post" action="<?php echo e(route('alsintan.update', $alsintan['id_alsintan'])); ?>" id="myForm">
                <?php echo csrf_field(); ?>
                <?php echo method_field('PUT'); ?>
                    <div class="form-group">
                        <label for="name">Nama Alsintan</label>                    
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo e($alsintan['nama_alsintan']); ?>" aria-describedby="name" required="required">                
                    </div>
                    <div class="form-group kecamatan">
                        <label for="name">Jenis Alsintan</label>                   
                        <select name="jenis_alsintan" class="form-control" id="jenis_alsintan" aria-describedby="jenis_alsintan" required="required">
                            <option value="">-- Pilih Jenis Alsintan --</option>
                             <?php $__currentLoopData = $jenis_alsintan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                             <option <?php echo e($alsintan['jenis_alsintan_id'] == $jenis->id ? 'selected' : ''); ?> value="<?php echo e($jenis->id); ?>"><?php echo e($jenis->jenis_alsintan); ?></option>

                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>    
                    </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="<?php echo e(route('alsintan')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/alsintan/edit.blade.php ENDPATH**/ ?>