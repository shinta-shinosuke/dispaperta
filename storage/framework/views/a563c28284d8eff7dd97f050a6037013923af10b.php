
  
<?php $__env->startSection('content'); ?>

<div class="container mt-5">
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Detail User
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>Name: </b><?php echo e($user->name); ?></li>
                    <li class="list-group-item"><b>Email: </b><?php echo e($user->email); ?></li>
                </ul>
            </div>
            <a class="btn btn-success mt-3" href="<?php echo e(route('users')); ?>">Kembali</a>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('users.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/users/detail.blade.php ENDPATH**/ ?>