<?php $__env->startSection('content'); ?>
<main id="main">
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2><?php echo e(__('Dashboard')); ?></h2>
          <ol>
            <li><a href="<?php echo e(url('/main/home)); ?>">Home</a></li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo e(url('/download')); ?>"><?php echo e(__('Download')); ?></a>
            </li>
          </ol>
        </div>

      </div>
    </section>

    <section class="inner-page">
      <div class="container">
        <p>Hi, <?php echo e(Auth::user()->name); ?></p>
        <?php echo e(__('You are logged in!')); ?>

      </div>
    </section>
</main><!-- End #main -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> ?><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/main/home.blade.php ENDPATH**/ ?>