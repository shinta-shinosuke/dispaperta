
  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Tambah Data User
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('users.store')); ?>" id="myForm">
            <?php echo csrf_field(); ?>
                 <div class="form-group">
                    <label for="name">Nama Lengkap</label>                    
                    <input type="text" name="fullname" class="form-control" id="fullname" aria-describedby="fullname" >                
                </div>
                <div class="form-group">
                    <label for="name">Username</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" >                
                </div>
                <div class="form-group">
                    <label for="password">Password</label>                    
                    <input type="password" name="password" class="form-control" id="password" aria-describedby="password" >                
                </div>
               
                <div class="form-group">
                    <label for="email">Email</label>                    
                    <input type="email" name="email" class="form-control" id="email" aria-describedby="email" >                
                </div>
                <div class="form-group">
                    <label for="name">No. Telephone</label>                    
                    <input type="text" name="phone" class="form-control" id="phone" aria-describedby="phone" >                
                </div>
                <div class="form-group">
                    <label for="name">Level</label>                   
                    <select name="level" class="form-control" id="level" aria-describedby="level">
                        <option value="Admin">Admin</option>
                        <option value="Admin Bidang">Admin Bidang</option>
                    </select>    
                </div>
                <div class="form-group kecamatan">
                    <label for="name">Kecamatan</label>                   
                    <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan">
                        <option value="">-- Pilih Kecamatan --</option>
                        <?php $__currentLoopData = $kecamatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kecamatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($kecamatan['kode_cepat']); ?>"><?php echo e($kecamatan['nama']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>    
                </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="<?php echo e(route('users')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<script type="text/javascript">
    $(document).on('change', '#level', function() {
        var level = $(this).val();

        if(level != 'Admin'){
            $('.kecamatan').hide();
        }else{
            $('.kecamatan').somehow();
        }
    });
</script>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/users/create.blade.php ENDPATH**/ ?>