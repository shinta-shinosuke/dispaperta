

<?php $__env->startSection('content'); ?>
<!-- ======= Download Section ======= -->
    <section id="download" style="margin-top:100px;" class="events">
      <div class="container" data-aos="fade-up" >
    	<div class="row">
          <div class="col-lg-12">
	        <div class="section-title">
	          <h2>Download</h2>
	          <p>Download File</p>
	        </div>
      	  </div>
      	</div>
      </div>
      <div class="container" data-aos="fade-up">
	      <div class="row">
	          <div class="col-lg-12">
	            <table class="table table-bordered data dataTable no-footer" id="tb-download">
	              <thead>
	                <tr>
	                    <th>No</th>
	                    <th>Tentang</th>
	                    <th>extensi</th>
	                    <th>Nama File</th>
	                    <th>Download</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	            </table>
	          </div>
	       </div>
	  </div>
    </section><!-- End Download Section -->
<?php $__env->stopSection(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {   
    $('#tb-download').DataTable({
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo e(route('main.download')); ?>",
        "deferRender": true,
        "columns": [
            {data: 'id', name: 'id'},
            {data: 'tentang', name: 'tentang'},
            {data: 'extensi', name: 'extensi'},
            {data: 'nama_file', name: 'nama_file'},
            {data: 'download', name: 'download'},
        ]
    });
   });
</script>
<?php echo $__env->make('main.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/main/download.blade.php ENDPATH**/ ?>