
  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 50rem;">
            <div class="card-header">
            Tambah Data Artikel
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('articles.store')); ?>" enctype="multipart/form-data" id="myForm">
            <?php echo csrf_field(); ?>
                <div class="form-group">
                    <label for="name">Judul</label>                    
                    <input type="text" name="judul" class="form-control" id="judul" aria-describedby="judul" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Url</label>                    
                    <input type="text" name="slug" class="form-control" id="slug" aria-describedby="slug" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Content</label>
                    <textarea id="create-article-summernote" name="content"></textarea>
                </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="<?php echo e(route('articles')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
    <script src="<?php echo e(asset('assets/js/jquery-3.5.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/bootstrap5.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/summernote.min.js')); ?>"></script>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#create-article-summernote').summernote({
                    height: 450,
                });
            });

        </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/articles/create.blade.php ENDPATH**/ ?>