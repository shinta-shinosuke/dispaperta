
  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 1024rem;">
            <div class="card-header">
            Edit Data Gapoktan / Poktan
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php $__currentLoopData = $gapoktan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <form method="post" action="<?php echo e(route('gapoktan.update', $g['id'])); ?>" id="myForm">
                <?php echo csrf_field(); ?>
                <?php echo method_field('PUT'); ?>
                    <div class="form-group">
                    <label for="name">Tanggal</label>                    
                    <label for="name"><?php echo e(date('d-m-Y H:i:s')); ?></label>         
                </div>
                <input type="hidden" name="id" class="form-control" id="id" value="<?php echo e($g['id']); ?>" required="required">   
                <input type="hidden" name="filename" class="form-control" id="filename" value="<?php echo e($g['nama_file']); ?>" required="required">   
                <input type="hidden" name="extensi" class="form-control" id="extensi" value="<?php echo e($g['extensi']); ?>" required="required">   
                <div class="form-group">
                    <label for="kecamatan"><?php echo e(__('Kecamatan')); ?></label>
                        <select name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" required="required">
                            <option value="">-- Pilih Kecamatan --</option>
                            <?php $__currentLoopData = $kecamatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kecamatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php echo e($g['id_kecamatan'] == $kecamatan['id_kecamatan'] ? 'selected' : ''); ?> value=<?php echo e($kecamatan['id_kecamatan']); ?>><?php echo e($kecamatan['kecamatan']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>    
                                
                        <?php $__errorArgs = ['kecamatan'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>

                <div class="form-group">
                    <label for="kecamatan"><?php echo e(__('Desa')); ?></label>
                        <select name="desa" class="form-control" id="desa" aria-describedby="desa" required="required">
                            <option value="">-- Pilih Desa --</option>
                            <?php $__currentLoopData = $desa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $desa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php echo e($g['id_desa'] == $desa['id_desa'] ? 'selected' : ''); ?> value=<?php echo e($desa['id_desa']); ?>><?php echo e($desa['desa']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>    
                                
                        <?php $__errorArgs = ['desa'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label for="name">No Induk</label>                    
                      <input type="text" name="no_induk" class="form-control" id="no_induk" aria-describedby="no_induk" value="<?php echo e($g['no_induk']); ?>" required="required">   
                    </div>
                    
                    <div class="col-md-6">
                      <label for="name">Nama</label>                       
                      <input type="text" name="nama" class="form-control" id="nama" aria-describedby="nama" value="<?php echo e($g['nama']); ?>" required="required">  
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                                 
                </div>
                
                <div class="form-group">
                    <label for="name">Jenis Kelembagaan</label>  
                    <div class="row">
                        <div class="input-group col-md-6">
                            <span class="input-group-addon">
                              <input type="radio" name="pelaku" value="Pelaku Utama" <?php echo e($g['jenis_kelembagaan'] == 'Pelaku Utama' ? 'checked' : ''); ?> >  
                            </span>
                            <label class="form-control">Pelaku Utama</label>
                        </div>
                        <div class="input-group col-md-6">
                            <span class="input-group-addon">
                              <input type="radio" name="pelaku" value="Pelaku Usaha" <?php echo e($g['jenis_kelembagaan'] == 'Pelaku Usaha' ? 'checked' : ''); ?> >  
                            </span>
                            <label class="form-control">Pelaku Usaha</label>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="col-md-6">
                            <label for="utama"><?php echo e(__('Utama')); ?></label>
                            <select name="utama" class="form-control" id="utama" aria-describedby="utama">
                                <option value="" <?php echo e($g['utama'] == '' ? 'selected' : ''); ?> >-----</option>
                                <option value="GAPOKTAN" <?php echo e($g['utama'] == 'GAPOKTAN' ? 'selected' : ''); ?> >GAPOKTAN</option>
                                <option value="POKTAN" <?php echo e($g['utama'] == 'POKTAN' ? 'selected' : ''); ?> >POKTAN</option>
                                <option value="POKDAKAN" <?php echo e($g['utama'] == 'POKDAKAN' ? 'selected' : ''); ?> >POKDAKAN</option>
                                <option value="KEL. TANI IKAN" <?php echo e($g['utama'] == 'KEL. TANI IKAN' ? 'selected' : ''); ?> >KEL. TANI IKAN</option>
                                <option value="PEMUDA TANI" <?php echo e($g['utama'] == 'PEMUDA TANI' ? 'selected' : ''); ?> >PEMUDA TANI</option>
                                <option value="KWT" <?php echo e($g['utama'] == 'KWT' ? 'selected' : ''); ?> >KWT</option>
                                <option value="KTHR" <?php echo e($g['utama'] == 'KTHR' ? 'selected' : ''); ?> >KTHR</option>
                                <option value="LMDH" <?php echo e($g['utama'] == 'LMDH' ? 'selected' : ''); ?> >LMDH</option>
                                <option value="POKNAK" <?php echo e($g['utama'] == 'POKNAK' ? 'selected' : ''); ?> >POKNAK</option>
                                <option value="FMA" <?php echo e($g['utama'] == 'FMA' ? 'selected' : ''); ?> >FMA</option>
                                <option value="POKHUT" <?php echo e($g['utama'] == 'POKHUT' ? 'selected' : ''); ?> >POKHUT</option>
                                <option value="UPG" <?php echo e($g['utama'] == 'UPG' ? 'selected' : ''); ?> >UPG</option>
                                <option value="KUB" <?php echo e($g['utama'] == 'KUB' ? 'selected' : ''); ?> >KUB</option>
                                <option value="KTP" <?php echo e($g['utama'] == 'KTP' ? 'selected' : ''); ?> >KTP</option>
                                <option value="P3A" <?php echo e($g['utama'] == 'P3A' ? 'selected' : ''); ?> >P3A</option>
                                <option value="POKBUN" <?php echo e($g['utama'] == 'POKBUN' ? 'selected' : ''); ?> >POKBUN</option>
                                <option value="AFINITAS / MAPAN" <?php echo e($g['utama'] == 'AFINITAS / MAPAN' ? 'selected' : ''); ?> >AFINITAS / MAPAN</option>
                                <option value="LMPA" <?php echo e($g['utama'] == 'LMPA' ? 'selected' : ''); ?> >LMPA</option>
                                <option value="LKMA" <?php echo e($g['utama'] == 'LKMA' ? 'selected' : ''); ?> >LKMA</option>
                                <option value="PAGUYUBAN" <?php echo e($g['utama'] == 'PAGUYUBAN' ? 'selected' : ''); ?> >PAGUYUBAN</option>
                            </select>    
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="col-md-6">
                            <label for="usaha"><?php echo e(__('Usaha')); ?></label>
                            <select name="usaha" class="form-control" id="usaha" aria-describedby="usaha">
                                <option value="" <?php echo e($g['usaha'] == '' ? 'selected' : ''); ?> >-----</option>
                                <option value="KWT" <?php echo e($g['usaha'] == 'KWT' ? 'selected' : ''); ?> >KWT</option>
                                <option value="KUB" <?php echo e($g['usaha'] == 'KUB' ? 'selected' : ''); ?> >KUB</option>
                                <option value="ASOSIASI" <?php echo e($g['usaha'] == 'ASOSIASI' ? 'selected' : ''); ?> >ASOSIASI</option>
                                <option value="P4K" <?php echo e($g['usaha'] == 'P4K' ? 'selected' : ''); ?> >P4K</option>
                                <option value="P4S" <?php echo e($g['usaha'] == 'P4S' ? 'selected' : ''); ?> >P4S</option>
                                <option value="UPG" <?php echo e($g['usaha'] == 'UPG' ? 'selected' : ''); ?> >UPG</option>
                                <option value="P3A" <?php echo e($g['usaha'] == 'P3A' ? 'selected' : ''); ?> >P3A</option>
                                <option value="KTP" <?php echo e($g['usaha'] == 'KTP' ? 'selected' : ''); ?> >KTP</option>
                                <option value="LMDH" <?php echo e($g['usaha'] == 'LMDH' ? 'selected' : ''); ?> >LMDH</option>
                                <option value="AFINITAS / MAPAN" <?php echo e($g['usaha'] == 'AFINITAS / MAPAN' ? 'selected' : ''); ?> >AFINITAS / MAPAN</option>
                                <option value="LKMA" <?php echo e($g['usaha'] == 'LKMA' ? 'selected' : ''); ?> >LKMA</option>
                                <option value="KEL. LEBAH" <?php echo e($g['usaha'] == 'KEL. LEBAH' ? 'selected' : ''); ?> >KEL. LEBAH</option>
                                <option value="PUAP" <?php echo e($g['usaha'] == 'PUAP' ? 'selected' : ''); ?> >PUAP</option>
                                <option value="KPK" <?php echo e($g['usaha'] == 'KPK' ? 'selected' : ''); ?> >KPK</option>
                            </select>    
                        </div>
                      </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="kelas kelompok"><?php echo e(__('Kelas Kelompok')); ?></label>
                        <select name="kelas_kelompok" class="form-control" id="kelas_kelompok" aria-describedby="kelas_kelompok" required="required">
                            <option value="" <?php echo e($g['kelas_kelompok'] == '' ? 'selected' : ''); ?> >-- Pilih Kelompok --</option>
                            <option value="Pemula" <?php echo e($g['kelas_kelompok'] == 'Pemula' ? 'selected' : ''); ?> >Pemula</option>
                            <option value="Lanjut" <?php echo e($g['kelas_kelompok'] == 'Lanjut' ? 'selected' : ''); ?> >Lanjut</option>
                            <option value="Madya" <?php echo e($g['kelas_kelompok'] == 'Madya' ? 'selected' : ''); ?> >Madya</option>
                            <option value="Utama" <?php echo e($g['kelas_kelompok'] == 'Utama' ? 'selected' : ''); ?> >Utama</option>
                        </select> 
                </div>
                
                <div class="form-group">
                    <label for="name">Jumlah Anggota</label>                    
                    <div class="row">
                      <div class="input-group col-md-3">
                        <input type="number" name="jumlah_anggota" class="form-control" id="jumlah_anggota" aria-describedby="jumlah_anggota" value="<?php echo e($g['jumlah_anggota']); ?>" required="required">
                      </div>
                        
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="anggota" value="Kelompok" <?php echo e($g['jenis_jumlah'] == 'Kelompok' ? 'checked' : ''); ?> >  
                            </span>
                            <label class="form-control">Kelompok</label>
                        </div>
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="anggota" value="Orang" <?php echo e($g['jenis_jumlah'] == 'Orang' ? 'checked' : ''); ?> >  
                            </span>
                            <label class="form-control">Orang</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Luas Areal</label>                    
                    <div class="row">
                      <div class="input-group col-md-3">
                        <input type="number" name="luas_areal" class="form-control" id="luas_areal" aria-describedby="luas_areal" value="<?php echo e($g['luas_areal']); ?>" required="required">
                      </div>
                        
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="luas" value="Ha"  <?php echo e($g['jenis_areal'] == 'Ha' ? 'checked' : ''); ?> >  
                            </span>
                            <label class="form-control">Ha</label>
                        </div>
                        <div class="input-group col-md-2">
                            <span class="input-group-addon">
                              <input type="radio" name="luas" value="Unit"  <?php echo e($g['jenis_areal'] == 'Unit' ? 'checked' : ''); ?> >  
                            </span>
                            <label class="form-control">Unit</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Ketua</label>                    
                    <input type="text" name="ketua" class="form-control" id="ketua" aria-describedby="ketua" value="<?php echo e($g['ketua']); ?>" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="name">Sekretaris</label>                    
                    <input type="text" name="sekretaris" class="form-control" id="sekretaris" aria-describedby="sekretaris" value="<?php echo e($g['sekretaris']); ?>" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="name">Bendahara</label>                    
                    <input type="text" name="bendahara" class="form-control" id="bendahara" aria-describedby="bendahara" value="<?php echo e($g['bendahara']); ?>" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="name">Tahun Pembentukan</label>             
                      <div class="input-group col-md-2" style="margin-left:-15px">
                        <input type="number" name="tahun_pembentukan" class="form-control" id="tahun_pembentukan" aria-describedby="tahun_pembentukan" value="<?php echo e($g['tahun_pembentukan']); ?>" required="required">
                      </div>
                </div>
                      
                <div class="form-group"> 
                    <div class="row">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Tanaman Pangan" <?php echo e($g['jenis_pembentukan'] == 'Tanaman Pangan' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Tanaman Pangan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Perkebunan" <?php echo e($g['jenis_pembentukan'] == 'Perkebunan' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Perkebunan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Prikanan Tangkap" <?php echo e($g['jenis_pembentukan'] == 'Prikanan Tangkap' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Prikanan Tangkap</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Peternakan" <?php echo e($g['jenis_pembentukan'] == 'Peternakan' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Peternakan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Kehutanan" <?php echo e($g['jenis_pembentukan'] == 'Kehutanan' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Kehutanan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Pengolahan Hasil" <?php echo e($g['jenis_pembentukan'] == 'Pengolahan Hasil' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Pengolahan Hasil</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Hortikultura" <?php echo e($g['jenis_pembentukan'] == 'Hortikultura' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Hortikultura</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Budidaya Ikan" <?php echo e($g['jenis_pembentukan'] == 'Budidaya Ikan' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Budidaya Ikan</label>
                        </div>
                        <div class="input-group col-md-3">
                            <span class="input-group-addon">
                              <input type="checkbox" name="jenis_pembentukan" value="Lainnya" <?php echo e($g['jenis_pembentukan'] == 'Lainnya' ? 'checked' : ''); ?>>  
                            </span>
                            <label class="form-control">Lainnya</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="name">HP</label>             
                      <div class="input-group">
                        <input type="text" name="hp" class="form-control" id="hp" aria-describedby="hp" value="<?php echo e($g['hp']); ?>" required="required">
                      </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Email</label>             
                      <div class="input-group">
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="email" value="<?php echo e($g['email']); ?>" required="required">
                      </div>
                </div>
                
                <div class="form-group">
                    <label for="name">Input Dokumen</label> 
                    <input type="file" class="form-control" name="file">
                </div>
                
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="<?php echo e(route('kecamatan')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function () { 
    var kecID = '';
    $('#kecamatan').change(function(){
    var kecID = $(this).val();    
    //alert(kecID);
    if(kecID){
        $.ajax({
           type:"GET",
           //url:"getdesa/"+kecID,
           url:"<?php echo e(url('/gapoktan/getdesa/')); ?>/"+kecID,
           dataType: 'JSON',
           success:function(res){               
            if(res){
                $("#desa").empty();
                $("#desa").append('<option>-- Pilih Desa --</option>');
                $.each(res,function(desa,id_desa){
                    $("#desa").append('<option value="'+id_desa+'">'+desa+'</option>');
                });
            }else{
               $("#desa").empty();
            }
           }
        });
    }else{
        $("#desa").empty();
    }      
   });
});
</script>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/gapoktan/edit.blade.php ENDPATH**/ ?>