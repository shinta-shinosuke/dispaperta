
  
<?php $__env->startSection('content'); ?>   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 34rem;">
            <div class="card-header">
            Tambah Data POKTAN
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('poktan.store')); ?>" id="myForm">
            <?php echo csrf_field(); ?>
                <div class="form-group">
                    <label for="name">Nama POKTAN</label>                    
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" required="required">                
                </div>
                
                <div class="form-group">
                    <label for="kecamatan"><?php echo e(__('GAPOKTAN')); ?></label>
                        <input type="hidden" name="gapoktan_nama" class="form-control" id="gapoktan_nama">
                        <select name="gapoktan" class="form-control" id="gapoktan" aria-describedby="gapoktan" required="required">
                            <option value="">-- Pilih GAPOKTAN --</option>
                            <?php $__currentLoopData = $gapoktan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gapoktan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($gapoktan->id); ?>"><?php echo e($gapoktan->nama_gapoktan); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>    
                </div>

                <div class="form-group">
                    <label for="name">Kecamatan</label>                    
                    <input type="text" name="kecamatan" class="form-control" id="kecamatan" aria-describedby="kecamatan" disabled>                
                </div>

                <div class="form-group">
                    <label for="name">Desa</label>                    
                    <input type="hidden" name="kode_desa" class="form-control" id="kode_desa" aria-describedby="kode_desa">                
                    <input type="text" name="desa" class="form-control" id="desa" aria-describedby="desa" disabled>                
                </div>

                <div class="form-group">
                    <label for="name">Penyuluh</label>                    
                    <input type="text" name="penyuluh" class="form-control" id="penyuluh" aria-describedby="penyuluh" disabled>                
                </div>

            <button type="submit" class="btn btn-primary">Kirim</button>
            <a href="<?php echo e(route('desa')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
    </div>
    <script src="<?php echo e(asset( 'assets/js/jquery-1.9.1.js')); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
            $.ajaxSetup({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
             });
     $('#gapoktan').change(function(){
        var id = $(this).val();

          $.ajax({
              url: "<?php echo e(route('poktan.check')); ?>",
              type: 'post',
              data: { id : id},
              success: function(response)
              {
                console.log(response);
                $('input[name="gapoktan_nama"]').val(response['gapoktan_nama']);
                $('input[name="kecamatan"]').val(response['kecamatan_nama']);
                $('input[name="desa"]').val(response['desa_nama']);
                $('input[name="kode_desa"]').val(response['desa_kode']);
                $('input[name="penyuluh"]').val(response['penyuluh_nama']);
              },
              error: function(jqXHR, textStatus, errorThrown) {
                    console.log("error");
                }
            });
     });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/poktan/create.blade.php ENDPATH**/ ?>