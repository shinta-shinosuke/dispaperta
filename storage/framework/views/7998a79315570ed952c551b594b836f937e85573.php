
  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 50rem;">
            <div class="card-header">
            Edit Data Halaman
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
            <form method="post" action="<?php echo e(route('pages.update', $pages['id'])); ?>" id="myForm">
                <?php echo csrf_field(); ?>
                <?php echo method_field('PUT'); ?>
                <div class="form-group">
                    <label for="name">Judul</label>                    
                    <input type="text" name="judul" class="form-control" id="judul" value="<?php echo e($pages->judul); ?>" aria-describedby="judul" required="required">                
                </div>
                 <div class="form-group">
                    <label for="name">Url</label>                    
                    <input type="text" name="slug" class="form-control" id="slug"value="<?php echo e($pages->slug); ?>" aria-describedby="slug" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Content</label>                   
                    <textarea id="summernote" name="content"><?php echo e($pages->content); ?></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="<?php echo e(route('pages')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
            </form>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#summernote').summernote({
                    height: 450,
                });
            });

        </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\dispaperta\resources\views/pages/edit.blade.php ENDPATH**/ ?>