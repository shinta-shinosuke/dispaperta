
  
<?php $__env->startSection('content'); ?>
   
<div class="container mt-5">
   
    <div class="row justify-content-center align-items-center">
        <div class="card" style="width: 24rem;">
            <div class="card-header">
            Edit Data Harga Pasar
            </div>
            <div class="card-body">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            <?php endif; ?>
                <form method="post" action="<?php echo e(route('harga.update', $harga->id)); ?>" id="myForm">
                <?php echo csrf_field(); ?>
                <?php echo method_field('PUT'); ?>
                <div class="form-group">
                    <label for="name">BLTH</label>                    
                    <input type="text" name="blth" class="form-control" id="blth" aria-describedby="blth" value="<?php echo e($harga->blth); ?>" laceholder="mmyyyy" maxlength="6" readonly="readonly">                
                </div>
                <div class="form-group">
                    <label for="name">Nama Sembako</label>
                    <select name="sembako" class="form-control" id="sembako" aria-describedby="sembako" required="required">
                        <option value="">-- Pilih Sembako --</option>
                        <?php $__currentLoopData = $sembako; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sembako): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option <?php echo e($harga->id_sembako == $sembako['id'] ? 'selected' : ''); ?> value=<?php echo e($sembako['id']); ?>><?php echo e($sembako['sembako']); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>  
                </div>
                <div class="form-group">
                    <label for="name">Harga Sebelum</label>                    
                    <input type="number" name="harga_sebelum" class="form-control" id="harga_sebelum" value="<?php echo e($harga->harga_sebelum); ?>" aria-describedby="harga_sebelum" required="required">                
                </div>
                <div class="form-group">
                    <label for="name">Harga Sesudah</label>                    
                    <input type="number" name="harga_sesudah" class="form-control" id="harga_sesudah" value="<?php echo e($harga->harga_sesudah); ?>" aria-describedby="harga_sesudah" required="required">                
                </div>
                <button type="submit" class="btn btn-primary">Kirim</button>
                <a href="<?php echo e(route('harga')); ?>"><button type="button" class="btn btn-warning">Kembali</button></a>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/ayog2384/public_html/dispaperta/resources/views/harga/edit.blade.php ENDPATH**/ ?>